<?php 

if(!isset($_SESSION))
{
    session_start();
}

include '../config/definitions.php';

	$ch = curl_init();
	// Obtener Usuarios
	$url = $urlWS.'service=usuarioservices&metodo=ObtenerUsuarios';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$usuarios = json_decode($resultData, true);

// Obtener listado de estados
	$url = $urlWS.'service=usuarioservices&metodo=ObtenerEstados';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$estados = json_decode($resultData, true);


	 if (isset($_POST['accion'])){
        switch($_POST['accion']){
            case 'ActualizarUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=usuarioservices&metodo=ActualizarUsuario';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $actualizar = json_decode($resultData, true);
                $respuesta = $actualizar['msg'];
                print_r($respuesta);
                exit();
                curl_close($ch);
                break;
            case 'ObtenerUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=usuarioservices&metodo=ObtenerUsuarioByEmail';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
				$resultData = curl_exec($ch);
                $user = json_decode($resultData, true);
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
            case 'EliminarUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=usuarioservices&metodo=EliminarUsuario';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
				$resultData = curl_exec($ch);
                $user = json_decode($resultData, true);
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
            case 'BloquearUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=usuarioservices&metodo=BloquearUsuario';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
				$resultData = curl_exec($ch);
                $user = json_decode($resultData, true);
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
            case 'DesbloquearUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=usuarioservices&metodo=DesbloquearUsuario';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
				$resultData = curl_exec($ch);
                $user = json_decode($resultData, true);
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
        }
    }
	
	include '../views/adminUsers.php';
?>