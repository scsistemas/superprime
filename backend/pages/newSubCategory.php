<?php 

if(!isset($_SESSION))
{
    session_start();
}

include '../config/definitions.php';

	$ch = curl_init();
	//Obtener Categorias
	$url = $urlWS.'service=productoservices&metodo=ObtenerCategorias';
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$categoriasSlider = json_decode($resultData, true);

	$index = 0;
	$flag = 0;
	$categorias = [];
	$subcategorias = [];

	// Obtener Filtros
	$url = $urlWS.'service=filtroservices&metodo=ObtenerFiltros';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$filtros = json_decode($resultData, true);

    curl_close($ch);

	if (isset($_POST['nombre'])){
    $ch = curl_init();
    $url = $urlWS.'service=categoriaservices&metodo=subcategoriacreate';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
    $resultData = curl_exec($ch);
    $subcategoria = json_decode($resultData, true);
    $respuesta = $subcategoria['msg'];
    curl_close($ch);
    print_r($respuesta);
    exit();
    }

    if (isset($_POST['accion'])){
        switch($_POST['accion']){
            case 'EliminarFiltro':
                $ch = curl_init();
                $url = $urlWS.'service=filtroservices&metodo=EliminarFiltro';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $filtro = json_decode($resultData, true);
                $respuesta = $filtro['msg'];
                print_r($respuesta);
                exit();
                curl_close($ch);
                break;
            case 'InsertarFiltro':
                $ch = curl_init();
                $url = $urlWS.'service=filtroservices&metodo=InsertarFiltro';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $filtro = json_decode($resultData, true);
                $respuesta = $filtro['msg'];
                print_r($respuesta);
                exit();
                curl_close($ch);
                break;
        }
    }

	include '../views/newSubCategory.php';

?>