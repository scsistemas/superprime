<?php
	include '../config/definitions.php';
	//echo('<pre>');

	$codProd = $_GET['codProd'];
	//print_r('codProd: ' . $codProd);

	//print_r($dataML);
	//print_r($rutaFotos);
	if($codProd != 'skp'){//el llamado fue desde el portal de Super Prime
		if($dataML['ml_token'] == null){//si no se tiene token
			actulizarClaveValor($urlWS, 'prdCod', $codProd);
			obtenerCode($dataML);
		}else{//si el token existe
			$fecExpira = $dataML['ml_fecToken'] + $dataML['ml_expires'];
			if((time() > ($fecExpira)) || (($fecExpira - time()) < 300)){//se verifica si el token esta vencido para refrescarlo
				$dataML = refreshToken($dataML, $urlWS);
			}
			
			//print_r($dataML);
			$redir = "Location: http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/mlCatalog.php?codPro=" . $codProd;
			header($redir);
		}
	}else{//el llamado es desde Mercado Libre
		$dataML['ml_authCode'] = $_GET['code'];
		actulizarClaveValor($urlWS, 'ml_authCode', $dataML['ml_authCode']);
		$codProd = obtenerClaveValor($urlWS, 'prdCod');

		$dataML = obtenerToken($dataML, $urlWS);
		//print_r($dataML);
		$redir = "Location: http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/mlCatalog.php?codPro=" . $codProd;
		header($redir);
	}
	//print_r('redir: ' . $redir);
	//echo('</pre>');
?>