<?php 

if(!isset($_SESSION))
{
    session_start();
}

include '../config/definitions.php';

	$ch = curl_init();
	// Obtener Administradores
	$url = $urlWS.'service=adminservices&metodo=ObtenerAdministradores';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$administradores = json_decode($resultData, true);


	// Obtener listado de menu
	$url = $urlWS.'service=adminservices&metodo=ObtenerMenus';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$accesos = json_decode($resultData, true);

    curl_close($ch);



	 if (isset($_POST['accion'])){
        switch($_POST['accion']){
            case 'ActualizarUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=adminservices&metodo=UpdateAdmin';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $uadmin = json_decode($resultData, true);
                $respuesta = $uadmin['msg'];
                print_r($respuesta);
                exit();
                curl_close($ch);
                break;
            case 'InsertarUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=adminservices&metodo=CrearAdmin';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $admin = json_decode($resultData, true);
                $respuesta = $admin['msg'];
                print_r($respuesta);
                exit();
                curl_close($ch);
                break;
            case 'ObtenerUsuario':
                $ch = curl_init();
                $url = $urlWS.'service=adminservices&metodo=ObtenerAdmin';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
				$resultData = curl_exec($ch);
                $admin = json_decode($resultData, true);
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
                break;
            case 'EliminarAdministrador':
                $ch = curl_init();
                $url = $urlWS.'service=adminservices&metodo=EliminarAdministrador';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
				$resultData = curl_exec($ch);
                $admin = json_decode($resultData, true);
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
        }
    }
	include '../views/adminAdministrators.php';
?>