<?php 

if(!isset($_SESSION))
{
    session_start();
}

include '../config/definitions.php';

	$ch = curl_init();
	// Obtener Razones Sociales
	$url = $urlWS.'service=tiendaservices&metodo=ObtenerRazonesSociales';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$razonsocial = json_decode($resultData, true);

	
// Obtener listado de estados
	$url = $urlWS.'service=usuarioservices&metodo=ObtenerEstados';
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$estados = json_decode($resultData, true);



	 if (isset($_POST['accion'])){
        switch($_POST['accion']){
            case 'ObtenerCiudadesByRazonSocial':
                $ch = curl_init();
                $url = $urlWS.'service=tiendaservices&metodo=ObtenerCiudadesByRazonSocial';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
				print_r($resultData);
				exit();
                curl_close($ch);
                break;
            case 'ObtenerTiendasByEstado':
                $ch = curl_init();
                $url = $urlWS.'service=tiendaservices&metodo=ObtenerTiendasByEstado';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
				print_r($resultData);
				exit();
                curl_close($ch);
                break;
            case 'ObtenerCiudades':
                $ch = curl_init();
                $url = $urlWS.'service=generalservices&metodo=ObtenerCiudades';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
				print_r($resultData);
				exit();
                curl_close($ch);
                break;
            case 'ObtenerMunicipios':
                $ch = curl_init();
                $url = $urlWS.'service=generalservices&metodo=ObtenerMunicipios';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
				print_r($resultData);
				exit();
                curl_close($ch);
                break;
            case 'ObtenerParroquias':
                $ch = curl_init();
                $url = $urlWS.'service=generalservices&metodo=ObtenerParroquias';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
				print_r($resultData);
				exit();
                curl_close($ch);
                break;
            case 'ActualizarTienda':
                $ch = curl_init();
                $url = $urlWS.'service=tiendaservices&metodo=ActualizarTienda';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $actualizar = json_decode($resultData, true);
                $respuesta = $actualizar['msg'];
                print_r($respuesta);
                exit();
                curl_close($ch);
                break;
            case 'InsertarTienda':
                $ch = curl_init();
                $url = $urlWS.'service=tiendaservices&metodo=InsertarTienda';
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_POST));
                $resultData = curl_exec($ch);
                $nueva = json_decode($resultData, true);
                $respuesta = $nueva['msg'];
                print_r($resultData);
                exit();
                curl_close($ch);
                break;
        }
    }
	include '../views/adminShops.php';
?>