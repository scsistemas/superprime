<?php 
	
	include '../config/definitions.php';

	$ch = curl_init();
	$url = $urlWS.'service=generalservices&metodo=ObtenerTotalesDashbord';

	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$resultTotales = json_decode(curl_exec($ch),true);
	
	/*echo "<pre>";
  		print_r($resultTotales );
	echo "</pre>";*/

	$url = $urlWS.'service=generalservices&metodo=ObtenerActividadReciente';
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$resultSincronizaciones = json_decode(curl_exec($ch),true);
	/*echo "<pre>";
  		print_r($resultSincronizaciones);
	echo "</pre>";*/

	curl_close($ch);

	include '../views/principal.php';
?>