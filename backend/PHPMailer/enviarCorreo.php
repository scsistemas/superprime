<?php
include '../config/definitions.php';
require_once('class.phpmailer.php');
require_once('class.smtp.php');
require_once("../dompdf/dompdf_config.inc.php");
$ch = curl_init();

session_start();
$url = $urlWS.'service=userservices&metodo=ObtenerDatosPerfil&p_id_usuario='.$_SESSION['userid'];
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$resultData = curl_exec($ch);
$tPersona = json_decode($resultData, true);



$fecha_desde = $_SESSION['data']['fecha_desde'];
$fecha_hasta = $_SESSION['data']['fecha_hasta'];
$tipo_prod = $_SESSION['data']['tipo_prod'];
$marca = $_SESSION['data']['marca'];
$genero = $_SESSION['data']['genero'];
$publicado = $_SESSION['data']['publicado'];
$p_publicado_ml = $_SESSION['data']['publicado_ml'];
$p_cod_prod = $_SESSION['data']['cod_prod'];
$p_desc_sap = $_SESSION['data']['desc_sap'];
$p_foto = $_SESSION['data']['foto'];
$p_ignorados = $_SESSION['ignorados'];
$recordsTotal = $_SESSION['data']['recordsTotal'];
$url = $urlWS.'service=productoservices&metodo=ObtenerProductos&p_fecha_desde='.$fecha_desde.'&p_fecha_hasta='.$fecha_hasta.'&p_tipo_prod='.$tipo_prod.'&p_marca='.$marca.'&p_genero='.$genero.'&p_publicado='.$publicado.'&p_cod_prod='.$p_cod_prod.'&p_desc_sap='.$p_desc_sap.'&p_foto='.$p_foto.'&p_publicado_ml='.$p_publicado_ml.'&ignorados='.$p_ignorados.'&start=0&length='.$recordsTotal.'&draw=1';


curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

$resultData = json_decode(curl_exec($ch),true);
if ($publicado)
    $pw = 'SI';
else
    $pw = 'NO';

if ($p_publicado_ml)
    $pml = 'SI';
else
    $pml = 'NO';

if ($genero == 1)
    $g = 'M';
else
{
    if ($genero == 2)
        $g = 'F';
}

if ($p_foto == 'S')
    $f = 'SI';
if ($p_foto == 'N')
    $f = 'NO';
if ($p_foto == '')
    $f= 'todos';
curl_close($ch);
ob_start();
include("../templates/listConsultProduct.php");
$page = ob_get_contents();  // almacenar el resultado de la salida en una variable
ob_end_clean();				// limpiar buffer de salida hasta este punto ..

//armas el mail
$mail = new PHPMailer;
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = host_prime;  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->FromName="SuperPrime";
$mail->Username =correo_notificacion;                 // SMTP username
$mail->Password = contrasena_notificacion;                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;                                    // TCP port to connect to

$mail->setFrom(correo_notificacion, 'SuperPrime');
$mail->addAddress($tPersona['Persona']['Usuario']);
$mail->isHTML(true);
$mail->CharSet = 'UTF-8';
$mail->Subject = 'Correo Consulta de Productos';
$mail->Body = 'Consulta de Productos Prime Shoes';
$mail->Timeout=30;




$dompdf = new DOMPDF();
$dompdf->load_html($page);
$dompdf->set_paper("letter","landscape");
$dompdf->render();
$nombre_archivo = "consulta_productos.pdf";
$gestor = fopen($nombre_archivo, 'w');
fwrite($gestor, $dompdf->output());
fclose($gestor);
$mail->AddAttachment($nombre_archivo,"consulta_productos.pdf");

$exito = $mail->Send();

?>
