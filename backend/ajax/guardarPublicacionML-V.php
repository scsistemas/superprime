<?php
	include '../config/definitions.php';
	require '../config/meli.php';

	//echo('<pre>');
	//ini_set('display_errors', '1');

	try {
		$id = $_POST['id'];
		$categorias = $_POST['categorias'];
		$plan = $_POST['plan'];
		$publicacion = $_POST['pub'];
		$respFinal = 0;
		$idsML = "";

		$urlDescProd = $urlWS . 'service=productoservices&metodo=ObtenerDetalleProducto&p_cod_prod=' . $id;
		$curl = curl_init($urlDescProd);
		curl_setopt($curl, CURLOPT_URL,$urlDescProd);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	
		$descProd = curl_exec($curl);
		curl_close($curl);
		$descProd = json_decode($descProd, true);

		$catBD = explode(';', $categorias);

		if($publicacion == 'S'){
			$tokenPre = $dataML['ml_token'];
			//print_r($dataML);
			//print_r('TokenPre: '.$tokenPre.'</br>');
			$dataML = comprobarRenovarToken($dataML, $urlWS);
			$tokenPost = $dataML['ml_token'];
			//print_r($dataML);
			//print_r('TokenPost: '.$tokenPost.'</br>');

			$fecExpira = $dataML['ml_fecToken'] + $dataML['ml_expires'];
			if($tokenPre == $tokenPost){
				if((time() > ($fecExpira)) || (($fecExpira - time()) < 300)){
					$error = true;
	    		}else{
	    			$error = false;
	    		}	
			}else{
				$error = false;
			}
    		
    		//print_r('error: '.$error.'</br>');
		}
		
		//print_r($descProd['producto']['ColoresProducto']);

		foreach ($descProd['producto']['ColoresProducto'] as $value) {
					
			/*if($op == 1){//se actualiza en la tabla
				$service_url = $urlWS . 'service=productoservices&metodo=actualizarPublicacion';
			}else{//se inserta en la tabla
				$service_url = $urlWS . 'service=productoservices&metodo=insertarPublicacion';
			}*/

			$service_url = $urlWS . 'service=productoservices&metodo=insertarPublicacion';

			if($publicacion == 'S'){//se hace la publicacion en ML mediante el consumo del WS
				if(!$error){
					$urlDetaColor = $urlWS . 'service=generalservices&metodo=ObtenerDetalleColor&p_cod_prod=' . $id . '&p_color=' . $value['CodColor'];
					//print_r('urlDetaColor: ' . $urlDetaColor . '</br>');
					$curl = curl_init($urlDetaColor);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					$detaResponse = curl_exec($curl);
					$detaColor = json_decode($detaResponse, true);
					curl_close($curl);

					$urlCantColor = $urlWS . 'service=productoservices&metodo=ObtenerProductosPorColor&p_cod_prod=' . $id . '&p_color=' . $value['CodColor'];
					$curl = curl_init($urlCantColor);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					$cantColorResponse = curl_exec($curl);
					$cantColor = json_decode($cantColorResponse, true);
					curl_close($curl);

					$publicParam = array('title' => $descProd['producto']['Brief'] . ' - ' . $value['DescWeb'],
										 'category_id' => $catBD[(count($catBD) - 1)],
										 'price' => $detaColor['colorProd']['PreciosWeb']['Total'],
										 'currency_id' => 'VEF',
										 'available_quantity' => $cantColor['total'],
										 'buying_mode' => 'buy_it_now',
										 'listing_type_id' => $plan,
										 'condition' => 'new',
										 'description' => $descProd['producto']['Brief'] . ' - ' . $value['DescWeb'],
									     'tags' => array("immediate_payment"),
										 'warranty' => '12 months'
										);

					if(count($detaColor['colorProd']['ImagenesSelec']) > 0){
						$publicParam['pictures'] = array();
						foreach($detaColor['colorProd']['ImagenesSelec'] as $valor){
							$rutImg['source'] = $_SERVER['HTTP_ORIGIN'] . $_SERVER['CONTEXT_PREFIX'] . str_replace('..', '', $rutaFotos) . $id . '/' . $value['CodColor'] . '/' . $valor;
							array_push($publicParam['pictures'], $rutImg);
						}
					}

					$urlPublicarML = 'https://api.mercadolibre.com/items?access_token=' . $dataML['ml_token'];
					//print_r('urlPublicML: ' . $urlPublicarML . '</br>');
					//print_r($publicParam);
					$publicParam = json_encode($publicParam);
					//print_r('paramPublicML: '.$publicParam.'</br>');
					$curlML = curl_init($urlPublicarML);
					curl_setopt($curlML, CURLOPT_POST, true);
					curl_setopt($curlML, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curlML, CURLOPT_POSTFIELDS, $publicParam);
					curl_setopt($curlML, CURLOPT_RETURNTRANSFER, true);
					$responseML = curl_exec($curlML);
					curl_close($curlML);
					
					//print_r('respPublicML: '.$responseML.'</br>');
					$responseML = json_decode($responseML, true);

					if(isset($responseML['message'])){
						$respFinal = '3';
						$idsML = null;
					}else{
						$parametros = array('p_codProducto' => $id, 'p_codColor' => $value['CodColor'], 'p_plan' => $plan, 'p_categorias' => $categorias, 'p_idPubML' => $responseML['id'], 'p_publicado' => $publicacion);
						//print_r($service_url.'</br>');
						//print_r($parametros);
						$parametros = json_encode($parametros);
						//print_r('paramSave: '.$parametros.'</br>');
						$curl = curl_init($service_url);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_POST, true);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros);
						$curl_response = curl_exec($curl);
						//print_r('respSave: '.$curl_response.'<p>hola</p></br>');
						$curl_response = json_decode($curl_response, true);
						//print_r($curl_response);
						curl_close($curl);

						if($respFinal == 0){
							$respFinal = ''.$curl_response['success'].'';
						}

						if($idsML == ""){
							$idsML = $curl_response['idML'];
						}else{
							$idsML .= ";" . $curl_response['idML'];
						}	
					}	
				}else{
					$respFinal = '2';
					$idsML = null;
				}
			}else{
				$parametros = array('p_codProducto' => $id, 'p_codColor' => $value['CodColor'], 'p_plan' => $plan, 'p_categorias' => $categorias, 'p_idPubML' => null, 'p_publicado' => $publicacion);
				$parametros = json_encode($parametros);

				$curl = curl_init($service_url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $parametros);
				$curl_response = curl_exec($curl);
				$curl_response = json_decode($curl_response, true);
				curl_close($curl);

				if($respFinal == 0){
					$respFinal = $curl_response['success'];
				}

				if($idsML == ""){
					$idsML = $curl_response['idML'];
				}
			}	
		}	
	} catch (Exception $e) {
		$respFinal = '0';
		$idsML = null;
		//print_r('exception: ' . $e . '</br>');
	}
	

	$curl_response = array('success' => $respFinal, 'idML' => $idsML);
	$curl_response = json_encode($curl_response);
	echo($curl_response);

	//print_r('respFinal: '.$curl_response.'</br>');
	//echo('</pre>');

	function consumoServicioGet($urlWS){
		$curl = curl_init($urlWS);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);	
		$resp = curl_exec($curl);
		curl_close($curl);
		$resp = json_decode($resp, true);

		return $resp; 
	}

	function consumoServicioPost($urlWS, $params){
		$curl = curl_init($urlWS);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		$resp = curl_exec($curl);
		$resp = json_decode($resp, true);
		curl_close($curl);

		return $resp; 
	}

	function consumoServicioPostSSL($urlWS, $params){
		$curl = curl_init($urlWS);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$resp = curl_exec($curl);
		curl_close($curl);
		
		$resp = json_decode($resp, true);

		return $resp; 
	}
?>