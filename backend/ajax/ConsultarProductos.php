<?php 
	include '../config/definitions.php';
	$ch = curl_init();

	$requestData= $_REQUEST;
    $fecha_desde = $_POST['fecha_desde'];
    $fecha_hasta = $_POST['fecha_hasta'];
    $tipo_prod = $_POST['tipo_prod'];
    $marca = $_POST['marca'];
    $genero = $_POST['genero'];
    $publicado = $_POST['publicado'];
    $p_publicado_ml = $_POST['publicado_ml'];
    $p_cod_prod = $_POST['cod_prod'];
    $p_desc_sap = urlencode($_POST['desc_sap']);
    $p_foto = $_POST['foto'];
    $p_ignorados = $_POST['ignorados'];

    session_start();
    $_SESSION['data']['fecha_desde'] = $fecha_desde;
    $_SESSION['data']['fecha_hasta'] = $fecha_hasta;
    $_SESSION['data']['tipo_prod'] = $tipo_prod;
    $_SESSION['data']['marca'] = $marca;
    $_SESSION['data']['genero'] = $genero;
    $_SESSION['data']['publicado'] = $publicado;
    $_SESSION['data']['publicado_ml'] =$p_publicado_ml;
    $_SESSION['data']['cod_prod'] = $p_cod_prod;
    $_SESSION['data']['desc_sap'] = $p_desc_sap;
    $_SESSION['data']['foto'] = $p_foto;
    $_SESSION['data']['ignorados'] = $p_ignorados;

	$url = $urlWS.'service=productoservices&metodo=ObtenerProductos&p_fecha_desde='.$fecha_desde.'&p_fecha_hasta='.$fecha_hasta.'&p_tipo_prod='.$tipo_prod.'&p_marca='.$marca.'&p_genero='.$genero.'&p_publicado='.$publicado.'&p_cod_prod='.$p_cod_prod.'&p_desc_sap='.$p_desc_sap.'&p_foto='.$p_foto.'&p_publicado_ml='.$p_publicado_ml.'&ignorados='.$p_ignorados.'&start='.$requestData['start'].'&length='.$requestData['length'].'&draw='.$requestData['draw'];
/*echo (json_encode($url));
exit();*/
//$url = "http://localhost/integracion/admin/index.php?service=productoservices&metodo=ObtenerProductos&p_fecha_desde=01-05-2016&p_fecha_hasta=31-05-2016&p_tipo_prod=1&p_marca=2&p_genero=1&p_publicado=S&p_cod_prod=&p_desc_sap=Zapatos%20Y%2095&p_foto=&p_publicado_ml=&start=0&length=5&draw=2";
 
//$url = "http://localhost/integracion/admin/index.php?service=productoservices&metodo=ObtenerProductos&p_fecha_desde=01-05-2016&p_fecha_hasta=31-05-2016&p_tipo_prod=1&p_marca=2&p_genero=1&p_publicado=S&p_cod_prod=&p_desc_sap=Zapatos Y 95&p_foto=&p_publicado_ml=&start=0&length=5&draw=2";
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	$resultData = curl_exec($ch);

	curl_close($ch);
    
	$result = json_decode($resultData, true);

    $_SESSION['data']['recordsTotal'] = $result['recordsTotal'];

	for ($i =0; $i<sizeof($result['data']); $i++){

		//array_push($result['data'][$i],'NO');
        $ignorado=$result['data'][$i][9]=='N'?'Ignorar':'No Ignorar';
        $indig=$result['data'][$i][9]=='N'?'S':'N';
		array_push($result['data'][$i], strval('<a href="../pages/loadProduct.php?codPro='.$result['data'][$i][0].'" class="btn btn-primary btn-xs">Administrar Web</a>
                                <a href="../pages/mlCatalog.php?codPro='.$result['data'][$i][0].'" class="btn btn-primary btn-xs">Administrar ML</a> <button  data-producto='.$result['data'][$i][0].' data-ignorar='.$indig.' id=btig-'.$result['data'][$i][0].' class="btn btn-primary btn-xs btignor">'.$ignorado.'</button>'));

		if (is_null($result['data'][$i][6]))
		 	$result['data'][$i][6] = 'NO';

	}

    if (isset($result))
        $_SESSION['existe'] = 1;
    else
        $_SESSION['existe'] = 0;
	$data = json_encode($result);

	echo $data;

	
?>