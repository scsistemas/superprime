﻿<?php include '../includes/header.php';?>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

              <!-- Resultados de Sincronizacion ../pages/processLoadProduct.php-->
            <form action="" name="load" id="load" method="post">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title" style="    padding: 1px 5px 2px;">
                    <div class="row">
                      <div class="col-lg-4">
                        <h2>Detalle del Producto</h2>
                      </div>
                      <div class="col-lg-2 col-md-offset-6 text-right" style="background-color: #FFC107;padding: 10px;color: #fff;margin-top: -11px;margin-bottom: -3px;">
                       <label class="control-label hover">Publicar en Web
                          <?php
                            if ($_SESSION['product']['producto']['Publicado'] == "S"){?>
                            <input type="checkbox" id="publicarW"  name="publicarW" checked class="js-switch" />
                          <?php }else{?>
                            <input type="checkbox" id="publicarW" name="publicarW" class="js-switch" />
                            <?php }?>
                        </label>
                      </div>
                    </div>
                    
                    <div class="clearfix"></div>

                  </div>

                  <div class="x_content">

                    <div class="form-horizontal">

                      <!-- Codigo, Nombre, Descripcion SAP, Modelo -->
                      <div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                          <div class="form-group" id="codigo">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="uno">Código </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="dos">
                              <input type="text" class="form-control" id="CodProducto" name="CodProducto" readonly="readonly" value="<?php echo $_SESSION['product']['producto']['CodProducto']?>">
                            </div>
                          </div>
                          <div class="form-group" id="nombre">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="uno">Nombre</label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="dos">
                              <input class="date-picker form-control col-md-7 col-xs-12" name="iNombre" id="iNombre" value="<?php echo $_SESSION['product']['producto']['Nombre']?>" required="required" type="text">
                            </div>
                          </div>
                         
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12" id="descripcionSap">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="uno">Descripción SAP </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="dos">
                              <input type="text" class="form-control" readonly="readonly" value="<?php echo $_SESSION['product']['producto']['DescSap']?>">
                            </div>
                          </div>
                          
                          <div class="form-group" id="modelo">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="uno">Modelo</label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="dos">
                              <input type="text" id="iModelo" name="iModelo" nombre="iNombre" class="form-control" value="<?php echo $_SESSION['product']['producto']['Modelo']?>">
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- /Codigo, Nombre, Descripcion SAP, Modelo -->


                      <div class="row"  id="colores">
                        <div class="col-md-6 col-xs-12">
                          <label class="col-md-2 control-label" id="uno">Colores</label>
                          <div class="col-md-5" id="dos">
                            <select name="colorSap" id="colorSap" class="form-control" required="" >
                            <option value="">Seleccione Color</option>
                            <?php 

                              foreach ($_SESSION['product']['producto']['ColoresProducto'] as $colores) { ?>
                                <option value="<?php echo $colores['CodColor'] ?>"><?php echo $colores['DescSap'] ?></option>
                                
                              <?php
                              }?>
                          </select>

                          </div>
                              <div class="col-md-4 col-sm-4 col-xs-12 3" id="tres">
                                  <input disabled class="form-control col-md-7 col-xs-12" type="text" value="" id="colorWeb" name="colorWeb">
				  <span class="form-control-feedback left" aria-hidden="true">Color Web</span>
                              </div>
                        </div>
                        <div class="col-md-6 col-xs-12">

                            <div class="col-md-4"  id="cuatro">
                              <div class="form-group" name="mostrarWeb" id="mostrarWeb">
                                <label class="control-label" >Mostrar en Web
                                  <input disabled type="checkbox" name="checkMWeb" id="checkMWeb"  class="js-switch" /><!--class="js-switch"-->
                                </label>
                              </div>
                            </div>
                            <div class="col-md-4" id="cinco">
                                <div class="form-group">
                                    <div id="colorP" name="colorP" class="input-group colorpicker-component">
                                        <input id="icolorP" name="icolorP" type="hidden"/>
                                        <label>Color Principal</label>
                                        <span class="input-group-addon"><i class="borde"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="seis">
                            
                               <input type="checkbox"  name="checkCSec" id="checkCSec" class="js-switch"/>
                                <div class="form-group">
	                                <div id="colorS" name="colorS" class="input-group colorpicker-component">
	                                    <input id="" name="icolorS" type="hidden"/>
	                                    <label >Color Secundario</label>
	                                    <span class="input-group-addon"><i class="borde"></i></span>
	                                </div>
                                </div>

                                </div>
                        </div>
                      </div>
                      <div class="row" id="dimensiones">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" >
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dimensiones</label>
                             <div class="col-md-4 col-sm-4 col-xs-12 1" id="uno">
                              <input type="text" id="iAlto" name="iAlto" class="form-control"  value="<?php echo $_SESSION['product']['producto']['Alto']?>">
                              <span class="form-control-feedback left" aria-hidden="true">Alto</span>
                            </div>
                             <div class="col-md-4 col-sm-4 col-xs-12 2" id="dos">
                              <input type="text" class="form-control" id="iAncho" name="iAncho"  value="<?php echo $_SESSION['product']['producto']['Ancho']?>">
                              <span class="form-control-feedback left" aria-hidden="true">Ancho</span>
                            </div>
                            
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6  col-lg-6 ">
                          <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-12 3" id="tres">
                              <input type="text" class="form-control" id="iProfundidad"  name="iProfundidad" value="<?php echo $_SESSION['product']['producto']['Profundidad']?>">
                              <span class="form-control-feedback left" aria-hidden="true">Profundidad</span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 4" id="cuatro">
                              <input type="text" class="form-control" id="iPeso" name="iPeso" value="<?php echo $_SESSION['product']['producto']['Peso']?>">
			      <span class="form-control-feedback left" aria-hidden="true">Peso</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <!-- /Colores y Dimenciones -->

                      <!-- Talla -->
                      <div class="row" id="Talla">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
                          <div class="form-group" id="groupTallas" name="groupTallas">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="uno">Talla</label>
                             <div class="col-md-4 col-sm-4 col-xs-12 1" id="dos">
                             <div name="divTallas" id="divTallas">
                              <select class="selectpicker" multiple id="tallas" name="tallas[]">
                                <option value="" selected>Seleccione</option>
                              </select>
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /Talla -->

                      <!-- Fotos -->
                      <div id="mostraFotos" hidden="true">
                      <div class="row" id="fotos">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                           <div class="form-group colors">
                            <label class="control-label col-md-1 col-sm-1 col-xs-12" id="uno">Fotos</label>
                            <div class="col-md-10 col-sm-10 col-xs-12" id="dos">
                              <div class="row" id="fotos-container">
                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                      <!-- /Fotos -->

                      <div class="divider-dashed"></div>

                      <!-- Precios -->
                      <div class="row" id="precios">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="uno">Precio Neto </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="dos">
                              <input type="text" class="form-control" readonly="readonly" placeholder="" id="precioSap" name="precioSap">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="tres">I.V.A </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="cuatro">
                              <input type="text" class="form-control" readonly="readonly" placeholder="" id="ivaSap" name="ivaSap">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="cinco">Total </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="seis">
                              <input type="text" class="form-control" readonly="readonly" placeholder="" id="totalSap" name="totalSap">
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="siete">
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="ocho">Precio Neto </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="nueve">
                              <input type="text" class="form-control numeric" id="precioWeb" name="precioWeb" disabled="true" onkeyDown="calculadora();">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="diez">% de Descuento </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="once">
                              <input type="text" disabled="true" class="form-control numeric" id="porcDesc" name="porcDesc" onkeyDown="calculadora();">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="doce">Con Descuento</label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="trece">
                              <input type="text" class="form-control numeric" id="precioDesc" name="precioDesc" disabled="true" readonly="readonly">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="catorce">I.V.A </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="quince">
                              <input type="text" disabled="true" class="form-control numeric" id="ivaWeb" name="ivaWeb" readonly="readonly">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="diesiseis" readonly="readonly">Total </label>
                            <div class="col-md-9 col-sm-9 col-xs-12" id="diesisiete">
                              <input type="text" disabled="true" class="form-control numeric" id="totalWeb" name="totalWeb" readonly="readonly">
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- /Precios -->

                      <div class="divider-dashed"></div>

                      <!-- Brief-Caracteristicas -->
                      <div class="row" id="brief">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                          <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" id="uno">Brief</label>
                            <div class="col-md-10 col-sm-10 col-xs-12" id="dos">
                              <textarea id="tBrief" name="tBrief" class="form-control" required="required"><?php echo $_SESSION['product']['producto']['Brief']?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12" id="cinco">Características adicionales</label>
                          <div class="col-md-10 col-sm-10 col-xs-12" id="seis">
                              <select multiple="multiple" id="sCaracteristicas[]" name="sCaracteristicas[]" >
                              <?php 
                              foreach($_SESSION['product']['producto']['Caracteristicas'] as $valor){
                                if($valor['MostrarWeb'] == "S"){
                              ?>
                                <option selected="selected" value="<?php echo($valor['CodCarac']);?>"><?php echo($valor['DescCarac']);?></option>
                              <?php  
                                }else{
                              ?>
                                <option value="<?php echo($valor['CodCarac']);?>"><?php echo($valor['DescCarac']);?></option>
                              <?php
                                }
                              }
                              ?>
                                 
                              </select>
                          </div>
                          </div>
                         
                        </div>
                      </div>
                      <!-- /Brief-Caracteristicas data-source="data/1000.json"-->

                      <div class="divider-dashed"></div>

                      <!-- Destacado -->
                      <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="busquedas" style="margin-left: 80px;">
                          <div class="form-group">
                            <label class="control-label hover">Primero en las Búsquedas 
                              <?php 
                               if ($_SESSION['product']['producto']['Busquedas'] == "S"){?>
                                    <input type="checkbox" checked id="primeroB" name="primeroB" class="js-switch" />
                                <?php }else{?>
                                    <input type="checkbox" id="primeroB" name="primeroB" class="js-switch" />
                                <?php }?>
                            </label>
                          </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" id="prime">
                          <div class="form-group">
                            <label class="control-label hover">Artículo Prime
                              <?php 
                              if ($_SESSION['product']['producto']['Busquedas'] == "S"){?>
                                  <input type="checkbox" id="articuloP" name="articuloP" checked class="js-switch" />
                               <?php }else{?>
                                    <input type="checkbox" id="articuloP" name="articuloP" class="js-switch" />
                                <?php }?>
                            </label>
                          </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" id="notificaciones">
                          <div class="form-group">
                            <label class="control-label hover">Notificaciones 
                            <?php 
                              if ($_SESSION['product']['producto']['Notificaciones'] == "S"){?>
                                  <input type="checkbox" id="notif" name="notif" checked class="js-switch" />
                              <?php }else{?>
                                  <input type="checkbox" id="notif" name="notif" class="js-switch" />
                              <?php }?>
                            </label>
                          </div>
                        </div>
                      </div>
                      <!-- /Destacado -->

                      <div class="divider-dashed"></div>
                      
                      <!-- Videos -->
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="videos">
                           <div class="form-group colors">
                               <iframe src="../views/video.php?codProd=<?php echo $_GET['codPro']?>" width='100%' height="300">
                               </iframe>
                          </div>
                        </div>
                      </div>
                      <!-- /Videos -->
                        
                    </div>
                    <div class="text-center">
                    <input type='button' class="btn btn-warning" onclick="backForm()" value="Regresar"/>
                      <input type='button' class="btn btn-success" onclick="submitForm()" value="Guardar"/>
                      <!--<a href="#" class="btn btn-success"  onclick="submitForm()" data-remodal-target="guardar">Guardar</a>-->
                    </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Resultados de Sincronizacion -->
          <input type="hidden" id="imagenescolor" name="imagenescolor" >
              <!-- Fin defl formulario  -->
            </form>

              <!-- Remodal para guardar -->
              <div class="remodal" data-remodal-id="guardar">
                <button data-remodal-action="close" class="remodal-close"></button>
                <h1>Información</h1>
                <p>Su producto se ha gestionado correctamente.</p>
                <br>
                <button data-remodal-action="confirm" class="btn btn-success">OK</button>
              </div>

              <!-- /Remodal para guardar -->

          </div>

     

        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
        <div id="jsonData" style="visibility : hidden" ><?php echo json_encode($_SESSION['product'])?></div>
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>



    <!-- Dropzone -->
    <script src="../js/dropzone/dropzone.js"></script>

    <!-- Switchery -->
    <script src="../js/switchery/switchery.min.js"></script>

    <!-- Remodal -->
    <script src="../js/remodal/remodal.js"></script>

    
    <!-- Numeric -->
    <script src="../js/numeric/jquery.numeric.min.js"></script>

    <!-- Numeric -->
    <script src="../js/priceFormat/jquery.price_format.2.0.js"></script>

    <!-- ColorPicker -->
    <script src="../js/colorpicker/bootstrap-colorpicker.min.js"></script>
    
    <!-- Remodal -->
    <script src="../js/duallistbox/dual-list-box.js"></script>
 
    <!-- Multiselect -->
    <script src="../js/multiselect/bootstrap-select.js"></script>


    <script type="text/javascript">



        $("input.numeric").numeric();
      $('input.numeric').priceFormat({
        prefix: ''
      });


      //$('#sCaracteristicas').DualListBox();
      $('select[name="sCaracteristicas[]"]').DualListBox();

      // Multiselect
      $('#tallas').selectpicker();
	   $('#tallas').prop('disabled', true);
	   $('#tallas').selectpicker('refresh');





      //ColorPicker
      $('#colorP').colorpicker();
      $('#colorP').colorpicker('disable');

      $('#colorS').colorpicker();
      //$('#colorS').colorpicker('disable');

      $('#checkCSec').click(function(){
        if (!this.checked) {
            
              $('#colorS').colorpicker('disable');
        }
        else{
              
              $('#colorS').colorpicker('enable');
            }
      });

       $("#colorSap").on('focus', function () {
        // Store the current value on focus and on change
        previous = this.value;
        }).change(function() {
            // almacena el codigo de color anterior al cambio del select colorSap
            var CodProAnt = previous;
            

            // Make sure the previous value is updated
            previous = this.value;

            //almacena los cambios que el usuaraio administro en la variable jsonHTML que constantemente se está actualizando y escribiendo en el div id #jsonData
            var jsonHTML = jQuery.parseJSON($('#jsonData').text());

            
            var CodProAct = previous;
            var CheckWeb = false;
            var CheckSec = false;
            var ColorPri = "";
            var ColorSec = "";
            var CheckWeb = false;
            var CheckSec = false;
            var DesColorWeb = "";
            var tallasA = [];
            var i=0;

            //guardar tallas que fueron seleccionadas
            $('#tallas :selected').each(function(){ 
              tallasA[i] = $( this ).val(); 
              i++;
            });

            if (CodProAnt!= ''){
              ColorPri = $('#colorP').colorpicker('getValue');
              ColorSec = $('#colorS').colorpicker('getValue');
              CheckWeb2 = document.querySelector('#checkMWeb');
              CheckSec2 = document.querySelector('#checkCSec');
              DesColorWeb = $('#colorWeb').val();

              if (CheckWeb2.checked)
                  CheckWeb = 1;
              else
                  CheckWeb = 0;

              if (CheckSec2.checked)
                  CheckSec = 1;
              else
                  CheckSec = 0;
             
            }

            //alert($('#colorSap').val());
            if ($('#colorSap').val() != ''){
              $('#colorWeb').prop("disable",false);
            }else
                {
                    //alert ('color sap vacio');
                    $('#colorWeb').attr("disable",true);
                    $('#colorWeb').prop("value",'');

                }
            

            $('#colorWeb').prop( "disabled", false );
            $('#checkMWeb').prop( "disabled", false );
            $('#checkCSec').prop( "disabled", false );
            $('#colorP').colorpicker('enable');
            //$('#colorS').colorpicker('disabled');


            var colores = jsonHTML['producto']['ColoresProducto'];

            //cambia el color de los componentes colorpicker según el colorsap
            var CodProducto = jsonHTML['producto']['CodProducto']; 
            
            for(var i=0;i<colores.length;i++)
            {   
                if ( colores[i]['CodColor'] == $("#colorSap").val() ){

                 var CodColor = colores[i]['CodColor'];

                //Inicio cambiar descripción web según colorsap
                 $('#colorWeb').prop('value',colores[i]['DescWeb']); 
            
                //Fin cambiar descripción web según colorsap

                //cambiar checkMweb según colorSap

               /* if (colores[i]['MostrarWeb'] == 'S'){
                  alert('CHECKEADO');
                  $('#mostrarWeb').html('');
                  $('#mostrarWeb').html('<label class="control-label" >Mostrar en Web<input type="checkbox" name="checkMWeb" id="checkMWeb" class="js-switch" checked /></label>');
                }
                else
                    {
                      alert('NO CHECKEADO');
                      $('#mostrarWeb').html('');
                      $('#mostrarWeb').html('<label class="control-label" >Mostrar en Web<input type="checkbox" name="checkMWeb" id="checkMWeb" class="js-switch" /></label>');
                    }*/

                var cWeb = document.querySelector('#checkMWeb');
                //alert ('valor checkMweb: '+cWeb.checked);
                if (colores[i]['MostrarWeb'] == 'S'){

                  if (!cWeb.checked){
                    //alert('entro a NO checked');
                    //$('#checkMweb').trigger('click');
                   // $('#mostrarWeb').html('');
                   // $('#mostrarWeb').html('<label class="control-label" >Mostrar en Web<input type="checkbox" name="checkMWeb" id="checkMWeb" class="js-switch" checked /></label>');
                    //$('#checkMweb').prop('checked',true);
                      // $("#checkMweb").toggle(this.checked);
                      $('#checkMWeb').click();


                  }
                }
                else{
                  if (cWeb.checked){
                      $('#checkMWeb').click();
                    // alert('entro a checked');
                    //$('#checkMweb').prop('checked',false);
                    //$('#checkMweb').trigger('click');
                    //$('#mostrarWeb').html('');
                    //$('#mostrarWeb').html('<label class="control-label" >Mostrar en Web<input type="checkbox" name="checkMWeb" id="checkMWeb" class="js-switch" /></label>');
                  }
                }
                //Fin cambiar checkMweb según colorSap

                //Agregar color principal
                $('#colorP').colorpicker('setValue', colores[i]['ColorPrinc']);
                //Fin Agregar color principal

                //Agregar color secundario
                  if (colores[i]['ColorSec']){
                    //alert('Hay color sec');
                    $('#colorS').colorpicker('enable');
                    $('#colorS').colorpicker('setValue', colores[i]['ColorSec']);
                    $('#checkCSec').prop('disabled', false );
                    var cSec = document.querySelector('#checkCSec');
                    if (!cSec.checked){

                      //$('#checkCSec').prop('checked',true);
                        $('#checkCSec').click();

                      //$('#checkCSec').trigger('click');

                    }
                    
                  }
                  else{

                          $('#checkCSec').click();
                          //$('#colorS').colorpicker('disable');
                          $('#checkCSec').prop('checked', false);


                  }
                //Fin Agregar color secundario
              }

            }

              // Fotos seleccionadas
              var imgs = [];
              $( "div.icheckbox_square-aero.checked" ).each(function( index ) {
                  var url = $( this).parent().parent().children("img").attr('src');
                  var img = url.substring(url.lastIndexOf('/')+1,url.length);
                  imgs.push(img);
              });




              // Ajax que permite traer los datos del producto según colorsap y guardar los cambios del producto anterior a la selección

           var ajax_data = {
              "DesColorWeb" : DesColorWeb,
              "ColorPri" : ColorPri,
              "ColorSec" : ColorSec,
              "CheckWeb" : CheckWeb,
              "CheckSec" : CheckSec,
              "CodProAnt" : CodProAnt,
              "CodProAct" : CodProAct,
              "CodProducto" : CodProducto,
              "PrecioWeb" : $('#precioWeb').val(),
              "PorcDesc" : $('#porcDesc').val(),
              "imagenes" : JSON.stringify(imgs),
              "tallasA" : JSON.stringify(tallasA)
           }

           var datos;
           $.ajax({
            
              url:   '../ajax/CargarDatosProductosxColor.php',
              data: ajax_data,
              type:  'post',
              dataType: 'json',
              success:  function (x) {

                  $('#jsonData').text(JSON.stringify(x));
                  var a = x[$("#colorSap").val()];
                  $('#imagenescolor').attr('value', a['colorProd']['ImagenesSelec']);
                  //Inicio Carga select de tallas si existen tallas para el producto seleccionado
                  if (a['colorProd']['tallas']){
                    $('#tallas').prop( "disabled", false );
                    $('#tallas').selectpicker('refresh');
                    $("#tallas option").remove();

                     for(var i=0;i<a['colorProd']['tallas'].length;i++){
                        if (a['colorProd']['tallas'][i]['MostrarWeb'] == 'S')
                        {
                            $('#tallas').append('<option value="'+a['colorProd']['tallas'][i].CodTalla+'"selected>'+a['colorProd']['tallas'][i].DescTalla+'</option>');
                        }else{
                                $('#tallas').append('<option value="'+a['colorProd']['tallas'][i].CodTalla+'">'+a['colorProd']['tallas'][i].DescTalla+'</option>');
                             }
                      }
                       $('#tallas').selectpicker('refresh');

                  }
                  //Fin Carga select de tallas si existen tallas para el producto seleccionado

                  //Inicio Carga de fotos

                  //Inicio Mostrar todas las fotos del directorio
                  $('#fotos-container').html('');
                  $.ajax({
                      url: '<?php echo $rutaFotos?>'+ x['producto']['CodProducto']  + '/' + $("#colorSap").val(),
                      success: function(data){
                          datos=data;
                          $('#mostraFotos').prop('hidden',false);
                          eliminarNoImagen(x['producto']['CodProducto'],$("#colorSap").val());
                          $(data).find("a[href$='.jpg'],a[href$='.jpeg'],a[href$='.gif'],a[href$='.png']").each(function(index){
                              var url = '<?php echo $rutaFotos?>'+ x['producto']['CodProducto']  + '/' + $("#colorSap").val()+"/"+$(this).attr("href");
                              var ImgSel = $(this).attr("href").substring(0,$(this).attr("href").indexOf('.'));
                              var ImgNombre = $(this).attr("href");
                              $('#fotos-container').append('<div class="col-xs-6 col-md-2"><label> ' +
                                  '                         <div id="mostrarImg'+index+'"> ' +
                                  '                         <div class="thumbnail" name="divImg'+ImgSel+'" id="divImg'+ImgSel+'">' +
                                  '                         <img src="'+url+'" alt="..."  name="Img'+ImgSel+'" id="Img'+ImgSel+'">' +
                                  '                         <div class="caption text-center">' +
                                  '                         <input type="checkbox"  data-nombre="'+ImgNombre+'" name="cImg'+ImgSel+'" id="cImg'+ImgSel+'" class="flat checkimagen"  style="position: absolute; opacity: 0;"></div></div></label></div>');





                          });
                          $('input.flat').iCheck({
                              checkboxClass: 'icheckbox_square-aero',
                              radioClass: 'iradio_square-aero'
                          });
                          //Inicio hacer check a las fotos que se encuentran en el arreglo

                          if (a['colorProd']['ImagenesSelec'].length > 0){

                              for(var i=0;i<a['colorProd']['ImagenesSelec'].length;i++){
                                  var ImgSel = a['colorProd']['ImagenesSelec'][i];
                                  ImgSel = ImgSel.substring(0,ImgSel.indexOf('.'));
                                  $('#cImg'+ImgSel).iCheck('check');

                              }
                          }



                          //Fin hacer check a las fotos que se encuentran en el arreglo

                      },complete:function(){
                          $(datos).find("a[href$='.jpg'],a[href$='.jpeg'],a[href$='.gif'],a[href$='.png']").each(function(index){
                              var url = '<?php echo $rutaFotos?>'+ x['producto']['CodProducto']  + '/' + $("#colorSap").val()+"/"+$(this).attr("href");
                              var ImgSel = $(this).attr("href").substring(0,$(this).attr("href").indexOf('.'));
                              var ImgNombre = $(this).attr("href");



                              // For oncheck callback
                              $('#cImg'+ImgSel).on('ifChecked', function () {
                                  a['colorProd']['ImagenesSelec'].push(this.getAttribute('data-nombre'));
                                  $('#imagenescolor').attr('value', a['colorProd']['ImagenesSelec']);
                              });
                              // For onUncheck callback
                              $('#cImg'+ImgSel).on('ifUnchecked', function () {
                                  var pos =  a['colorProd']['ImagenesSelec'].indexOf(this.getAttribute('data-nombre'));
                                  pos > -1 && a['colorProd']['ImagenesSelec'].splice(pos, 1);
                                  $('#imagenescolor').attr('value', a['colorProd']['ImagenesSelec']);
                              });





                          });



                          },
                      error: function (obj) {
                           llenarNoImagen( x['producto']['CodProducto'],$("#colorSap").val());
                          $('#mostraFotos').prop('hidden',true);
                      }
                  });


                  
                  //Fin Mostrar todas las fotos del directorio


                  //Fin Carga de fotos

                  //Inicio Carga Precios Sap
                  $('#precioSap').removeAttr('value');
                  $('#precioSap').attr('value',a['colorProd']['PreciosSap'].Precio);

                  $('#ivaSap').removeAttr('value');
                  $('#ivaSap').attr('value',a['colorProd']['PreciosSap'].MontoIVA);

                  $('#totalSap').removeAttr('value');
                  $('#totalSap').attr('value',a['colorProd']['PreciosSap'].Total);
                  //Fin carga Preciso Sap

                  //Inicio Carga Precios Web
                  if(a['colorProd']['PreciosWeb'].Precio!=null && a['colorProd']['PreciosWeb'].Precio != '' && a['colorProd']['PreciosWeb'].Precio!='0.00' && a['colorProd']['PreciosWeb'].Precio != '0,00'){

	                  $("#precioWeb").prop( 'disabled', false );
	                  $('#precioWeb').removeAttr('value');
	                  $('#precioWeb').prop('value',a['colorProd']['PreciosWeb'].Precio);
	
	                  $('#ivaWeb').removeAttr('value');
	                  $('#ivaWeb').prop('value',a['colorProd']['PreciosWeb'].MontoIVA);
	
	                  $('#porcDesc').prop( 'disabled', false );
	                  $('#porcDesc').removeAttr('value');
	                  $('#porcDesc').prop('value',a['colorProd']['PreciosWeb'].PorcDesc);
	           
	                  $('#precioDesc').removeAttr('value');
	                  $('#precioDesc').prop('value',a['colorProd']['PreciosWeb'].MontoDesc);
	
	                  $('#totalWeb').removeAttr('value');
	                  $('#totalWeb').prop('value',a['colorProd']['PreciosWeb'].Total);
                  }else {
                  
                  	  $("#precioWeb").prop( 'disabled', false );
	                  $('#precioWeb').removeAttr('value');
	                  $('#precioWeb').prop('value',a['colorProd']['PreciosSap'].Precio);
	
	                  $('#ivaWeb').removeAttr('value');
	                  $('#ivaWeb').prop('value',a['colorProd']['PreciosSap'].MontoIVA);
	
	                  $('#porcDesc').prop( 'disabled', false );
	                  $('#porcDesc').removeAttr('value');
	                  $('#porcDesc').prop('value','0,00');
	           
	                  $('#precioDesc').removeAttr('value');
	                  $('#precioDesc').prop('value',a['colorProd']['PreciosSap'].Precio);
	
	                  $('#totalWeb').removeAttr('value');
	                  $('#totalWeb').prop('value',a['colorProd']['PreciosSap'].Total);
	          }



                  //Fin Carga Precios Web 
                 
                  
                  
              }
            });
            

        });

    function existeUrl(url) {
       var http = new XMLHttpRequest();
       http.open('HEAD', url, false);
       http.send();
       if (http.status==200)
          return true;
        else
          return false;

    }



    function submitForm()
    {
        $('#load').attr('action','../pages/processLoadProduct.php');
        $('#load').submit();

    }

    function backForm()
    {
        $('#load').attr('action','../pages/consultProducts.php');
        $('#load').submit();

    }


    $( document ).ready(function() {

       
        var val =<?php echo $val?>;

        if (val == 1)
          $('[data-remodal-id=guardar]').remodal().open();
        
    });

    function calculadora()
    {

      var valP = $('#precioWeb').val();
      precio = valP.replace(".","");
      precio = precio.replace(",",".");
      var valD = $('#porcDesc').val();
      porDes = valD.replace(".","");
      porDes =porDes.replace(",",".");
     

      var precioDes = parseFloat(precio) -((parseFloat(precio) /100) * parseFloat(porDes));
      var iva = precioDes * 0.12;
      var total = precioDes + iva;

      
      //console.log ('precio:  '+ precio +' PorcDesc:  '+ porDes+'  precioDes:  '+ precioDes.format(2, 3, '.', ',') +' iva : '+ iva.format(2, 3, '.', ',') +'  total:  '+total.format(2, 3, '.', ','));

      $('#precioDesc').prop('value',precioDes.format(2, 3, '.', ','));
      $('#ivaWeb').prop('value',iva.format(2, 3, '.', ','));
      $('#totalWeb').prop('value',total.format(2, 3, '.', ','));
     

    }

    Number.prototype.format = function(n, x, s, c) {
      var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
          num = this.toFixed(Math.max(0, ~~n));

      return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    };

      function llenarNoImagen(codigoProducto,codigoColor) {
          var parametros={
              'p_cod_prod':codigoProducto,
              'p_cod_color':codigoColor
          };
          $.ajax({
              type: "POST",
              url: "../../admin/index.php?service=productoservices&metodo=InsertarNoImagen",
              data:JSON.stringify(parametros),
              contentType : "application/json"
          }
              )

      }
      function eliminarNoImagen(codigoProducto,codigoColor) {
          var parametros={
              'p_cod_prod':codigoProducto,
              'p_cod_color':codigoColor
          };
          $.ajax({
              type: "POST",
              url: "../../admin/index.php?service=productoservices&metodo=EliminarNoImagen",
              data:JSON.stringify(parametros),
              contentType : "application/json"
          }
              )

      }


    </script>
     
  </body>
</html>