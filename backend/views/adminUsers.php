<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Administrar Usuarios Registrados</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                   
                      <!-- Tabla de usuarios -->

                      <table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>Nombre</th> 
                              <th>Correo</th>
                              <th>Estado</th> 
                              <th>Último acceso</th>
                              <th>Ver</th> 
                              <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                         <?php foreach ($usuarios as $f=>$value) { ?>
                            <tr class="estatus<?php print_r(($usuarios[$f]['estatus'])); ?>" >
                              <input name="estatus" type="hidden" value="<?php print_r(($usuarios[$f]['estatus'])); ?>"/>
                              <td><?php print_r(ucfirst($usuarios[$f]['Nombre'])); ?> <?php print_r(ucfirst($usuarios[$f]['Apellido'])); ?></td> 
                              <td id="usuario"><?php print_r(ucfirst($usuarios[$f]['Usuario'])); ?></td>
                              <td><?php print_r(($usuarios[$f]['desestado'])); ?></td>
                              <td><?php print_r(($usuarios[$f]['fecha_acceso'])); ?></td>
                              <td>
                                <a href="" class="btn btn-info btn-xs" data-toggle="modal" data-target="#verCompras">Ver Compras</a>
                                <a href="" class="btn btn-info btn-xs">Ver Reservas</a>
                              </td> 
                              <td class="text-center">
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal"><span name="edit" id="<?php print_r($usuarios[$f]['IdUser']); ?>" class="glyphicon glyphicon-pencil"></span></a>
                              
                              <?php if (($usuarios[$f]['estatus'])==1) { ?>
                              <a href="" class="btn btn-xs btn-warning" data-remodal-target="blockUser"><span name="bloquear" class="glyphicon glyphicon-ban-circle"></span></a>
                              <?php }else{ ?>
                              <a class="btn btn-xs btn-success"><span name="desbloquear" class="glyphicon glyphicon-ok-circle"></span></a>
                              <?php } ?>
                              <a href="" class="btn btn-xs btn-danger" data-remodal-target="deleteUser" ><i name="delete" class="fa fa-trash" aria-hidden="true"></i></a>
                              </td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>

                    <!-- //Tabla de usuarios -->


                      

                      <!-- Remodal Bloquear usuario-->
                      <div class="remodal" data-remodal-id="blockUser">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Confirme</h1>
                        <p>​¿Desea bloquear al usuario?</p>
                        <br>
                        <button id="confirm_bloq" class="btn btn-success">Si</button>
                        <button data-remodal-action="cancel" class="btn btn-danger">No</button>
                      </div>
                      <!-- /Remodal  -->

                      <!-- Remodal Borrar usuario-->
                      <div class="remodal" data-remodal-id="deleteUser">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Confirme</h1>
                        <p>​¿Desea eliminar al usuario?</p>
                        <br>
                        <button id="confirm" class="btn btn-success">Si</button>
                        <button data-remodal-action="cancel" class="btn btn-danger">No</button>
                      </div>
                      <!-- /Remodal  -->

                      <!-- Modal para ver compras-->
                      <div class="modal fade bs-example-modal-lg" id="verCompras"  tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Editar usuario</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-condensed" style="border-collapse:collapse;">

                                  <thead>
                                      <tr><th>&nbsp;</th>
                                          <th>Orden Número</th>
                                          <th>Fecha</th>
                                          <th>Monto</th>
                                          <th>Estatus</th>
                                      </tr>
                                  </thead>

                                    <tbody>
                                        <tr data-toggle="collapse" data-target="#demo1" class="accordion-toggle">
                                                  <td><button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button></td>
                                            <td>OBS Name</td>
                                            <td>OBS Description</td>
                                            <td>hpcloud</td>
                                            <td>nova</td>
                                        </tr>
                                        <tr>
                                            <td colspan="12" class="hiddenRow">
                                              <div class="accordian-body collapse text-left" id="demo1"> 
                                                <p>
                                                  Zapatos de paseo<br>
                                                  Juguete para niño <br>
                                                  Mesa de dibujo

                                                </p>
                                              </div> 
                                            </td>
                                        </tr>
                                        <tr data-toggle="collapse" data-target="#demo2" class="accordion-toggle">
                                                <td><button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button></td>
                                                <td>OBS Name</td>
                                            <td>OBS Description</td>
                                            <td>hpcloud</td>
                                            <td>nova</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="hiddenRow"><div id="demo2" class="accordian-body collapse">Demo2</div></td>
                                        </tr>
                                        <tr data-toggle="collapse" data-target="#demo3" class="accordion-toggle">
                                            <td><button class="btn btn-default btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button></td>
                                            <td>OBS Name</td>
                                            <td>OBS Description</td>
                                            <td>hpcloud</td>
                                            <td>nova</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="hiddenRow"><div id="demo3" class="accordian-body collapse">Demo3</div></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->

                      <!-- Modal para editar usuario -->
                      <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Editar usuario</h4>
                              <input type="hidden" name="id_user" />
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Nombre</label>
                                  <input type="text" class="form-control" id="editnombre" placeholder="">
                                </div>
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Apellido</label>
                                  <input type="text" class="form-control" id="editapell" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Correo</label>
                                  <input type="text" class="form-control" id="editemail" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Estado</label>
                                  <select id="editestado" class="form-control">
                                  <?php foreach ($estados as $f=>$value) { ?>
                                  <option value="<?php print_r(ucfirst($estados[$f]['DesEstado'])); ?>" id="<?php print_r($estados[$f]['CodEstado']); ?>"><?php print_r(ucfirst($estados[$f]['DesEstado'])); ?> </option>
                                <?php } ?>
                                </select>
                                </div>
                              </form>
                            </div>
                            <div id="message-agregar" class="col-md-12 text-center"></div>
                            <div class="modal-footer">
                              <button type="submit"  id="ActualizarUsuario"  class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->

                  </div>
                </div>
              </div>
            <!-- /Opcion  -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>
    <script src="../js/remodal/remodal.js"></script>

      <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/datatables.scroller.min.js"></script>

    <script src="../js/views/usuarios.js"></script>

  </body>
</html>