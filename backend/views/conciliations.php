<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

            <!-- Opcion Sincronizar -->
            <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Carga de estados de cuentas bancarios</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <form class="form-horizontal">


                      <!-- Select Basic -->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="selectbasic">Seleccione el número de cuenta</label>
                        <div class="col-md-8">
                          <select id="selectbasic" name="selectbasic" class="form-control">
                            <option value="1">Option one</option>
                            <option value="2">Option two</option>
                          </select>
                        </div>
                      </div>

                      <!-- Text input-->
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Período de estado de cuenta</label>  
                        <div class="col-md-4">
                          <div class="input-daterange input-group" id="datepicker">
                              <input type="text" class="input-sm form-control" name="start" />
                              <span class="input-group-addon">al</span>
                              <input type="text" class="input-sm form-control" name="end" />
                          </div>
                          
                        </div>
                      </div>

                      <!-- File Button --> 
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="filebutton">Archivo de estado de cuenta</label>
                        <div class="col-md-4">
                          <input id="filebutton" name="filebutton" class="input-file" type="file">
                        </div>
                      </div>

                      <label>Vista Previa</label>

                      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="preview" width="100%">
                        <thead>
                          <tr>
                            <th>Fecha</th>
                            <th>Concepto</th>
                            <th>Referencia</th>
                            <th>Cargo</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>20/03/2016</td>
                            <td>Trans otros bancos</td>
                            <td>2986</td>
                            <td>30.256 Bs</td>
                          </tr>
                          <tr>
                            <td>15/03/2016</td>
                            <td>Cheque</td>
                            <td>98563</td>
                            <td>15.369 Bs</td>
                          </tr>
                          <tr>
                            <td>10/03/2016</td>
                            <td>Transf mismo bancos</td>
                            <td>8596</td>
                            <td>13.302 Bs</td>
                          </tr>
                          <tr>
                            <td>05/03/2016</td>
                            <td>Master XX02</td>
                            <td>78596</td>
                            <td>150.258 Bs</td>
                          </tr>
                        </tbody>
                      </table>

                      <div class="clearfix"></div>
                      
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-success">Aceptar</button>
                        </div>
                      </div>

                    </form>

                  </div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

<script>
  $(document).ready(function() {

    // Date range
    $('.input-daterange').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
    todayHighlight: true
    });

    // Inicializar tabla de vista previa
    $('#preview').DataTable({
      "bFilter": false,
      "sDom": 'Rfrtlip',
    });

});
</script>

  </body>
</html>