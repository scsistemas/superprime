<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Crear nueva Categoría</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <div id="newCategory" > <!--Changes form to div (evitar variables GET) -->
                      <div class="form-inline">
                        <div class="form-group">
                          <label for="nombre">Nombre</label>
                          <input name="nombre" type="text" class="form-control" id="nombre" require>
                        </div>
                        <div class="form-group">
                          <label for="principal">Página principal</label>
                            <div class="checkbox">
                              <label>
                                <input name="principal" value="1" id="principal" type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="destacado">Destacado</label>
                            <div class="checkbox">
                              <label>
                                <input name="destacado" value="1" id="destacado" type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_desde" name="destacado_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_hasta" name="destacado_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        
                       <div id="destacado_message" class="col-md-12 text-center"></div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="discount">Descuento (%)</label>
                            <input name="descuento" id="descuento" type="number" min="1" max="100" maxlength="2" onchange="changeHandler(this)" step="0.5" class="form-control numeric" style="width:70px">                     
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_desde" name="descuento_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_hasta" name="descuento_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        
                       <div id="descuento_message" class="col-md-12 text-center"></div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button id="guardar" type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>
                    </div> <!-- Form to div -->

                  </div>
                       <div id="message" class="col-md-12 text-center"></div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>
<script src="../js/views/newCategory.js"></script>
  </body>
</html>