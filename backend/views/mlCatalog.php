<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div id="test" name="test"></div>
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <form id="formML" action="" method="post">
          <input id="codPro" name="codPro" type="hidden" value="<?php echo($codPro);?>" />
          <input id="path" name="path" type="hidden" value="<?php echo($catSelect);?>" />
          <input id="hoja" name="hoja" type="hidden" value="<?php echo($hoja);?>" />
          <input id="publicado" name="publicado" type="hidden" value="<?php echo($disabled);?>" />
          <div class="right_col" role="main">

            <div class="row">

              <!-- Title -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                      <h2>Publicación en Mercado Libre</h2>
                      <div class="clearfix"></div>
                    </div>
                  <div class="x_content">
                    <div class="bs-example" data-example-id="simple-jumbotron">

                        <!-- Filtros -->
                        <fieldset style="font-size:12px">
                          <div class="form-inline">
                            <div class="form-group">
                              <label for="ex4">Plan</label>
                              <select id= "planes" class="select2_single form-control" tabindex="-1" <?php if($disabled == 1){echo("disabled");}?>>
                                <option value="">Seleccione...</option>
                                <?php 
                                  foreach ($respPlanes as $valor){
                                ?>
                                    <option value="<?php echo($valor['id']); ?>" <?php if($valor['id'] == $plan){echo("selected");}?> ><?php echo($valor['name']); ?></option>
                                <?php
                                  }
                                ?>
                              </select>
                            </div>

                            <div class="form-group hover">
                              <label class="control-label hover">
                                Publicar <input id="publicacion" type="checkbox" <?php if($publicarML == 'S'){echo('checked="checked"');}?>class="js-switch" />
                              </label>
                            </div>
                          </div>
                        </fieldset>
                        <!-- /Filtros -->

                        <div class="divider-dashed"></div>
                        
                        <!-- Categorias -->
                        <label style="margin-left: 9px;">Categorias</label>

                        <div id="divCategorias" class="categoriesBox row" style="overflow-x:scroll">
                          <?php echo($divCategorias);?>
                        </div>
                        <span></span>
                        <br>

                        <div class="divider-dashed"></div>
                        <br>
                        <!-- /Categorias -->
                        <div id="botones" class="text-center">
                        <?php
                          if($disabled == 1 && $publicarML == 'N'){
                        ?>
                            <p>Se eliminaran las publicaciones en Mercado Libre que est&eacute;n asociadas. Est&aacute; seguro de eliminar las publicaciones?</p>
                            <a href="../pages/catalogAdmin.php" class="btn btn-warning">Regresar</a>
                            <input type="button" name="" value="Guardar" class="btn btn-primary" onmousedown="guardar()"/>
                        <?php
                          }else{
                        ?>
                            <a href="../pages/catalogAdmin.php" class="btn btn-warning">Regresar</a>
                            <input type="button" name="" value="Guardar" class="btn btn-primary" onmousedown="guardar()"/>
                        <?php
                          }
                        ?>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Title -->
            </div>
          </div>
        </form>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>
    
    <?php include('../includes/scripts.php') ?>
    <!-- Switchery -->
    <script src="../js/switchery/switchery.min.js"></script>
    <!-- Remodal -->
    <script src="../js/remodal/remodal.js"></script>
    <script>

      $('#publicacion').on('click', function () {
        if($('#publicacion').is(':checked')){
          $('#botones').html('');
          $('#botones').html('<a href="#" class="btn btn-warning">Regresar</a>' + 
                             '<input type="button" name="" value="Guardar" class="btn btn-primary" onmousedown="guardar()"/>');
        }else{
          if($('#publicado').val() == 1){
            $('#botones').html('');
            $('#botones').html('<p>Se eliminaran las publicaciones en Mercado Libre que est&eacute;n asociadas. ' + 
                               'Est&aacute; seguro de eliminar las publicaciones?</p>' + 
                               '<a href="../pages/catalogAdmin.php" class="btn btn-warning">Regresar</a>' + 
                               '<input type="button" name="" value="Guardar" class="btn btn-primary" onmousedown="guardar()"/>');
          }else{
            $('#botones').html('');
            $('#botones').html('<a href="../pages/catalogAdmin.php" class="btn btn-warning">Regresar</a>' + 
                               '<input type="button" name="" value="Guardar" class="btn btn-primary" onmousedown="guardar()"/>');
          }
        }
      });

      function buscarValor(idSelector){
        var nroDiv = 0;
        $('#divCategorias').next('span').html('<img src="../images/cargando.gif"/> Cargando...');

        $('#divCategorias').children('div').each(function(i) { 
          nroDiv = $(this).attr('id').substring(6);
          if(nroDiv > idSelector){
            $('#div_cs'+nroDiv).remove();
          }
        });
        $('#hoja').val(0);

        var parametros = {
          "idCategoria" : $('#cs'+idSelector).val()
        };

        $.ajax({
          data:  parametros,
          url:   '../ajax/ConsultarCategoriasML.php',
          type:  'get',
          dataType: 'json',
          success:  function (a) {
            
            var data = '', i = 0, split;

            if(a.length > 1){
              data = '<div id="div_cs' + (idSelector+1) + '" class="col-lg-4" style="float:left">';
              data += '<select id="cs' + (idSelector+1) + '" class="form-control" name="category" size="5" onchange="buscarValor(' + (idSelector+1) + ', &quot;&quot;)">';
              for(i=0; i< a.length; i++){
                if(i == 0){
                  $('#path').val(a[i]);
                }else{
                  split = a[i].split(';'); 
                  data += ' <option value="' + split[0] + '" >' + split[1] + '</option>';
                }
              }
              
              data += '</select>';
              data += '</div>';

              $("#divCategorias").append(data);  
              $('#divCategorias').next('span').html('');
            }else{
              $('#path').val(a[0]);
              $('#divCategorias').next('span').html('');
              $('#divCategorias').next('span').html('<p>Has llegado al final de las categorias disponibles</p>');
              $('#hoja').val(1);
            }
          }
        });
      }

      function guardar(){
        var parametros, categorias, id, plan, publicacion, textoRespuesta;

        if($('#hoja').val() == 1){
          if($('#publicacion').is(':checked')){
            publicacion = "S";  
          }else{
            publicacion = "N";  
          }
          
          plan = $('#planes').val();
          id = $('#codPro').val();
          categorias = $('#path').val();

          if(plan != ''){
            parametros = {
              'id' : id,
              'categorias' : categorias,
              'plan' : plan,
              'pub' : publicacion
            }
            
            $('#divCategorias').next('span').html('<img src="../images/cargando.gif"/> Guardando...');

            $.ajax({
              data:  parametros,
              url:   '../ajax/guardarPublicacionML.php',
              type:  'post',
              dataType: 'json',
              success:  function (a) {
                
                $('#divCategorias').next('span').html('');
                //$('#test').html(a);

                $('#divCategorias').next('span').html(a.message);
              }
            });
          }else{
            $('#divCategorias').next('span').html('<p>Debes seleccionar un plan para publicar...</p>');
          }
        }else{
          $('#divCategorias').next('span').html('<p>Debes llegar hasta la categoria más baja para poder publicar, sigue seleccionando categorias...</p>');
        }
      }
  </script>
  </body>
</html>