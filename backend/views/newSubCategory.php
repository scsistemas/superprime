<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">
          <div class="col-lg-4">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Arbol de Categorías</h2>
                  <div class="clearfix"></div>
               </div> 

               <div class="x_content">
                 <div id="container">

                    <ol id='auto-checkboxes' data-name='foo'>
                      <li class='expanded' data-value='0'>Todas
                        <ol>
                        
                        <?php foreach ($categoriasSlider['Categorias'] as $c) { ?>
                          <li data-value='<?php print_r($categoriasSlider['Categorias'][$index]['Id'])?>'><?php print_r($categoriasSlider['Categorias'][$index]['Nombre']); ?>
                          <ol>
                          <?php foreach (($categoriasSlider['Categorias'][$index]['subCategorias']) as $sub){ ?>
                                <li data-name='baz' data-value='<?php echo $index?>'> - <?php print_r($sub['Nombre']); ?></li>
                          <?php } ?>
                            </ol>
                          <?php  $index++; } ?>
                          </li>
                        </ol>
                      </li>
                    </ol>
                  </div>
               </div>   
            </div>
          </div>

                        
            <!-- Opcion Sincronizar -->
            <div class="col-md-6 col-sm-12 col-xs-12 col-lg-8">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Crear nueva SubCategoría</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <div id="newCategory" > <!--Changes form to div (evitar variables GET) -->
                      <div class="form-inline">
                        <div class="form-group">
                          <label for="nombre">Nombre</label>
                          <input name="nombre" type="text" class="form-control" id="nombre" require>
                        </div>
                        <div class="form-group">
                          <label for="principal">Página principal</label>
                            <div class="checkbox">
                              <label>
                                <input name="principal" value="1" id="principal" type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="destacado">Destacado</label>
                            <div class="checkbox">
                              <label>
                                <input name="destacado" value="1" id="destacado" type="checkbox" class="flat" checked="checked"> 
                              </label>
                            </div>                      
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_desde" name="destacado_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="destacado_hasta" name="destacado_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        
                       <div id="destacado_message" class="col-md-12 text-center"></div>
                      </div>

                      <div class="form-inline">
                         <div class="form-group">
                          <label for="discount">Descuento (%)</label>
                            <input name="descuento" id="descuento" type="number" min="1" max="100" maxlength="2" onchange="changeHandler(this)" step="0.5" class="form-control numeric" style="width:70px">                     
                        </div> 
                        <div class="form-group" style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_desde" name="descuento_desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group"  style="width: 30%;">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="descuento_hasta" name="descuento_hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        
                       <div id="descuento_message" class="col-md-12 text-center"></div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button id="guardar" type="submit" class="btn btn-success">Guardar</button>
                        </div>
                      </div>
                       <div id="message" class="col-md-12 text-center"></div>
                  </div>
                      
                </div>
                      <br>
                      <br>
                      <br>  
                      <table class="table table-bordered"> 
                        <thead> 
                          <tr> 
                            <th></th>
                            <th>Nombre del Filtro</th> 
                            <th>Tipo de Campo</th> 
                            <th>Acciones</th> 
                          </tr> 
                        </thead> 
                        <tbody> 
                          <?php foreach ($filtros as $f=>$value) { ?>
                          <tr> 
                            <td>
                               <input class="filtro" type="checkbox" id="<?php print_r($filtros[$f]['id']); ?>" name="filtro" value="<?php print_r($filtros[$f]['id']); ?>"/>
                            </td>
                            <td><?php print_r(ucfirst($filtros[$f]['nombre'])); ?></td> 
                            <td><?php print_r(ucfirst($filtros[$f]['tipo_elemento'])); ?></td> 
                            <td>
                            <a class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-remove" id="eliminar"></span> Eliminar</a>
                            <!--<a href="" class="btn  btn-xs btn-primary"><span class="glyphicon glyphicon-pencil" id="editar"></span></a>
                            <a href="" class="btn  btn-xs btn-info"><span class="glyphicon glyphicon-info-sign" id="ver"></span></a>-->
                            </td> 
                          </tr> 
                          <?php } ?>
                        </tbody> 
                      </table>

                      <div class="col-md-12 text-center">
                          <a href="" class="btn btn-success" data-toggle="modal" data-target="#myModal">Agregar Nueva</a>
                        </div>

                    </div>

                    <div id="message-filtro" class="col-md-12 text-center"></div>

                    <!-- Modal para agregar un nuevo filtro a la tabla -->
                    <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Nuevo filtro</h4>
                          </div>
                          <div class="modal-body">
                            <div>
                              <div class="form-group">
                                <label for="nameFilter">Nombre del Filtro</label>
                                <input type="text" class="form-control" id="nameFilter" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="typeElement">Tipo de elemento</label>
                                <select id="elemento" class="form-control">
                                  <option>Radio</option>
                                  <option>Checkbox</option>
                                  <option>Lista de opciones</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="option1">Opción 1</label>
                                <input type="text" class="form-control" id="option1" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="option2">Opción 2</label>
                                <input type="text" class="form-control" id="option2" placeholder="">
                              </div>
                              <div class="form-group">
                                <label for="option3">Opción 3</label>
                                <input type="text" class="form-control" id="option3" placeholder="">
                              </div>
                            </div>
                          </div>
                          <div id="message-agregar" class="col-md-12 text-center"></div>
                          <div class="modal-footer">
                          <button type="submit"  id="InsertarFiltro"  class="btn btn-primary">Guardar</button>
                          </div>
                          
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->



                  </div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

    <!-- Proton tree checkboxes -->
    <script src="../js/bonsai/jquery.bonsai.js"></script>
    <script src="../js/bonsai/jquery.qubit.js"></script>
    <script src="../js/views/newSubCategory.js"></script>

  </body>
</html>