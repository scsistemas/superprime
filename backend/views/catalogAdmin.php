﻿<?php include '../includes/header.php';?>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">

        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

              <!-- Resultados de Sincronizacion -->
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Consulta de Productos</h2>
                    <ul class="navbar-right">
                      <li><a href="sync.php" class="btn btn-info btn-sm">Volver a sincronización</a></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30"></p>

                      <table id="datatable" class="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>Código</th>
                          <th>Marca</th>
                          <th>Descripción SAP</th>
                          <th>Tipo de Producto</th>
                          <th>Género</th>
                          <th>Fecha de Sincronización</th>
                          <th>Publicado en Web</th>
                          <th>Publicado en ML</th>
                          <th>Opciones</th>
                        </tr>
                      </thead>

                            <!-- /Construcción del TBODY vía ajax -->
                    </table>

                    </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Resultados de Sincronizacion -->

          </div>



        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>

    <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../js/jszip/dist/jszip.min.js"></script>
    <script src="../js/pdfmake/build/pdfmake.min.js"></script>
    <script src="../js/pdfmake/build/vfs_fonts.js"></script>

    <!-- Datatables -->
    <script>

      $(document).ready(function() {

        $('#datatable').DataTable( {
           "language": {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    //"sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                          "sFirst":    "Primero",
                          "sLast":     "Último",
                          "sNext":     "Siguiente",
                          "sPrevious": "Anterior"
                    },
                    "oAria": {
                          "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
            },
            "processing": true,
            "serverSide": true,
            "sort": false,
            "searching": false,    //deshabilitar el filtro de búsqueda
            "bFilter" : false,    //deshabilitar el length menu
            "bLengthChange": false, //deshabilitar el length menu
            "pageLength": 5, //definir número de registros por páginas

            "ajax": "../ajax/SincronizarProductos.php?IdSinc=<?php echo($resultSincronizar['IdSinc']);?>"


        } );


      });
    </script>

  </body>
</html>