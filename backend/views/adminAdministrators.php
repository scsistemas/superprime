<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">


            <!-- Opcion Sincronizar -->
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Administrar Usuarios Administradores</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<!--
                    <div class="form-inline">
                        <div class="form-group">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="desde" name="desde" placeholder="Desde" >
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" class="form-control border" id="hasta" name="hasta" placeholder="Hasta" >
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                             <input type="text" class="form-control" id="search" placeholder="Introduce un término">
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="input-group">
                             <button type="submit" class="btn btn-primary">Buscar</button>
                          </div>
                        </div>
                      </div>-->

                   

                      <table id="users" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                              <th>Nombre</th> 
                              <th>Correo</th>
                              <th>Teléfono</th> 
                              <th>Nivel de acceso</th>
                              <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                         <?php foreach ($administradores as $f=>$value) { ?>
                            <tr>
                              <td><?php print_r(ucfirst($administradores[$f]['Nombre'])); ?> <?php print_r(ucfirst($administradores[$f]['Apellido'])); ?></td> 
                              <td><?php print_r(ucfirst($administradores[$f]['Usuario'])); ?></td>
                              <td><?php print_r(($administradores[$f]['telcel'])); ?></td>
                              <td id="acceso_td"><?php print_r(ucfirst($administradores[$f]['acceso'])); ?></td>
                              <td class="text-center">
                              <a href="" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#edituser"><span name="edit" id="<?php print_r($administradores[$f]['IdUser']); ?>" class="glyphicon glyphicon-pencil"></span></a>
                              <a href="" class="btn btn-xs btn-danger"  data-remodal-target="deleteUser" name="delete" id="<?php print_r($administradores[$f]['IdUser']); ?>" ><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr>

                          <?php } ?>
                        </tbody>
                    </table>
                    
                    <div class="clearfix"></div>
                    <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <a href="" class="btn btn-success" data-toggle="modal" data-target="#addUser">Agregar Usuario</a>
                        </div>
                      </div>

                    <!-- //Tabla de usuarios -->


                      <!-- Remodal -->
                      <div class="remodal" data-remodal-id="deleteUser">
                        <button data-remodal-action="close" class="remodal-close"></button>
                        <h1>Confirme</h1>
                        <p>​¿Desea eliminar al usuario?</p>
                        <br>
                        <button id="confirm" class="btn btn-success">Si</button>
                        <button data-remodal-action="cancel" class="btn btn-danger">No</button>
                      </div>
                      <!-- /Remodal  -->


                      
                      <!-- Modal para editar usuario -->
                      <div class="modal fade" id="edituser"  tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Editar usuario</h4>
                              <input type="hidden" name="id_user" />
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Nombre</label>
                                  <input type="text" class="form-control" id="editnombre" placeholder="" >
                                </div>
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Apellido</label>
                                  <input type="text" class="form-control" id="editapell" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Correo</label>
                                  <input type="text" class="form-control" id="editcorreo" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label style="display: block;" for="nameFilter">Cedula</label>
                                  <select id="editipo_cedula" class="cedula">
                                  <option value="V">V</option>
                                  <option value="E">E</option>
                                  </select>
                                  <input type="text" class="cedula" id="editcedula" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Contraseña temporal</label>
                                  <input type="password" class="form-control" id="editcontra" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Teléfono</label>
                                  <input type="text" class="form-control" id="editelf" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Nivel de Acceso</label>
                                  <select id="editacceso" class="form-control">
                                  <?php foreach ($accesos as $f=>$value) { ?>
                                  <option value="<?php print_r(ucfirst($accesos[$f]['nombre'])); ?>" id="<?php print_r($accesos[$f]['id']); ?>"><?php print_r(ucfirst($accesos[$f]['nombre'])); ?> </option>
                                <?php } ?>
                                </select>
                                </div>
                              </form>
                            </div>
                            <div id="message-agregar" class="col-md-12 text-center"></div>
                            <div class="modal-footer">
                              <button type="submit"  id="ActualizarUsuario"  class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->



                      <!-- Modal para añadir usuario -->
                      <div class="modal fade" id="addUser"  tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">Añadir usuario</h4>
                            </div>
                            <div class="modal-body">
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Nombre</label>
                                  <input type="text" class="form-control" id="nombreUser" placeholder="">
                                </div>
                              <form>
                                <div class="form-group">
                                  <label for="nameFilter">Apellido</label>
                                  <input type="text" class="form-control" id="LastnameUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Correo</label>
                                  <input type="text" class="form-control" id="correoUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label style="display: block;" for="nameFilter">Cedula</label>
                                  <select id="tipo_cedula" class="cedula">
                                  <option>V</option>
                                  <option>E</option>
                                  </select>
                                  <input type="text" class="cedula" id="cedulaUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Contraseña temporal</label>
                                  <input type="password" class="form-control" id="contraUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Teléfono</label>
                                  <input type="text" class="form-control" id="telUser" placeholder="">
                                </div>
                                <div class="form-group">
                                  <label for="nameFilter">Nivel de Acceso</label>
                                  <select id="acceso" class="form-control">
                                  <?php foreach ($accesos as $f=>$value) { ?>
                                  <option id="<?php print_r($accesos[$f]['id']); ?>"><?php print_r(ucfirst($accesos[$f]['nombre'])); ?> </option>
                                <?php } ?>
                                </select>
                                </div>
                              </form>
                            </div>
                            <div id="message-agregar" class="col-md-12 text-center"></div>
                            <div class="modal-footer">
                              <button type="submit"  id="InsertarUsuario"  class="btn btn-primary">Guardar</button>
                            </div>
                          </div>
                        </div>
                      </div><!-- /.modal -->

                  </div>
                </div>
              </div>
            <!-- /Opcion  -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

    <?php include('../includes/scripts.php') ?>
    <script src="../js/remodal/remodal.js"></script>

      <!-- Datatables -->
    <script src="../js/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../js/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../js/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../js/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../js/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../js/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../js/datatables.net-scroller/js/datatables.scroller.min.js"></script>
    <script src="../js/views/administradores.js"></script>
  </body>
</html>