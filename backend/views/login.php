<?php include '../includes/header.php'; ?>

  <body style="background:#607D8B;">
    <div class="">
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>

      <div id="wrapper">
        <div id="login" class=" form">
          <section class="login_content">
            <form action="../pages/login.php" method="post" role="form" data-toggle="validator">

              <h1>SuperPrime.com</h1>
              <div>
                <input type="email" id="email" name="email" class="form-control" placeholder="Username"  required />
              </div>
              <div>
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" required />
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-default submit">Ingresar</button>
                <br>
                <a class="reset_pass" href="#">Olvidó su contraseña?</a>
              </div>
              <div class="clearfix"></div>

            </form>
          </section>
        </div>
 	    <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p id="msj"></p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>
	
      </div>
    </div>


    <?php include('../includes/scripts.php') ?>

    <?php if (isset($user)){ ?>
   <script type="text/javascript">
    $( document ).ready(function() {
        <?php if (isset($_SESSION['userid'])){ ?>
            window.location='principal.php';
        <?php }else{?>
            $('#msj').html('Usuario y/o Clave invalida');
            $('[data-remodal-id=reporte]').remodal().open();
        <?php }?>
    });
    </script>
    <?php }?>
  </body>
</html>