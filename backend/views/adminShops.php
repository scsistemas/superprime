<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

          <div class="col-lg-4">
            <div class="x_panel">
               <div class="x_title">
                  <h2>Razones Sociales</h2>
                  <div class="clearfix"></div>
               </div> 

               <div class="x_content">
                  <ul id="razonS" class="listSelection">
                  <?php foreach ($razonsocial as $f=>$value) { ?>
                    <li id="<?php print_r($razonsocial[$f]['id']); ?>"><a href="#"><?php print_r(ucfirst($razonsocial[$f]['descripcion'])); ?></a></li>
                    <?php } ?>
                  </ul>
               </div>   
            </div>
          </div>

            <div class="col-lg-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Estados</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <ul id="ciudades" class="listSelection">
                    </ul>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tiendas</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div id="tiendas">

                    </div>
                      <div class="text-center">
                      <br>
                        <a href="" class="btn btn-success" data-toggle="modal" data-target="#myModal">Agregar Nueva</a>
                      </div>


                        <!-- Modal para agregar un nueva tienda -->
                        <div class="modal fade" id="myModal"  tabindex="-1" role="dialog">
                          <div  class="modal-dialog modal-lg" role="document">
                            <div class="newShop" data-toggle="validator">

                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Editar Tienda</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-4 col-md-offset-2">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="codigo">Código</label>
                                          <input type="text" class="form-control numeric" id="codigo"  required>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="edittienda">Nombre</label>
                                          <input type="text" class="form-control" id="tienda" required>
                                        </div>
                                      </div>
                                    </div> 
                                    <div class="col-lg-6">

                                      <h1>Dirección</h1>

                                      <div class="form-horizontal">
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="estado">Estado</label>
                                          <div class="col-md-8">
                                            <select id="estado" name="estado" class="form-control" required>
                                              <?php foreach ($estados as $f=>$value) { ?>
                                                <option value="<?php print_r(ucfirst($estados[$f]['DesEstado'])); ?>" id="<?php print_r($estados[$f]['CodEstado']); ?>"><?php print_r(ucfirst($estados[$f]['DesEstado'])); ?> </option>
                                              <?php } ?>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="ciudad">Ciudad</label>
                                          <div class="col-md-8">
                                            <select id="ciudad" name="ciudad" class="form-control" required>
                                              
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="municipio">Municipio</label>
                                          <div class="col-md-8">
                                            <select id="municipio" name="municipio" class="form-control" required>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="parroquia">Parroquia</label>
                                          <div class="col-md-8">
                                            <select id="parroquia" name="parroquia" class="form-control" required>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="postal">Código Postal</label>
                                          <div class="col-md-8">
                                            <input id="postal" name="postal" type="text" class="form-control input-md" required>
                                          </div>
                                        </div>

                                        
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="web">Pagina Web</label>
                                          <div class="col-md-8">
                                            <input id="web" name="web" type="text" class="form-control input-md" required>
                                          </div>
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-lg-6">

                                      <div class="form-horizontal">
                                      <br><br>
                                         <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="direccion">Dirección</label>  
                                        <div class="col-md-8">
                                        <textarea class="form-control" id="direccion" name="direccion" required></textarea>
                                          
                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Email</label>  
                                        <div class="col-md-8">
                                        <input id="email" name="email" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="nombre">Encargado</label>  
                                        <div class="col-md-8">
                                        <input id="nombre" name="nombre" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>
                                       <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="telefono_1" style="padding: 7px 2px;">Teléfono 1</label>  
                                              <div class="col-md-8">
                                              <input id="telefono_1" name="telefono_1" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="telefono_2" style="padding: 7px 2px;">Teléfono 2</label>  
                                              <div class="col-md-8">
                                              <input id="telefono_2" name="telefono_2" type="text" class="form-control input-md">
                                                
                                              </div>
                                            </div>
                                      </div>
                                      
                                    </div>  
                                  </div>
                              </div>
                              <div id="message-agregar" class="col-md-12 text-center"></div>
                              <div class="modal-footer">
                                <button type="submit" id="Nueva" class="btn btn-primary">Guardar</button>
                              </div>

                              </div>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->





                        <!-- Modal para editar una tienda -->
                        <div class="modal fade" id="editShop"  tabindex="-1" role="dialog">
                          <div  class="modal-dialog modal-lg" role="document">
                            <div class="newShop" data-toggle="validator">

                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Editar Tienda</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                    <div class="col-lg-4 col-md-offset-2">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="codigotienda">Código</label>
                                          <input type="text" class="form-control numeric" id="codigotienda"  required disabled>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-lg-6">
                                      <div class="form-inline">
                                        <div class="form-group">
                                          <label for="edittienda">Nombre</label>
                                          <input type="text" class="form-control" id="edittienda" required>
                                        </div>
                                      </div>
                                    </div> 
                                    <div class="col-lg-6">

                                      <h1>Dirección</h1>

                                      <div class="form-horizontal">
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="editestado">Estado</label>
                                          <div class="col-md-8">
                                            <select id="editestado" name="estado" class="form-control" required>
                                              <?php foreach ($estados as $f=>$value) { ?>
                                                <option value="<?php print_r(ucfirst($estados[$f]['DesEstado'])); ?>" id="<?php print_r($estados[$f]['CodEstado']); ?>"><?php print_r(ucfirst($estados[$f]['DesEstado'])); ?> </option>
                                              <?php } ?>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="editciudad">Ciudad</label>
                                          <div class="col-md-8">
                                            <select id="editciudad" name="ciudad" class="form-control" required>
                                              
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="editmunicipio">Municipio</label>
                                          <div class="col-md-8">
                                            <select id="editmunicipio" name="municipio" class="form-control" required>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="editparroquia">Parroquia</label>
                                          <div class="col-md-8">
                                            <select id="editparroquia" name="parroquia" class="form-control" required>
                                            </select>
                                          </div>
                                        </div>

                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="editpostal">Código Postal</label>
                                          <div class="col-md-8">
                                            <input id="editpostal" name="editpostal" type="text" class="form-control input-md" required>
                                          </div>
                                        </div>

                                        
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="editweb">Pagina Web</label>
                                          <div class="col-md-8">
                                            <input id="editweb" name="editweb" type="text" class="form-control input-md" required>
                                          </div>
                                        </div>
                                        
                                      </div>
                                    </div>
                                    <div class="col-lg-6">

                                      <div class="form-horizontal">
                                      <br><br>
                                         <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="editdireccion">Dirección</label>  
                                        <div class="col-md-8">
                                        <textarea class="form-control" id="editdireccion" name="editdireccion" required></textarea>
                                          
                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="editemail">Email</label>  
                                        <div class="col-md-8">
                                        <input id="editemail" name="editemail" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="editnombre">Encargado</label>  
                                        <div class="col-md-8">
                                        <input id="editnombre" name="editnombre" type="text" placeholder="" class="form-control input-md" required>
                                          
                                        </div>
                                      </div>
                                       <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="edittelefono_1" style="padding: 7px 2px;">Teléfono 1</label>  
                                              <div class="col-md-8">
                                              <input id="edittelefono_1" name="edittelefono_1" type="text" class="form-control input-md" required>
                                                
                                              </div>
                                            </div>

                                            <!-- Text input-->
                                            <div class="form-group">
                                              <label class="col-md-4 control-label" for="edittelefono_2" style="padding: 7px 2px;">Teléfono 2</label>  
                                              <div class="col-md-8">
                                              <input id="edittelefono_2" name="edittelefono_2" type="text" class="form-control input-md">
                                                
                                              </div>
                                            </div>
                                      </div>
                                      
                                    </div>  
                                  </div>
                              </div>
                              <div id="message-agregar" class="col-md-12 text-center"></div>
                              <div class="modal-footer">
                                <button type="submit" id="Actualizar" class="btn btn-primary">Guardar</button>
                              </div>

                              </div>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->

                  </div>
                </div>
              </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

<script src="../js/switchery/switchery.min.js"></script>
<script src="../js/views/shops.js"></script>

</body>
</html>