﻿<?php include '../includes/header.php';?>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <div class="row">

            <!-- Title -->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_content">
                  <div class="bs-example" data-example-id="simple-jumbotron">
                      <h5 class="text-muted">
                      <i class="fa  fa-exclamation-circle"></i> Recuerde hacer una sincronización periódicamente para tener actualizada la base de datos de productos.<br><br>
                      <i class="fa fa-history" aria-hidden="true"></i> Última sincronización <?php echo $resultSincronizaciones[0]['fecha']?> por <strong><?php echo $resultSincronizaciones[0]['nombre']?></strong>
                      </h5>
                  </div>
                </div>
              </div>
            </div>
            <!-- /Title -->

            <!-- Opcion Sincronizar -->
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sincronización de Productos</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br>
                    <p>Este proceso le permite sincronizar manualmente los productos de SAP. Una vez que se ejecute el proceso, todos los productos que no se hayan sincronizado previamente serán agregados al Catálogo Web</p>
                    <br>
                    <p>Tome en cuenta que se realizará un proceso de sincronización automática que se ejecutará diariamente a las 12:00 am</p>
                    <form class="form-horizontal form-label-left" action="../pages/catalogAdmin.php">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i>  Sincronizar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            <!-- /Opcion Sincronizar -->

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

  </body>
</html>