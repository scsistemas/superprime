<?php
include '../includes/header.php';
?>


  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
       
        <!-- Sidebar -->
          <?php include '../includes/sidebarMenu.php'; ?>
        <!-- /Sidebar -->

        <!-- top navigation -->
          <?php include '../includes/topNavigation.php'; ?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">

          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-tag"></i> Total de Productos Sincronizados</span>
              <div class="count"><?php echo ($resultTotales['totalesProductos']['Sincronizados']); ?></div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-globe" aria-hidden="true"></i> Total de Productos en Web</span>
              <div class="count"><?php echo ($resultTotales['totalesProductos']['Publicados']);?></div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-shopping-bag" aria-hidden="true"></i> Total de Productos en MercadoLibre</span>
              <div class="count"><?php echo ($resultTotales['totalesProductos']['MercadoLibre']);?></div>
            </div>
            <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-camera" aria-hidden="true"></i> Total de Productos sin Foto</span>
              <div class="count"><?php echo ($resultTotales['totalesProductos']['SinFoto']);?></div>
            </div>

           </div>
          <!-- /top tiles -->

   



          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Actividad Reciente </h2>

                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                    <?php
                          foreach ($resultSincronizaciones  as $sin) {?>
                            <li>
                              <div class="block">
                                <div class="block_content">
                                  <h2 class="title">
                                      <a><?php echo "Fecha de Sincronización ".$sin['fecha'] ?></a>
                                  </h2>
                                  <div class="byline">
                                    <span>Por</span> <a><?php echo $sin['nombre'] ?></a>
                                  </div>
                                  <p class="excerpt">
                                  <?php echo "Cantidad de productos sincronizados ". $sin['cantProdSinc'] ?></p>
                                </div>
                              </div>
                            </li>

                          <?php
                          }
                          ?>

                      

                    </ul>
                  </div>
                </div>
              </div>
            </div>


          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php include('../includes/footer.php') ?>
        <!-- /footer content -->
      </div>
    </div>

<?php include('../includes/scripts.php') ?>

  </body>
</html>