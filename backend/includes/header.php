<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>​Modulo Administrativo de SuperPrime</title>
    
    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../css/iCheck/skins/square/aero.css" rel="stylesheet">

    <!-- Datatables -->
    <link href="../css/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../css/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../css/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../css/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../css/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

    <!-- Multiselect -->
    <link href="../js/multiselect/bootstrap-select.css" rel="stylesheet">
    
    <!-- Color Picker -->
    <link href="../css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

    <!-- Switchery -->
    <link href="../css/switchery/switchery.min.css" rel="stylesheet">
    
    <!-- fileinput -->
    <link href="../js/fileinput/css/fileinput.min.css" rel="stylesheet">
    
    <!-- Dropzone -->
    <link href="../js/dropzone/dropzone.css" rel="stylesheet">

    <!-- Remodal -->
    <link href="../js/remodal/remodal.css" rel="stylesheet">
    <link href="../js/remodal/remodal-default-theme.css" rel="stylesheet">

    <!-- bonsai for tree checkboxes -->
    <link href="../js/bonsai/jquery.bonsai.css" rel="stylesheet">

    <!-- Datepicker -->
    <link href="../js/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">

    <!--  -->


    <!-- Custom Theme Style -->
    <link href="../css/custom.css" rel="stylesheet">
  </head>
