
<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.php" class="site_title"><span>​Super Prime</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_info">
                <span>Bienvenido(a),</span>
                <h2 id="nameMenu"></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <div class="clearfix"></div>
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3></h3>
                <ul id="menu" class="nav side-menu">
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

          </div>
        </div> <!-- fin -->
 <script src="../js/jquery/jquery.min.js"></script>
<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<script>
     var CURRENT_URL = window.location.href.split('?')[0],
         $BODY = $('body'),
         $MENU_TOGGLE = $('#menu_toggle'),
         $SIDEBAR_MENU = $('#sidebar-menu'),
         $SIDEBAR_FOOTER = $('.sidebar-footer'),
         $LEFT_COL = $('.left_col'),
         $RIGHT_COL = $('.right_col'),
         $NAV_MENU = $('.nav_menu'),
         $FOOTER = $('footer');
     // Sidebar
     function loadmenu () {
         // TODO: This is some kind of easy fix, maybe we can improve this
         var setContentHeight = function () {
             // reset height
             $RIGHT_COL.css('min-height', $(window).height());

             var bodyHeight = $BODY.height(),
                 leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                 contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

             // normalize content
             contentHeight -= $NAV_MENU.height() + $FOOTER.height();

             $RIGHT_COL.css('min-height', contentHeight);
         };

         $SIDEBAR_MENU.find('a').on('click', function(ev) {
             var $li = $(this).parent();

             if ($li.is('.active')) {
                 $li.removeClass('active');
                 $('ul:first', $li).slideUp(function() {
                     setContentHeight();
                 });
             } else {
                 // prevent closing menu if we are on child menu
                 if (!$li.parent().is('.child_menu')) {
                     $SIDEBAR_MENU.find('li').removeClass('active');
                     $SIDEBAR_MENU.find('li ul').slideUp();
                 }

                 $li.addClass('active');

                 $('ul:first', $li).slideDown(function() {
                     setContentHeight();
                 });
             }
         });

         // toggle small or large menu
         $MENU_TOGGLE.on('click', function() {
             if ($BODY.hasClass('nav-md')) {
                 $BODY.removeClass('nav-md').addClass('nav-sm');

                 if ($SIDEBAR_MENU.find('li').hasClass('active')) {
                     $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
                 }
             } else {
                 $BODY.removeClass('nav-sm').addClass('nav-md');

                 if ($SIDEBAR_MENU.find('li').hasClass('active-sm')) {
                     $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
                 }
             }

             setContentHeight();
         });

         // check active menu
         $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

         $SIDEBAR_MENU.find('a').filter(function () {
             return this.href == CURRENT_URL;
         }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
             setContentHeight();
         }).parent().addClass('active');

         // recompute content when resizing
         $(window).smartresize(function(){
             setContentHeight();
         });

         // fixed sidebar
         if ($.fn.mCustomScrollbar) {
             $('.menu_fixed').mCustomScrollbar({
                 autoHideScrollbar: true,
                 theme: 'minimal',
                 mouseWheel:{ preventDefault: true }
             });
         }
     };
     // /Sidebar

     $.getJSON( "<?php echo  $urlWS?>service=userservices&metodo=ObtenerMenuUsuarioAdmin&p_id_usuario=<?php echo  $_SESSION['userid']?>")
       .done(function( json ) {
         var html = '';
         for (var i = 0; i < json.menus.length; i++) {
           html+='<li>';
           if (json.menus[i].link.length>0) {
             html += '<a href="' + json.menus[i].link + '">';
           }else{
             html += '<a>';
           }
           html+='<i class="'+json.menus[i].icono+'"></i> '+json.menus[i].nombre;
           var submenus = json.menus[i].submenus;
           if (submenus.length>0){
             html+='<span class="fa fa-chevron-down"></span>';
           }
           html+='</a>';
           if (submenus.length>0){
             html+='<ul class="nav child_menu">';
             for (var j = 0; j < submenus.length; j++) {
               html+='<li><a href="'+submenus[j].link+'"><i class="'+submenus[j].icono+'" aria-hidden="true"></i> '+submenus[j].nombre+'</a></li>';
             }
             html+='</ul>';
           }
           html+='</li>';
         }
         $("#menu").html(html);
           loadmenu();

       })
       .fail(function( jqxhr, textStatus, error ) {
         var err = textStatus + ", " + error;
         console.log( "Request Failed: " + err );
       });
 </script>