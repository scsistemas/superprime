<?php
	function insertEstado($nom, $idPais){
		
		$enlace = startConnection();
		
		$sql = 'INSERT INTO estado(pais_id, nombre, estatus) ';
		$sql .= 'VALUES('.$idPais.', "'.$nom.'", "ACT")';
		
		$resultado = queryExecution($sql, $enlace, 1);
		
		closeConnection($enlace);
		
		return $resultado;
	}
		
	function updateEstado($idEstado, $nom, $idPais){

		$enlace = startConnection();
		
		$sql = 'UPDATE estado SET nombre = "'.$nom.'", pais_id ='.$idPais.' ';
		$sql .= 'WHERE id='.$idEstado;
				
		$resultado = queryExecution($sql, $enlace, 0);
		
		closeConnection($enlace);
		
		return $resultado;
	}
	
	function updateEstatusXEstado($idEstado, $estatus){
		
		$enlace = startConnection();
		
		$sql = 'UPDATE estado SET estatus = "'.$estatus.'" WHERE id =' . $idEstado;
						
		$resultado = queryExecution($sql, $enlace, 0);
		
		closeConnection($enlace);
		
		return $resultado;
	}
	
	function deleteEstado($idEstado){
	
		$enlace = startConnection();
		
		$sql = 'UPDATE estado SET estatus = "INA" WHERE id =' . $idEstado;
						
		$resultado = queryExecution($sql, $enlace, 0);
		
		closeConnection($enlace);
		
		return $resultado;
	}
	
	function searchEstadoXId($idEstado){
	
		$enlace = startConnection();
		
		$sql = 'SELECT * FROM estado WHERE id ='.$idEstado;
						
		$resultado = queryExecution($sql, $enlace, 0);
		
		closeConnection($enlace);
		
		return $resultado;
	}
	
	function listEstado(){
		
		$enlace = startConnection();
		
		$sql = 'SELECT * FROM estado WHERE estatus = "ACT" ORDER BY id ASC';
						
		$resultado = queryExecution($sql, $enlace, 0);
		
		closeConnection($enlace);
		
		return $resultado;
	}
?>