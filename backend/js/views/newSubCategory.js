 $(document).ready(function() {
        

        $('#destacado_desde, #destacado_hasta, #descuento_desde, #descuento_hasta').datepicker({
            format: "yyyy-mm-dd",
            language: "es",
            todayHighlight: true
        });

        //Inicializar arbol de categorias
        $('#auto-checkboxes').bonsai({
          expandAll: true,
          checkboxes: false, // depends on jquery.qubit plugin
          createInputs: 'checkbox' // takes values from data-name and data-value, and data-name is inherited
        });

          //====== Inicializacion de variables =====\\
        $('#destacado').prop('checked', false);
        $('#principal').prop('checked', false);
        $('#destacado_hasta').prop( "disabled", true );
        $('#destacado_desde').prop( "disabled", true );
        $('#descuento_message').css( "margin", "10px" );
        $('#destacado_message').css( "margin", "10px" );
        $('#guardar').prop('disabled', true);
        $("#descuento_desde").prop('disabled', true); 
        $("#descuento_hasta").prop('disabled', true); 
        $('#destacado').val(0);
        //====== Inicializacion de variables =====\\

        String.prototype.capitalize = function() {
          return this.charAt(0).toUpperCase() + this.slice(1);
        }


  // --------------------------------------
	// Eliminar Filtro
	// -------------------------------------- 
    $('#eliminar').click(function(){
          var parametros = {
                  'id' : ($(this).closest('tr').find('.filtro').attr("id")),
                  'accion': 'EliminarFiltro'
                };

              $.ajax({
                  url: "../pages/newSubCategory.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message-filtro').addClass('alert-success');
                        $('#message-filtro').html(respuesta);

                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-filtro').removeClass('alert-success');
                        $('#message-filtro').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }

              });
    });


  // --------------------------------------
	// Agregar Filtro
	// -------------------------------------- 
    $('#InsertarFiltro').click(function(){

          var parametros = {
                  'accion': 'InsertarFiltro',
                  'nombre_filtro' : $("#nameFilter").val(),
                  'tipo_elemento' : $("#elemento").val(),
                  'opcion_1' : $("#option1").val(),
                  'opcion_2' : $("#option2").val(),
                  'opcion_3': $("#option3").val(),
                };

              $.ajax({
                  url: "../pages/newSubCategory.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message-agregar').addClass('alert-success');
                        $('#message-agregar').html(respuesta);
                         setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-agregar').removeClass('alert-success');
                        $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }
              });
    });


         //============================= Valores de descuento validos =============================\\
       document.getElementById("descuento").onkeyup=function(){
        var input=parseInt(this.value);
        if(input<0 || input>100){
          $('#descuento').val('');
          $('#descuento_message').addClass('alert-danger');
          $('#descuento_message').html( '<label> Maximo de 100% <label>');

        }else{
           $('#descuento_message').removeClass('alert-danger');
           $('#descuento_message').html('');
        } 
     }  
    //==========================================================================================\\


      // ==== No habilitar buton hasta que el campo nombre tenga valor ====\\
      $("#nombre").blur(function(){
              if ($(this).val() != "") {
                  $('#guardar').prop('disabled', false);
              } else {
                  $('#guardar').prop('disabled', true);       
              }
          });    
      //================================================================================\\


        // === No habilitar fechas de descuento hasta que el campo nombre tenga valor ===\\
        $("#descuento").blur(function(){
              if ($(this).val() != "") {
                  $('#descuento_desde').prop('disabled', false);
                  $('#descuento_hasta').prop('disabled', false);
              } else {
                $('#descuento_desde').prop('disabled', true);    
                $('#descuento_hasta').prop('disabled', true);  
                $('#descuento_hasta').val("");
                $('#descuento_desde').val("");
              }
          });    
        //================================================================================\\

        $('#destacado').click(function(){
            if($(this).prop("checked") == true){
               $('#destacado_desde').prop( "disabled", false );
               $('#destacado_hasta').prop( "disabled", false );
               this.value = this.checked ? 1 : 0;
            }
            else if($(this).prop("checked") == false){
               $('#destacado_hasta').prop( "disabled", true );
               $('#destacado_desde').prop( "disabled", true );
               $('#destacado_hasta').val("");
               $('#destacado_desde').val("");
                this.value = this.checked ? 1 : 0;
            }
        });
             $('#principal').on('change', function(){
              this.value = this.checked ? 1 : 0;
              //alert(this.value);
            }).change();



        $('input:checkbox').removeAttr('checked');
        $('#bonsai-checkbox-0').remove();
        $('ol.bonsai [data-name="baz"] :checkbox').remove();

        $("#auto-checkboxes").on('click', function() {
        var $box = $(this);
          if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
          } else {
            $box.prop("checked", false);
          }
      });





      //=========== GUARDAR DATOS DE SUBCATEGORIA =================\\
      
      $("#guardar").on('click', function() {
        if ($('input[name=foo]:checked').length === 0) {
               $('#message-filtro').addClass('alert-danger');
               $('#message-filtro').html("Debe seleccionar una categoria");
          }else if($('input[name=filtro]:checked').length === 0){
               $('#message-filtro').addClass('alert-danger');
               $('#message-filtro').html("Debe seleccionar un filtro");
            }else{

              var parents = [];
                  $('input[name=foo]:checked').each(function(){
                    parents.push($(this).val());
                });

                var fil = [];
                  $('input[name=filtro]:checked').each(function(){
                    fil.push($(this).val());
                });

              
            //alert($('input[type=checkbox]:checked').val());
              var destacado_desde = $("#destacado_desde").datepicker("getDate");
              var destacado_hasta = $("#destacado_hasta").datepicker("getDate");
              var descuento_desde = $("#descuento_desde").datepicker("getDate");
              var descuento_hasta = $("#descuento_hasta").datepicker("getDate");

              var flag_destacado = 1;
              var flag_descuento = 1;

                if($('#destacado').prop("checked") == true){
                      if ( (!destacado_desde) || (!destacado_hasta) ) {
                        $('#destacado_message').addClass('alert-danger');
                        $('#destacado_message').html( '<label> Fecha no puede estar vacia</label>');
                        flag_destacado = 0;
                      }else if ((destacado_desde) > (destacado_hasta)){
                          $('#destacado_message').addClass('alert-danger');
                          $('#destacado_message').html( '<label> Fecha hasta no puede ser menor</label>');
                        flag_destacado = 0;
                      }else{
                        $('#destacado_message').removeClass('alert-danger');
                        $('#destacado_message').html('');
                        flag_destacado = 1;
                }
              }

              if($('#descuento').val() != ""){
                      if ( (!descuento_desde) || (!descuento_hasta) ) {
                        $('#descuento_message').addClass('alert-danger');
                        $('#descuento_message').html( '<label> Fecha no puede estar vacia</label>');
                        
                        flag_descuento = 0;
                      }else if ((descuento_desde) > (descuento_hasta)){
                          $('#descuento_message').addClass('alert-danger');
                          $('#descuento_message').html( '<label> Fecha hasta no puede ser menor</label>');
                          
                        flag_descuento = 0;
                      }else{
                        $('#descuento_message').removeClass('alert-danger');
                        $('#descuento_message').html('');
                        flag_descuento = 1;
                }
              }

               
              if (flag_destacado != 0 && flag_descuento != 0){

                var parametros = {
                  'nombre' : $("#nombre").val().capitalize(),  //Primera letra en mayuscula
                  'principal' : $("#principal").val(),
                  'destacado' : $("#destacado").val(),
                  'descuento' : $("#descuento").val(),
                  'destacado_desde' : $("#destacado_desde").val(),
                  'destacado_hasta' : $("#destacado_hasta").val(),
                  'descuento_desde' : $("#descuento_desde").val(),
                  'descuento_hasta' : $("#descuento_hasta").val(),
                  'filtro' : fil,
                  'parent' : parents
                };
                
              $.ajax({
                  url: "../pages/newSubCategory.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message').addClass('alert-success');
                        $('#message').html(respuesta);
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message').removeClass('alert-success');
                        $('#message').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }

              });
              }
          }
      });
  
    });
