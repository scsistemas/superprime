 $(document).ready(function() {

        // Mostrar clic en lista de razon social
        $("#razonS").on('click','li',function(){
            // remove classname 'active' from all li who already has classname 'active'
            $("#razonS li a.active").removeClass("active"); 
            // adding classname 'active' to current click li 
            $(this).children("a").addClass("active"); 
            var razonsocial_id = $(this).attr("id");
            var parametros = {
                    'accion': 'ObtenerCiudadesByRazonSocial',
                    'razonsocial_id': razonsocial_id
                    };
                    $.ajax({
                    url: "../pages/adminShops.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                    var ciudades = JSON.parse(respuesta);
                    $('#ciudades').empty();
                      $.each(ciudades, function(key, value) {   
                            $('#ciudades')
                                .append($("<li></li>")
                                            .attr("id", value['codestado'])
                                                .append($("<a></a>").attr("href","#").text(value['desestado']))
                                            );  
                        });

                    },beforeSend: function () {
                    },
                    complete: function () {
                    }
                });
        });

        // Mostrar clic en lista de ciudades
        $("#ciudades").on('click','li',function(){
            // remove classname 'active' from all li who already has classname 'active'
            $("#ciudades li a.active").removeClass("active"); 
            // adding classname 'active' to current click li 
             $(this).children("a").addClass("active"); 

            var estado_id = $(this).attr("id");
            var parametros = {
                    'accion': 'ObtenerTiendasByEstado',
                    'estado_id': estado_id
                    };
                    $.ajax({
                    url: "../pages/adminShops.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                    var tiendas = JSON.parse(respuesta);
                     $('#tiendas').empty();
                    $.each(tiendas, function(key,value){
                    $('#tiendas')
                                .append($("<div></div>")
                                            .addClass("row itemShop")
                                            .attr("id", value['codtienda'])
                                                .append($("<div></div>").addClass("col-lg-2")
                                                    .append($("<a></a>").attr("href", "#").text(value['codtienda']).attr("name", "OnClick").attr("id", key))
                                                )
                                                 .append($("<div></div>").addClass("col-lg-10")
                                                    .append($("<label></label>")
                                                        .text(value['nombre'])
                                                    )
                                                    .append($("<label></label>")
                                                        .text(value['ciudad'])
                                                    )
                                                )
                                            );  
                    });
                    // Aplicar odd y even en la lista de tiendas
                    $ ('.itemShop:even').addClass ('even');
                    $ ('.itemShop:odd').addClass ('odd');

                    $("a[name='OnClick']").on('click',function(){
                        var indice = $(this).attr("id");
                        var data = tiendas[indice];
                        construirModal(data);
                    })
                    },beforeSend: function () {
                    },
                    complete: function () {
                    }
                });
        });


       
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('form').validator('destroy').validator();

        });

        function construirModal(data){
            listarCiudad(data);
            listarMunicipio(data);
            listarParroquia(data);
            $("#codigotienda").val(data['codtienda']);
            $("#edittienda").val(data['nombre']);
            $('#editestado option[value="'+data['estado']+'"]').attr('selected','selected');
            $("#editpostal").val(data['postal']);
            $("#edittelefono_1").val(data['telefono_1']);
            $("#edittelefono_2").val(data['telefono_2']);
            $("#editnombre").val(data['encargado']);
            $("#editemail").val(data['encargado_email']);
            $("#editdireccion").val(data['direccion']);
            $("#editweb").val(data['website']);
            $('#editShop').modal('show');
        };

        $("#editestado").on('change', function () {
            var id = this.options[this.selectedIndex].id;
            var data = {CodEstado: id, Ciudad:""};
            listarCiudad(data);
            $("#editmunicipio").empty();
            $("#editparroquia").empty();
        });

        $("#editciudad").on('change', function () {

            var estado = $("#editestado option:selected").prop('id');
             var id = this.options[this.selectedIndex].id;
            var data = {CodEstado: estado, CodCiudad: id, municipio:""};
            listarMunicipio(data);
            $("#editparroquia").empty();
        });

        $("#editmunicipio").on('change', function () {
            var estado = $("#editestado option:selected").prop('id');
            var ciudad = $("#editciudad option:selected").prop('id');
            var id = this.options[this.selectedIndex].id;
            var data = {CodEstado: estado, CodCiudad: ciudad, CodMunicipio: id, parroquia:""};
            listarParroquia(data);
        });


        function listarCiudad(data){
            var estado = data['CodEstado'];
             var parametros = {
                    'accion': 'ObtenerCiudades',
                    'p_cod_estado': estado
                    };
                    $.ajax({
                    url: "../pages/adminShops.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                    var ciudades = JSON.parse(respuesta);
                    $('#editciudad').empty();
                      $.each(ciudades, function(key, value) {   
                            $('#editciudad')
                                .append($("<option></option>")
                                            .attr("id", value['CodCiudad'])
                                            .val(value['DescCiudad'])
                                            .text(value['DescCiudad'])
                                );  
                        });
                    $('#ciudad').empty();
                      $.each(ciudades, function(key, value) {   
                            $('#ciudad')
                                .append($("<option></option>")
                                            .attr("id", value['CodCiudad'])
                                            .val(value['DescCiudad'])
                                            .text(value['DescCiudad'])
                                );  
                        });
                     $('#editciudad option[value="'+data['ciudad']+'"]').attr('selected','selected');
                     $('#ciudad option[value="'+data['ciudad']+'"]').attr('selected','selected');
                    },beforeSend: function () {
                    },
                    complete: function () {
                    }
                });
        };

        function listarMunicipio(data){
            var estado = data['CodEstado'];
            var ciudad = data['CodCiudad'];
             var parametros = {
                    'accion': 'ObtenerMunicipios',
                    'p_cod_estado': estado,
                    'p_cod_ciudad': ciudad
                    };
                    $.ajax({
                    url: "../pages/adminShops.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                    var municipio = JSON.parse(respuesta);
                    $('#editmunicipio').empty();
                      $.each(municipio, function(key, value) {   
                            $('#editmunicipio')
                                .append($("<option></option>")
                                            .attr("id", value['CodMunicipio'])
                                            .val(value['DescMunicipio'])
                                            .text(value['DescMunicipio'])
                                );  
                        });
                    $('#municipio').empty();
                      $.each(municipio, function(key, value) {   
                            $('#municipio')
                                .append($("<option></option>")
                                            .attr("id", value['CodMunicipio'])
                                            .val(value['DescMunicipio'])
                                            .text(value['DescMunicipio'])
                                );  
                        });
                    $('#editmunicipio option[value="'+data['municipio']+'"]').attr('selected','selected');
                    $('#municipio option[value="'+data['municipio']+'"]').attr('selected','selected');
                    },beforeSend: function () {
                    },
                    complete: function () {
                    }
                });
        };

        function listarParroquia(data){
            var estado = data['CodEstado'];
            var ciudad = data['CodCiudad'];
            var municipio = data['CodMunicipio'];
             var parametros = {
                    'accion': 'ObtenerParroquias',
                    'p_cod_estado': estado,
                    'p_cod_ciudad': ciudad,
                    'p_cod_municipio': municipio,
                    };
                    $.ajax({
                    url: "../pages/adminShops.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                    var parroquia = JSON.parse(respuesta);
                    $('#editparroquia').empty();
                      $.each(parroquia, function(key, value) {   
                            $('#editparroquia')
                                .append($("<option></option>")
                                            .attr("id", value['CodParroquia'])
                                            .val(value['DescParroquia'])
                                            .text(value['DescParroquia'])
                                );  
                        });
                    $('#parroquia').empty();
                      $.each(parroquia, function(key, value) {   
                            $('#parroquia')
                                .append($("<option></option>")
                                            .attr("id", value['CodParroquia'])
                                            .val(value['DescParroquia'])
                                            .text(value['DescParroquia'])
                                );  
                        });
                    $('#editparroquia option[value="'+data['parroquia']+'"]').attr('selected','selected');
                    $('#parroquia option[value="'+data['parroquia']+'"]').attr('selected','selected');
                    },beforeSend: function () {
                    },
                    complete: function () {
                    }
                });
        };



        
    // --------------------------------------
	// Update Tienda
	// -------------------------------------- 
    $('#Actualizar').click(function(){
        
        var codTienda =  $("#codigotienda").val();
        var nombre =  $("#edittienda").val();
        
        var estado =  $("#editestado option:selected").attr("id");
        var ciudad =  $("#editciudad option:selected").attr("id");
        var municipio =  $("#editmunicipio option:selected").attr("id");
        var parroquia =  $("#editparroquia option:selected").attr("id");
        var postal =  $("#editpostal").val();
        var website =  $("#editweb").val();
        var direccion =  $("#editdireccion").val();
        var email =  $("#editemail").val();
        var encargado =  $("#editnombre").val();
        var telefono_1 =  $("#edittelefono_1").val();
        var telefono_2 =  $("#edittelefono_2").val();

         var parametros = {
                  'accion': 'ActualizarTienda',
                  'codTienda': codTienda,
                  'nombre': nombre,
                  'estado': estado,
                  'ciudad': ciudad,
                  'municipio': municipio,
                  'parroquia': parroquia,
                  'postal': postal,
                  'website': website,
                  'direccion': direccion,
                  'email': email,
                  'encargado': encargado,
                  'telefono_1': telefono_1,
                  'telefono_2': telefono_2
                };
                 $.ajax({
                  url: "../pages/adminShops.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message-agregar').addClass('alert-success');
                        $('#message-agregar').html(respuesta);
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                      
                  },
                  complete: function () {
                  }
              });
    });



    
        $("#estado").on('change', function () {
            var id = this.options[this.selectedIndex].id;
            var data = {CodEstado: id, Ciudad:""};
            listarCiudad(data);
            $("#municipio").empty();
            $("#parroquia").empty();
        });

        $("#ciudad").on('change', function () {
            var estado = $("#estado option:selected").prop('id');
             var id = this.options[this.selectedIndex].id;
            var data = {CodEstado: estado, CodCiudad: id, municipio:""};
            listarMunicipio(data);
            $("#parroquia").empty();
        });

        $("#municipio").on('change', function () {
            var estado = $("#estado option:selected").prop('id');
            var ciudad = $("#ciudad option:selected").prop('id');
            var id = this.options[this.selectedIndex].id;
            var data = {CodEstado: estado, CodCiudad: ciudad, CodMunicipio: id, parroquia:""};
            listarParroquia(data);
        });

    // --------------------------------------
	// Insertar Tienda
	// -------------------------------------- 
    $('#Nueva').click(function(){
        
        var codTienda =  $("#codigo").val();
        var nombre =  $("#tienda").val();
        var estado =  $("#estado option:selected").attr("id");
        var ciudad =  $("#ciudad option:selected").attr("id");
        var municipio =  $("#municipio option:selected").attr("id");
        var parroquia =  $("#parroquia option:selected").attr("id");
        var postal =  $("#postal").val();
        var website =  $("#web").val();
        var direccion =  $("#direccion").val();
        var email =  $("#email").val();
        var encargado =  $("#nombre").val();
        var telefono_1 =  $("#telefono_1").val();
        var telefono_2 =  $("#telefono_2").val();

         var parametros = {
                  'accion': 'InsertarTienda',
                  'codTienda': codTienda,
                  'nombre': nombre,
                  'estado': estado,
                  'ciudad': ciudad,
                  'municipio': municipio,
                  'parroquia': parroquia,
                  'postal': postal,
                  'website': website,
                  'direccion': direccion,
                  'email': email,
                  'encargado': encargado,
                  'telefono_1': telefono_1,
                  'telefono_2': telefono_2
                };
                 $.ajax({
                  url: "../pages/adminShops.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message-agregar').addClass('alert-success');
                        $('#message-agregar').html(respuesta);
                       setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                      
                  },
                  complete: function () {
                  }
              });
    });

        $(".numeric").numeric()

        $('#celular').mask('(0000) 0000000');

});
