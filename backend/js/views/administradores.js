$(document).ready(function() {

   
    $("#acceso").change(function() {
        var id = $(this).children(":selected").attr("id");
    });

    // --------------------------------------
	// Agregar Usuario
	// -------------------------------------- 
    $('#InsertarUsuario').click(function(){
        var acceso = $("#acceso option:selected").attr("id")
        var tipo_cedula = $("#tipo_cedula option:selected").val()

         var parametros = {
                  'accion': 'InsertarUsuario',
                  'nombre' : $("#nombreUser").val(),
                  'apellido' : $("#LastnameUser").val(),
                  'correo' : $("#correoUser").val(),
                  'telefono' : $("#telUser").val(),
                  'acceso': acceso,
                  'contrasena': $("#contraUser").val(),
                  'cedula': $("#cedulaUser").val(),
                  'tipo_cedula': tipo_cedula,
                };
                 $.ajax({
                  url: "../pages/adminAdministrators.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message-agregar').addClass('alert-success');
                        $('#message-agregar').html(respuesta);
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-agregar').removeClass('alert-success');
                        $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }
              });
    });


    // --------------------------------------
	// Obtener Datos ID
	// -------------------------------------- 
    
    $("span[name='edit']").click(function(){
        
        var acceso_desc = $(this).closest('td').siblings('#acceso_td').text();
        $("input").prop('disabled', true);
        $('#editipo_cedula').prop('disabled', true);
         var parametros = {
                  'accion': 'ObtenerUsuario',
                  'iduser': ($(this).attr("id")),
                  'acceso_desc' : acceso_desc
                };
                 $.ajax({
                  url: "../pages/adminAdministrators.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                      var result = JSON.parse(respuesta);
                      $("#editnombre").val(result['nombre']);
                      $("input[name='id_user']").val(result['IdUser']);
                      $("#editapell").val(result['apellido']);
                      $("#editcorreo").val(result['usuario']);
                      $('#editipo_cedula option[value="'+result['TipoId']+'"]').attr('selected','selected');
                      $("#editcedula").val(result['NumId']);
                      $("#editcontra").val(result['contrasena']);
                      $("#editelf").val(result['telcel']);
                      $('#editacceso option[value="'+result['acesso']+'"]').attr('selected','selected');
                  },beforeSend: function () {
                  },
                  complete: function () {
                  }
              });
              
    });


    // --------------------------------------
	// Update Usuario
	// -------------------------------------- 
    $('#ActualizarUsuario').click(function(){
        var edit_acceso = $("#editacceso option:selected").attr("id");
        var userid = $("input[name='id_user']").val();

         var parametros = {
                  'accion': 'ActualizarUsuario',
                  'iduser': userid,
                  'acceso': edit_acceso
                };
                 $.ajax({
                  url: "../pages/adminAdministrators.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        console.log(respuesta);
                        $('#message-agregar').addClass('alert-success');
                        $('#message-agregar').html(respuesta);
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-agregar').removeClass('alert-success');
                        $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }
              });
    });


    // --------------------------------------
	// Delete Usuario
	// -------------------------------------- 
    $('a[name="delete"]').click(function(){

        var id = $(this).attr('id');
        var acceso_d = $(this).closest('td').siblings('#acceso_td').text();
         $('#confirm').click(function(){
            var parametros = {
                  'accion': 'EliminarAdministrador',
                  'iduser': id,
                  'acceso': acceso_d
                };
                 $.ajax({
                  url: "../pages/adminAdministrators.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('.remodal-close').trigger('click');
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-agregar').removeClass('alert-success');
                        $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }
              });
        });
    });

     $('#users').DataTable({
        "bFilter": false,
        "sDom": 'Rfrtlip',
    });

             
});