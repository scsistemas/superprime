$(document).ready(function() {

    $('.estatus2').css({'background-color':'rgba(101, 101, 101, 0.16)',
    'color':'red'});

    // --------------------------------------
	// Obtener Datos ID
	// -------------------------------------- 
    
    $("span[name='edit']").click(function(){
        
        var usuario = $(this).closest('td').siblings('#usuario').text();

        var parametros = {
                  'accion': 'ObtenerUsuario',
                  'usuario': usuario
                };
                 $.ajax({
                  url: "../pages/adminUsers.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                     var otro = JSON.parse(respuesta);
                      $("input[name='id_user']").val(otro['IdUser']);
                      $("#editnombre").val(otro['Nombre']);
                      $("#editapell").val(otro['Apellido']);
                      $("#editemail").val(otro['Usuario']);
                      $('#editestado option[value="'+otro['estado']+'"]').attr('selected','selected');
                  },beforeSend: function () {
                  },
                  complete: function () {
                  }
              });
              
    });


    // --------------------------------------
	// Update Usuario
	// -------------------------------------- 
    $('#ActualizarUsuario').click(function(){
       
        var userid =  $("input[name='id_user']").val();
        
        var estado =  $("#editestado option:selected").attr("id");

         var parametros = {
                  'accion': 'ActualizarUsuario',
                  'idUser': userid,
                  'nombre': $("#editnombre").val(),
                  'apellido': $("#editapell").val(),
                  'usuario': $("#editemail").val(),
                  'estado': estado
                };
                 $.ajax({
                  url: "../pages/adminUsers.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('#message-agregar').addClass('alert-success');
                        $('#message-agregar').html(respuesta);
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-agregar').removeClass('alert-success');
                        $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }
              });
    });


    // --------------------------------------
	// Delete Usuario
	// -------------------------------------- 
    $('i[name="delete"]').click(function(){

       
        var usuario = $(this).closest('td').siblings('#usuario').text();
         $('#confirm').click(function(){
           var parametros = {
                  'accion': 'EliminarUsuario',
                  'usuario': usuario
                };
                 $.ajax({
                  url: "../pages/adminUsers.php",                        
                  type : 'POST',
                  data: parametros,
                  success: function (respuesta) {
                        $('.remodal-close').trigger('click');
                        setTimeout( location.reload(), 20000);
                  },beforeSend: function () {
                        $('#message-agregar').removeClass('alert-success');
                        $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                  },
                  complete: function () {
                  }
              });
        });
    });


    // --------------------------------------
	// Bloquear  Usuario
	// -------------------------------------- 
    $('span[name="bloquear"]').click(function(){

       
        var usuario = $(this).closest('td').siblings('#usuario').text();
        $('#confirm_bloq').click(function(){
            var parametros = {
                    'accion': 'BloquearUsuario',
                    'usuario': usuario
                    };
                    $.ajax({
                    url: "../pages/adminUsers.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                            $('.remodal-close').trigger('click');
                            setTimeout( location.reload(), 20000);
                    },beforeSend: function () {
                            $('#message-agregar').removeClass('alert-success');
                            $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                    },
                    complete: function () {
                    }
                });
            });
    });

    // --------------------------------------
	// Desbloquear Usuario
	// -------------------------------------- 
    $('span[name="desbloquear"]').click(function(){

       
        var usuario = $(this).closest('td').siblings('#usuario').text();
            var parametros = {
                    'accion': 'DesbloquearUsuario',
                    'usuario': usuario
                    };
                    $.ajax({
                    url: "../pages/adminUsers.php",                        
                    type : 'POST',
                    data: parametros,
                    success: function (respuesta) {
                            setTimeout( location.reload(), 20000);
                    },beforeSend: function () {
                            $('#message-agregar').removeClass('alert-success');
                            $('#message-agregar').html( '<h4>Por favor Espere...</h4>');
                    },
                    complete: function () {
                    }
                });
    });


    $('#users').DataTable({
    "bFilter": false,
    "sDom": 'Rfrtlip'
    });


});
