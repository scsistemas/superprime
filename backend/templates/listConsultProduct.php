<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<style>
body{
	margin: 0;
	padding: 0;
	font-family: Verdana, Geneva, Tahoma, sans-serif;
	
}
#container{
	width: 1000px;
	margin: auto;
}


table tr td{
	font-size: 12px;
	padding: 2px 0 0 2px;
}

.table2{
	width: 100%; 
	color:#003399;
	margin-top:20px;
}
	.table2 tr td{
		border: 1px #49749e solid;
		padding: 2px 0 0 2px;
		color: #333333;
		font-size:10px; 
	}

div.formulario{
	border: 0;
}

h3{
	color: #000;
	text-align: center;
	margin-top:100px;
	
}

div.formulario table tr td{
	border: 1px #49749e solid;
	padding: 2px 0 0 2px;
	color: #49749e;
	width: 100%;
}

div.formulario table tr td.header{
	background-color: #dbe5f1;
	text-align: center;
	font-weight: bold;
	padding: 2px 0 0 2px;
}

.pie{
	font-size: 8px;
	color: #999999;
}

.cabecera{
	border:0;
	float:right;
	font-size: 10px;
	width: 70%
	text-align: right;
	margin-right: 85%;

}
.logo{
	border:0;
	float: left;
	width:30px;
	

}
</style>
</head>
	<body>
		<div class="container" style="width: 900px; margin: auto">
			<?php 
			$cont = 1;
			$pag =0;
			$regP = 10; // cantidad de registros a mostrar por pag.
			$totalP = ceil(sizeof($resultData['data']) / $regP);//total páginas a generar
			
			if (is_array($resultData['data'])){
				foreach ($resultData['data'] as $data) {
					if ($cont > $regP || $cont == 1){
						$pag = $pag +1;
						if ($cont > $regP){?>
							
							</table>
							<br><div><!--contenedor principal-->
							
						<?php
						} 
						?>
						
					
							<div class='logo'  >
								<img src="../images/logo.jpg" title="" alt="">
							</div>
							<?php $textP = 'Página ' . $pag .' / ' . $totalP;
								  $time = time();
				
							?>
							<div class='cabecera'>
								<div><strong><?php echo  $textP ?></strong></div>
								<div><strong>Generado por: </strong><?php echo  $tPersona['Persona']['Usuario']?></div>
								<div><strong>Fecha: </strong><?php echo date("d-m-Y (H:i:s)", $time); ?></div> 
 							</div>
						
						<!--</div>-->
				
						<div class="formulario" style="width:100%">
							<h3>LISTADO DE PRODUCTOS</h3>
							<br>
						
							<div style="border:0px;font-size:10px;text-align:center;width:100%">
								
									<?php 
									if ( $fecha_desde != ''){?>
										<strong>Fecha: </strong><?php echo $fecha_desde.' a '.$fecha_hasta.',' ?>
									<?php
									}
									?>
										
									<strong>Tipo de Producto: </strong><?php echo $data[3] ?><?php echo $g!=''?',<strong>Género: </strong>'.$g:""; ?>
									
									<?php 
									if (isset( $pw )){?>
										,<strong> Publicado en Web: </strong><?php echo $pw?>
									<?php
									}
									?>
										
									<?php 
									if (isset( $pml )){?>
										,<strong> Publicado en ML: </strong><?php echo $pml?>

									<?php
									}
									?>
									
									<?php 
									if (isset( $f)){?>
										,<strong> Foto: </strong><?php echo $f?>
									<?php
									}
									?>

								
							</div>
							<br>
							<br>
							<?php 
								
								if ($pag == $totalP){
							?>
								<table width="100%" cellpadding="0" cellspacing="0" style="font-size:10PX;">
							<?php 
								}
								else{
							?>
								<table style='page-break-after:always;' width="100%" cellpadding="0" cellspacing="0" style="font-size:10PX;">
								<?php 
								}
								?>
								
								<tr>
									<td style="text-align:center; height: 18px;"><strong>Código</strong></td>
									<td style="text-align:center; height: 18px;"><strong>Marca</strong></td>
									<td style="text-align:center; height: 18px;"><strong>Descripción SAP</strong></td>
									<td style="text-align:center; height: 18px;"><strong>Colores sin Imagenes</strong></td>
									<td style="text-align:center; height: 18px;"><strong>Entregado a Fotografo </strong></td>
									<td style="text-align:center; height: 18px;"><strong>Devuelto a Almacen</strong></td>
								</tr>
	
						
										
				<?php
					} ?>
					
					<tr>
						<td style="text-align:center; height: 18px;"><?php echo $data[0]?></td>
						<td style="text-align:center; height: 18px;"><?php echo $data[1]?></td>
						<td style="text-align:center; height: 18px;"><?php echo $data[2]?></td>
						<td style="text-align:center; height: 18px;"><?php echo $data[8]?></td>
						<td style="text-align:center; height: 18px;"></td>
						<td style="text-align:center; height: 18px;"></td>
					</tr>
					<?php 
					$cont = $cont + 1;
					if ($cont == $regP+2)
						$cont = $regP;
					?>
					</div><!--contenedor principal-->
				<?php
				}
				?>
				
			<?php
			}
			?>
								
		</div>
		</div>
				
	</body>	
</html>