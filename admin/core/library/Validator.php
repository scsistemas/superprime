<?php 

Class Validator{

	// Mensajes de error 
	public $mensajes = [];

	// Si existe un error 
	public $error = false;

	// Grupos validados 
	public $group= [];

	// Parametros
	protected $param=[];

	// Reglas
	protected $rules=[];
	
	// Ignorar regex 
	protected $ignoreRegex=[];

	protected function validCondition($nameVar)
	{
		$required = (in_array('required', $this->rules[$nameVar]))? "true" : "false";
		$content  = "true";
		if(isset($this->param)){
			if(array_key_exists($nameVar, $this->param)){
				if(empty($this->param[$nameVar])){
					$content = "false";
				}
			}
		}		
		return ( $required=="false" && $content=="false" )? false : true;
	}

	protected function condition($condition, $nameVar)
	{
		foreach($condition as $name => $val){
			$msg = "";
			// ----------------------------
			// Casos con Nombres
			// ----------------------------
			switch(strtolower($name)){
				case 'regex':
					if(!in_array($nameVar, $this->ignoreRegex))
					{
						if(array_key_exists($nameVar, $this->param))
						{
							if(!$this->isValid($this->param[$nameVar], $val))
							{
								$msg = "Datos invalidos";
							}
						}
					}
					break;
				case "equalto":
					if(!$this->isEqualTo($nameVar, $val)){
						$msg = "El campo {$nameVar} no es igual a {$val}";
					}
					break;
				case 'atleastone':
					if(!$this->isAtleastone($val)){
						$msg = "Este Campo es requerido";
					}
					break;
			}
			// ----------------------------
			// Casos sin Nombres
			// ----------------------------
			switch(strtolower($val)){
				case 'required':
					if(!$this->isRequired($nameVar)){
						$msg = "El campo es requerido";
					}
					break;
			}
			// ----------------------------
			// Resultado
			// ----------------------------
			if(!empty($msg)){
				$this->mensajes[$nameVar][] = $msg;
				$this->error = true;
			}
		}
		return $this->error;
	}


	//** *****************************************
	// Condiciones 
	//** *****************************************

	// by Equal To
	function isEqualTo($name, $confirmed)
	{
		if( array_key_exists($name, $this->param) &&
			array_key_exists($confirmed, $this->param)
		){
			if($this->param[$name] === $this->param[$confirmed]){
				return true;
			}		
		}
		return false;
	}
	// by Required
	function isRequired($name){		
		return array_key_exists($name, $this->param);
	}
	// by RegExr
	function isValid($data, $regexp){
		$result = false;
		switch($regexp){
			case "email":
				if(preg_match("/[\+]{1,}/", $data)){break;}
				$result = filter_var($data, FILTER_VALIDATE_EMAIL);
				break;
			case "password":
				if(strlen($data) < 8 ||
				!preg_match("/[a-z]{1,}/", $data) ||
				!preg_match("/[A-Z]{1,}/", $data) ||
				!preg_match("/[\!\@\#\*\-\_]{1,}/", $data) ||
				!preg_match("/[0-9]{1,}/", $data)
				){
					break;
				}
				$result = true;
				break;
			default:
				$result = (preg_match("/{$regexp}/", $data))? true : false;
				break;
		}
		return $result;
	}
	// by AtLeastOne
	function isAtleastone($group){
		$_atleastone = [];
		if(!in_array($group, $this->group)){
			foreach($this->rules as $name => $rule){
				if(array_key_exists('atleastone', $rule)){
					if($rule['atleastone'] == $group){
						$valid = $this->isValid($this->param[$name], $rule['regex']);
						if($valid){
							$_atleastone[] = $name;
						}
						$this->ignoreRegex[] = $name;
					}
				}
			}
			$this->group[] = $group;
		}else{
			return true;
		}
		return (empty($_atleastone))? false : true;
	}

	function check($param, $regex){
		$this->param = $param;
		$this->rules = $regex;
		foreach ($this->rules as $name => $condition) {
			if($this->validCondition($name)){
				$this->condition($condition, $name);
			}
		}
		return $this->error;
	}

	static function All($param, $rule){
		$this->param = $param;
		$this->rules = $rule;
		foreach ($rule as $name => $val) {
			if(!self::isValid($param[$name], $val)){
				return false;
			}
		}
		return true;
	}

	
	
}