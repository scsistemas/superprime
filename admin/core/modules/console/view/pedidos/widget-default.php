<script src="core/modules/console/view/pedidos/pedidos.js"></script>
<div class="row">
    <div class="col-md-12" id="pedidos">
        <h1>Lista de Pedidos</h1>
        <br>
        <br>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <table id="Tpedidos" class="display table" width="100%">
            <thead>
            <th>Código Pedido</th>
            <th>Código Producto</th>
            <th>Código Tienda</th>
            <th>Cantidad</th>
            <th>Estatus</th>
            </thead>
        <?php
        $pedidos = ProductoData::ObtenerPedidos();
        if (count($pedidos) > 0) {
            ?>

                <tbody>
                <?php
                foreach ($pedidos as $pedido) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $pedido->CodPedido; ?></td>
                        <td align="center"><?php echo $pedido->CodProducto; ?></td>
                        <td align="center"><?php echo $pedido->CodTienda; ?></td>
                        <td align="center"><?php echo $pedido->Cantidad; ?></td>
                        <td align="center"><?php echo $pedido->Estatus; ?></td>
                    </tr>
                <?php

                }?>
                </tbody>
            </table>
        <?php
        } ?>

    </div>
</div>
</div>
