<!DOCTYPE html>
<html lang="en">
<?php session_start();?>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Prime</title>

    <!-- Bootstrap core CSS -->
     <link href="js/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="res/bootstrap3/css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-dialog.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <script src="js/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <script type="text/javascript" src="js/datatable/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/BlockUi.js"></script>

    <script src="js/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script>

  </head>

  <body>

    <div id="wrapper">

      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Prime <sup><small><span class="label label-info"></span></small></sup> </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
<?php 
$u=null;
if(Session::getUID()!=""):
  $u = UserData::getById(Session::getUID());
?>
       <!--  <ul class="nav navbar-nav">
          <li><a href="index.php?"><i class="fa fa-home"></i>Home</a></li>
          </ul>-->
          <ul class="nav navbar-nav side-nav">
          <li><a href="index.php?view=home"><i class="fa fa-home"></i> Home</a></li>
          <?php /*if($u->is_admin):*/?><!--
          <li><a href="index.php?view=users"><i class="fa fa-users"></i> Usuarios </a></li>
        --><?php /*endif;*/?>
          </ul>




<?php endif;?>



<?php if(Session::getUID()!=""):?>
<?php 
$u=null;
if(Session::getUID()!=""){
  $u = UserData::getById(Session::getUID());
  $user = $u->name." ".$u->lastname;

  }?>
          <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown user-dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <?php echo $user; ?> <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
<!--          <li><a href="index.php?view=configuration">Configuracion</a></li>-->
          <li><a href="logout.php">Exit</a></li>
        </ul>

<?php else:?>
<?php endif;

?>




            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>

      <div id="page-wrapper">

<?php
  View::load("login");

?>



      </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    <!-- JavaScript -->

<script src="res/bootstrap3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-dialog.js"></script>
<script src="js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

  </body>
</html>
