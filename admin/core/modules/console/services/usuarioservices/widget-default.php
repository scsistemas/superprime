<?php

$services= new UsuarioServices();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        switch($_GET['metodo']) {
            case 'ObtenerUsuarios' :
                $services->ObtenerUsuarios();
            break;
            case 'ObtenerEstados' :
                $services->ObtenerEstados();
            break;
        }
}else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'ActualizarUsuario' :
            $services->ActualizarUsuario($body);
            break;
        case 'ObtenerUsuarioByEmail' :
            $services->ObtenerUsuarioByEmail($body);
            break;
        case 'EliminarUsuario' :
            $services->EliminarUsuario($body);
            break;
        case 'BloquearUsuario' :
            $services->BloquearUsuario($body);
            break;
        case 'DesbloquearUsuario' :
            $services->DesbloquearUsuario($body);
            break;
    }
}

?>