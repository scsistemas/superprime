<?php

$services= new AdminServices();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        switch($_GET['metodo']) {
            case 'ObtenerAdministradores' :
                $services->ObtenerAdministradores();
            break;
            case 'ObtenerMenus' :
                $services->ObtenerMenus();
            break;
        }
}else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'CrearAdmin' :
            $services->CrearAdmin($body);
            break;
        case 'UpdateAdmin' :
            $services->UpdateAdmin($body);
            break;
        case 'ObtenerAdmin' :
            $services->ObtenerAdmin($body);
            break;
            break;
        case 'EliminarAdministrador' :
            $services->EliminarAdministrador($body);
            break;
    }
}

?>