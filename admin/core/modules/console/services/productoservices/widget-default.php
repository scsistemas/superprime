<?php
global $lang;
$services=new ProductoServices();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Decodificando formato Json
        switch($_GET['metodo']) {
            case 'ObtenerProductos' :
                $services->ObtenerProductos($_GET);
            break;
            case 'ObtenerPrecioProducto' :
                $services->ObtenerPrecioProducto($_GET);
            break;
            case 'ObtenerInventarioTallaColor' :
                $services->ObtenerInventarioTallaColor($_GET);
            break;
            case 'ObtenerDetalleProducto' :
                $services->ObtenerDetalleProducto($_GET);
                break;
            case 'ObtenerDetalleProductoListado' :
                $services->ObtenerDetalleProductoListado($_GET);
                break;
            case 'ObtenerDetalleProductoSeleccionado' :
                $services->ObtenerDetalleProductoSeleccionado($_GET);
                break;
            case 'ObtenerTopProductos' :
                $services->ObtenerTopProductos($_GET);
                break;
            case 'ObtenerProductosSincronizados' :
                $services->ObtenerProductosSincronizados($_GET);
                break;
            case 'consultarPublicacionProducto' :
                $services->consultarPublicacionProducto($_GET);
                break;
            case 'ObtenerProductosPorColor' :
                $services->ObtenerProductosPorColor($_GET);
                break;
            case 'ObtenerMarcasProducto' :
                $services->ObtenerMarcasProducto($_GET);
                break;
            case 'ObtenerMarcasListado' :
                $services->ObtenerMarcasListado($_GET);
                break;
            case 'ObtenerGenerosProducto' :
                $services->ObtenerGenerosProducto($_GET);
                break;
 	         case 'ObtenerCategorias' :
                $services->ObtenerCategorias();		
                break;
            case 'ObtenerSubCategorias' :
                $services->ObtenerSubCategoria($_GET);
                break;
            case 'ObtenerListadoProductos' :
                $services->ObtenerListadoProductos($_GET);
                break;
            case 'ValidaOrdenML' :
                $services->ValidaOrdenML($_GET,$lang);
                break;
            case 'ObtenerTallaColor' :
                $services->ObtenerTallaColor($_GET);
                break;
            case 'ObtenerImagenesColor' :
                $services->ObtenerImagenesColor($_GET);
                break;
            case 'ObtenerDetalleCompraExpress' :
                $services->ObtenerDetalleCompraExpress($_GET);
                break;
            case 'ObtenerDetalleCompraCarro' :
                $services->ObtenerDetalleCompraCarro($_GET);
                break;
        }
} else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'GuardarDetalleProducto' :
            $services->ActualizaProducto($body);
            break;
        case 'Sincronizar' :
            $services->SincronizarProductos();
            break;
        case 'ActualizaInventario' :
            $services->ActualizaInventario($body);
            break;
        case 'CrearPedidoML' :
            $services->CrearPedidoML($body);
            break;
        case 'CrearCompraExpress' :
            $services->CrearCompraExpress($body,$lang);
            break;
        case 'CrearCompraExpressLogin' :
            $services->CrearCompraExpressLogin($body,$lang);
            break;
        case 'ProcesarCarro' :
            $services->ProcesarCarro($body);
            break;
        case 'ActualizaPedido' :
            $services->ActualizaPedido($body);
            break;
        case 'NotificaNotaCredito' :
            $services->NotificaNotaCredito($body);
            break;
        case 'NotificaPedidoEnviado' :
            $services->NotificaPedidoEnviado($body);
            break;
        case 'NotificaPedidoFacturado' :
            $services->NotificaPedidoFacturado($body);
            break;
        case 'InsertarNoImagen' :
            $services->InsertarNoImagen($body);
            break;
        case 'EliminarNoImagen' :
            $services-> EliminarNoImagen($body);
            break;
        case 'PagarProducto' :
            $services->PagarProducto($body);
            break;
        case 'ActualizaIgnorado' :
            $services-> ActualizaIgnorado($body);
            break;
        case 'insertarPublicacion' :
            $services->insertarPublicacion($body);
            break;
        case 'actualizarPublicacion' :
            $services->actualizarPublicacion($body);
            break;
        case 'GestionarCompraML' :
            $services->GestionarCompraML($body);
            break;
        case 'CrearPedido' :
            $services->CrearPedido($body);
            break;
        case 'PagarPedido' :
            $services->PagarPedido($body);
            break;

    }



}

?>
