<?php

$services= new FiltroServices();

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        switch($_GET['metodo']) {
            case 'ObtenerFiltros' :
                $services->ObtenerFiltros();
            break;
        }
}else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'EliminarFiltro' :
            $services->EliminarFiltro($body);
            break;
        case 'EditarFiltro' :
            $services->EditarFiltro($body);
            break;
        case 'InsertarFiltro' :
            $services->InsertarFiltro($body);
            break;
        case 'ObtenerFiltrosByID' :
            $services->ObtenerFiltrosByID($body);
        break;
    }
}

?>