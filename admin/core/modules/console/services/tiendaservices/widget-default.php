<?php
global $lang;
$services=new TiendaServices();
if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        switch($_GET['metodo']) {
            case 'ObtenerRazonesSociales' :
                $services->ObtenerRazonesSociales();
            break;
        }
}else if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $body = json_decode(file_get_contents("php://input"), true);
    switch($_GET['metodo']) {
        case 'ActualizarTienda' :
            $services->ActualizarTienda($body);
            break;
        case 'ObtenerTiendasByEstado' :
            $services->ObtenerTiendasByEstado($body);
            break;
        case 'ObtenerCiudadesByRazonSocial' :
            $services->ObtenerCiudadesByRazonSocial($body);
            break;
        case 'InsertarTienda' :
            $services->InsertarTienda($body);
            break;
    }
}


?>