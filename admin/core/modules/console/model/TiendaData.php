<?php

	// --------------------------------------
	// Servicio de Tiendas Backend
	// --------------------------------------

class TiendaData
{
    ///============ Obtener Tiendas ============\\\
	static function ObtenerRazonesSociales(){
		$sql = "CALL ObtenerRazonesSociales()";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new TiendaData());
        }

	}

    ///============ Obtener Estados by Razon Social ============\\\
	static function ObtenerCiudadesByRazonSocial($razonsocial_id){
		$sql = "CALL ObtenerCiudadesByRazonSocial($razonsocial_id)";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new TiendaData());
        }

	}

    ///============ Obtener tienda by estado ============\\\
	static function ObtenerTiendasByEstado($estado_id){
		$sql = "CALL ObtenerTiendasByEstado($estado_id)";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new TiendaData());
        }

	}

	
    ///============ Actualizar tienda ============\\\
	static function ActualizarTienda($codTienda, $nombre, $estado, $ciudad, $municipio, $parroquia, $postal, $website, $direccion, $email, $encargado, $telefono_1, $telefono_2){
		$sql = "CALL ActualizarTienda(
				  '$codTienda',
				  '$nombre',
				  '$estado',
				  '$ciudad',
                  '$municipio',
                  '$parroquia',
                  '$postal',
                  '$website',
                  '$direccion',
                  '$email',
                  '$encargado',
                  '$telefono_1',
                  '$telefono_2'
		)";
		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}
    ///============ Insertar tienda ============\\\
	static function InsertarTienda($codTienda, $nombre, $estado, $ciudad, $municipio, $parroquia, $postal, $website, $direccion, $email, $encargado, $telefono_1, $telefono_2){
		$sql = "CALL InsertarTienda(
				  '$codTienda',
				  '$nombre',
				  '$estado',
				  '$ciudad',
                  '$municipio',
                  '$parroquia',
                  '$postal',
                  '$website',
                  '$direccion',
                  '$email',
                  '$encargado',
                  '$telefono_1',
                  '$telefono_2'
		)";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}



}
