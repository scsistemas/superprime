<?php

	// --------------------------------------
	// Servicio de Usuarios Backend
	// --------------------------------------

class UsuarioData
{
    ///============ Obtener Usuarios ============\\\
	static function ObtenerUsuarios(){
		$sql = "CALL ObtenerUsuarios()";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new UsuarioData());
        }

	}

    ///============ Obtener Estados ============\\\
	static function ObtenerEstados(){
		$sql = "CALL ListadoEstados()";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new UsuarioData());
        }

	}

	
    ///============ Obtener usuario by Email ============\\\
	static function ObtenerUsuarioByEmail($usuario){
		$sql = "CALL ObtenerUsuarioByEmail('$usuario')";
		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}

    ///============ Actualizar Usuario ============\\\
	static function ActualizarUsuario($idUser, $nombre, $apellido, $usuario, $estado){
		$sql = "CALL ActualizarUsuario(
			 $idUser,
			'$nombre',
			'$apellido',
			'$usuario',
			$estado
		)";
		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}

    ///============ Eliminar Usuario ============\\\
	static function EliminarUsuario($usuario){
		$sql = "CALL BorrarUsuario('$usuario')";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}
	///============ Bloquear Usuario ============\\\
	static function BloquearUsuario($usuario){
		$sql = "CALL BloquearUsuario('$usuario')";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}
	///============ Desbloquear Usuario ============\\\
	static function DesbloquearUsuario($usuario){
		$sql = "CALL DesbloquearUsuario('$usuario')";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}


}
