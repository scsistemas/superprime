<?php

	// --------------------------------------
	// Servicio de Filtros
	// --------------------------------------

class FiltrosData
{

    
	///============ OBTENER FILTROS  ============\\\
	static function ObtenerFiltros(){
 		$sql = "CALL ObtenerFiltros()";
        $query = Executor::doit($sql);
        return Model::many($query[0],new FiltrosData());
	}
	
	///============ OBTENER FILTROS BY ID ============\\\
	static function ObtenerFiltrosByID($id){
 		$sql = "CALL ObtenerFiltrosByID('$id')";
        $query = Executor::doit($sql);
			if(is_a($query[0],'mysqli_result')==1) {
				return Model::one($query[0],new FiltrosData());
			}
	}
    
	///============ EDITAR FILTROS  ============\\\
	static function EditarFiltro($id, $nombre, $tipo_elemento, $opcion_1, $opcion_2, $opcion_3){
 		$sql = "CALL ActualizarFiltro(
			'$id',
			'$nombre',
			'$tipo_elemento',
			'$opcion_1',
			'$opcion_2',
			'$opcion_3'
			)";
        $query = Executor::doit($sql);
			if(is_a($query[0],'mysqli_result')==1) {
				return Model::one($query[0],new FiltrosData());
			}
	}
    
	///============ ELIMINAR FILTROS  ============\\\
	static function EliminarFiltro($id){
 		$sql = "CALL EliminarFiltro('$id')";
       
        $query = Executor::doit($sql);
			if(is_a($query[0],'mysqli_result')==1) {
				return Model::one($query[0],new FiltrosData());
			}
	}

	///============ AGREGAR FILTROS  ============\\\
	static function InsertarFiltro($nombre, $tipo_elemento, $opcion_1, $opcion_2, $opcion_3){
 		$sql = "CALL InsertarFiltro(
			'$nombre',
			'$tipo_elemento',
			'$opcion_1',
			'$opcion_2',
			'$opcion_3'
			)";
        $query = Executor::doit($sql);
			if(is_a($query[0],'mysqli_result')==1) {
				return Model::one($query[0],new FiltrosData());
			}
	}
}