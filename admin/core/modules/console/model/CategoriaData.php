<?php

	// --------------------------------------
	// Servicio de Categoria
	// --------------------------------------

class CategoriaData
{
    ///============ CARGAR CATEGORIA ============\\\
	static function create($nombre,$principal,$destacado,$descuento){
		$sql = "CALL InsertarCategoria(
			'$nombre',
			'$principal',
			'$destacado',
			'$descuento'
		)";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}

	  ///============ OBTENER ID ============\\\
	static function ObtenerCategoriaID($nombre){
		$sql = "CALL ObtenerCategoriaID(
			'$nombre'
		)";
        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}


	///============ CARGAR DETALLES DE CATEGORIA (Descuento) ============\\\
	static function InsertarDescuento($categorias_id, $descuento, $descuento_desde, $descuento_hasta){
		$sql = "CALL InsertarDescuento(
			'$categorias_id',
			'$descuento',
			'$descuento_desde',
			'$descuento_hasta'
		)";
        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}

	///============ CARGAR DETALLES DE CATEGORIA (Destacado) ============\\\
	static function InsertarDestacado($categorias_id, $destacado_desde, $destacado_hasta){
		$sql = "CALL InsertarDestacado(
			'$categorias_id',
			'$destacado_desde',
			'$destacado_hasta'
		)";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}


	///============ CARGAR SUB-CATEGORIA ============\\\
		static function subcategoriacreate($nombre,$principal,$destacado,$descuento){
			$sql = "CALL InsertarSubCategoria(
				'$nombre',
				'$principal',
				'$destacado',
				'$descuento'
			)";

			$query = Executor::doit($sql);

			if(is_a($query[0],'mysqli_result')==1) {
				return Model::one($query[0], new CategoriaData());
			}

		}
	
    ///============ OBTENER ID SUBCATEGORIA ============\\\
	static function ObtenerSubCategoriaID(){
		$sql = "CALL ObtenerSubCategoriaID()";
        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}


	
	///============ CARGAR DETALLES DE SUBCATEGORIA (Descuento) ============\\\
	static function InsertarSubCategoriaDescuento($subcategoria_id, $descuento, $descuento_desde, $descuento_hasta){
		$sql = "CALL InsertarSubCategoriaDescuento(
			'$subcategoria_id',
			'$descuento',
			'$descuento_desde',
			'$descuento_hasta'
		)";
        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}

	///============ CARGAR DETALLES DE SUBCATEGORIA (Destacado) ============\\\
	static function InsertarSubCategoriaDestacada($subcategoria_id, $destacado_desde, $destacado_hasta){
		$sql = "CALL InsertarSubCategoriaDestacada(
			'$subcategoria_id',
			'$destacado_desde',
			'$destacado_hasta'
		)";
        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}


	///============ CARGAR DETALLES DE FILTRO - SUBCATEGORIA ============\\\
	static function InsertarFiltroSubCategoria($subcategoria_id, $filt){
		$sql = "CALL InsertarFiltroSubCategoria(
			'$subcategoria_id',
			'$filt'
		)";
        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}

	///============ CARGAR CATEGORIA - SUBCATEGORIA ============\\\
	static function InsertarCategorias_SubCategorias($subcategoria_id, $categoria_id){
		$sql = "CALL InsertarCategorias_SubCategorias(
			'$subcategoria_id',
			'$categoria_id'
		)";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new CategoriaData());
        }

	}

}
