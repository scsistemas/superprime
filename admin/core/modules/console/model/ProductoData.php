<?php

class ProductoData
{
    public function ProductoData()
    {
    }

    public static function ObtenerProductos($p_fecha_desde, $p_fecha_hasta, $p_tipo_prod, $p_marca, $p_genero, $p_publicado, $p_desde, $p_hasta, $p_cod_prod, $p_desc_sap, $p_foto, $p_publicado_ml,$p_ignorados)
    {
        $sql = "CALL ObtenerProductos('$p_fecha_desde','$p_fecha_hasta','$p_tipo_prod','$p_marca','$p_genero','$p_publicado',$p_desde,$p_hasta,'$p_cod_prod','$p_desc_sap','$p_foto','$p_publicado_ml','$p_ignorados')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }

    public static function ContarProductos($p_fecha_desde, $p_fecha_hasta, $p_tipo_prod, $p_marca, $p_genero, $p_publicado, $p_cod_prod, $p_desc_sap, $p_foto, $p_publicado_ml,$p_ignorados)
    {
        $sql = "CALL ContarProductos('$p_fecha_desde','$p_fecha_hasta','$p_tipo_prod','$p_marca','$p_genero','$p_publicado','$p_cod_prod','$p_desc_sap','$p_foto','$p_publicado_ml','$p_ignorados')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }

    public static function  ObtenerProductosSincronizados($p_id_sinc, $p_desde, $p_hasta)
    {
        $sql = "CALL  ObtenerProductosSincronizados($p_id_sinc,$p_desde,$p_hasta)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }

    public static function ContarProductosSincronizados($p_id_sinc)
    {
        $sql = "CALL ContarProductosSincronizados($p_id_sinc)";
        $query = Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }

    public static function ObtenerDetalleProducto($p_cod_prod)
    {
        $sql = "CALL ObtenerDetalleProducto('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }
    public static function ObtenerDetalleProductoListado($p_cod_prod)
    {
        $sql = "CALL  ObtenerDetalleProductoListado('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }

    public static function ObtenerTopProductos($p_cod_prod)
    {
        $sql = "CALL  ObtenerTopProductos('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }

    public static function ObtenerColoresProducto($p_cod_prod)
    {
        $sql = "CALL  ObtenerColoresProducto('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerVideosProducto($p_cod_prod)
    {
        $sql = "CALL  ObtenerVideosProducto('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerCaracProducto($p_cod_prod)
    {
        $sql = "CALL  ObtenerCaracProducto('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ActualizaProducto($p_cod_prod, $p_descsap, $p_nombre, $p_modelo, $p_alto, $p_ancho, $p_profundidad,
                                             $p_peso, $p_brief, $p_busquedas, $p_prime, $p_publicado, $p_notificaciones,$publicado_ml)
    {
        $sql = "CALL  ActualizaProducto('$p_cod_prod','$p_descsap','$p_nombre','$p_modelo',$p_alto,$p_ancho,$p_profundidad,
								        $p_peso,'$p_brief','$p_busquedas','$p_prime','$p_publicado','$p_notificaciones','$publicado_ml')";
        Executor::doit($sql);
    }

    public static function ActualizaColorProducto($p_cod_prod, $p_color, $p_desc_web, $p_mostrar_web, $p_color_princ, $p_color_sec)
    {
        $sql = "CALL ActualizarColorProducto('$p_cod_prod','$p_color','$p_desc_web','$p_mostrar_web','$p_color_princ','$p_color_sec')";
        Executor::doit($sql);
    }

    public static function ActualizarColoresTallas($p_cod_prod, $p_color, $p_talla, $p_mostrar_web)
    {
        $sql = "CALL ActualizarColoresTallas ('$p_cod_prod','$p_color','$p_talla','$p_mostrar_web')";
        Executor::doit($sql);
    }


    public static function EliminarImagenesColor($p_cod_prod, $p_color)
    {
        $sql = "CALL EliminarImagenesColor('$p_cod_prod','$p_color')";
        Executor::doit($sql);
    }
    public static function InsertarImagenProducto($p_cod_prod, $p_color,$p_nombre)
    {
        $sql = "CALL InsertarImagenProducto('$p_cod_prod','$p_color','$p_nombre')";
        Executor::doit($sql);
    }
    public static function ActualizarPrecios($p_cod_prod, $p_color,$p_precio_web,$p_iva_web,$p_total_web,$p_porc_desc,$p_monto_desc)
    {
        $sql = "CALL ActualizarPrecios('$p_cod_prod', '$p_color',$p_precio_web,$p_iva_web,$p_total_web,$p_porc_desc,$p_monto_desc)";
        Executor::doit($sql);
    }
    public static function  EliminarVideosProducto($p_cod_prod)
    {
        $sql = "CALL EliminarVideosProducto('$p_cod_prod')";
        Executor::doit($sql);
    }
    public static function InsertarVideoProducto($p_cod_prod,$p_nombre)
    {
        $sql = "CALL InsertarVideoProducto('$p_cod_prod','$p_nombre')";
        Executor::doit($sql);
    }
    public static function EliminarCaractProducto($p_cod_prod)
    {
        $sql = "CALL EliminarCaractProducto('$p_cod_prod')";
        Executor::doit($sql);
    }
    public static function InsertarCaracProducto($p_cod_prod,$p_cod_carac)
    {
        $sql = "CALL InsertarCaracProducto('$p_cod_prod','$p_cod_carac')";
        Executor::doit($sql);
    }

    public static function ActualizaInventario($p_cod_tienda, $p_cod_producto, $p_cantidad,$p_cod_color,$p_cod_talla)
    {
        $sql = "CALL ActualizaInventario ('$p_cod_tienda','$p_cod_producto',$p_cantidad,'$p_cod_color','$p_cod_talla')";
        Executor::doit($sql);
    }
    
    public static function ObtenerCodigoTalla($desc_talla)
    {
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        $result = mysqli_query($con,"SELECT CodTalla from tallas WHERE DescTalla =  '$desc_talla'");
        if(!$result){
        }else{
            if ($row = $result->fetch_array()) {
                return $row['CodTalla'];
            }
        }

    }  

    public static function ObtenerDescTalla($cod_talla)
    {
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        $result = mysqli_query($con,"SELECT DescTalla from tallas WHERE CodTalla =  '$cod_talla'");
        if(!$result){
        }else{
            if ($row = $result->fetch_array()) {
                return $row['DescTalla'];
            }
        }

    }  

    public static function ActualizaPedido($p_cod_pedido,$p_cod_producto,$p_estatus,$p_cantidad,$p_cod_tienda,$p_cod_color,$p_cod_talla)
    {
        $sql = "CALL ActualizaPedido($p_cod_pedido, '$p_cod_producto',$p_estatus,$p_cantidad,'$p_cod_tienda','$p_cod_color','$p_cod_talla')";
        Executor::doit($sql);
    }
    public static function ObtenerPedidoProductosTienda($p_cod_pedido,$p_cod_producto,$p_cod_tienda,$p_cod_color,$p_cod_talla)
    {
        $sql = "CALL  ObtenerPedidoProductosTienda('$p_cod_pedido','$p_cod_producto','$p_cod_tienda','$p_cod_color','$p_cod_talla')";
        $query=Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }
    public static function ObtenerPedidos()
    {
        $sql = "CALL  ObtenerPedidos()";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerInventario()
    {
        $sql = "CALL  ObtenerInventario()";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function  ObtenerProductosPorColor($p_cod_producto,$p_cod_color,$p_cod_talla)
    {
        $sql = "CALL  ObtenerProductosPorColor('$p_cod_producto','$p_cod_color','$p_cod_talla')";
        $query=Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }
    public static function ObtenerNoImagenProducto($p_cod_producto)
    {
        $sql = "CALL  ObtenerNoImagenProducto('$p_cod_producto')";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function InsertarNoImagen($p_cod_prod,$p_cod_color)
    {
        $sql = "CALL InsertarNoImagen('$p_cod_prod','$p_cod_color')";
        Executor::doit($sql);
    }
    public static function  EliminarNoImagen($p_cod_prod,$p_cod_color)
    {
        $sql = "CALL  EliminarNoImagen('$p_cod_prod','$p_cod_color')";
        Executor::doit($sql);
    }

    public static function ObtenerMarcasProducto($p_cod_tipo_producto)
    {
        $sql = "CALL  ObtenerMarcasProducto('$p_cod_tipo_producto')";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function  ObtenerMarcasListado($p_subcategoria)
    {
        $sql = "CALL ObtenerMarcasListado($p_subcategoria)";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerGenerosProducto($p_cod_tipo_producto)
{
    $sql = "CALL  ObtenerGenerosProducto('$p_cod_tipo_producto')";
    $query=Executor::doit($sql);
    return Model::many($query[0], new GeneralData());
}

    public static function ObtenerCategorias()
    {
        $sql = "CALL  ObtenerCategorias()";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerSubCategPorCateg($p_id_categoria)
    {
        $sql = "CALL  ObtenerSubCategPorCateg($p_id_categoria)";
        $query=Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ActualizaIgnorado($p_cod_prod,$p_ignorado)
    {
        $sql = "CALL ActualizaIgnorado('$p_cod_prod','$p_ignorado')";
        Executor::doit($sql);
    }

    public static function insertarPublicacion($p_codProducto, $p_codColor, $p_plan, $p_categorias, $p_idPubML){
        $sql = "CALL insertarPublicacion('$p_codProducto','$p_codColor','$p_plan','$p_categorias','$p_idPubML')";
        Executor::doit($sql);
    }

    public static function actualizarPublicadoProducto($p_codProducto, $p_publicado){
        $sql = "CALL actualizarPublicadoProducto('$p_codProducto','$p_publicado')";
        Executor::doit($sql);
    }

    public static function actualizarPublicacion($p_codProducto, $p_codColor, $p_plan, $p_categorias, $p_idPubML){
        $sql = "CALL actualizarPublicacion('$p_codProducto', '$p_codColor', '$p_plan', '$p_categorias', '$p_idPubML')";
        Executor::doit($sql);
    }

    public static function consultarPublicacionProducto($p_codProducto){
        $sql = "CALL consultarPublicacionProducto('$p_codProducto')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }

    public static function  ObtenerPublicacionProductoXIdML($p_idML){
        $sql = "CALL  ObtenerPublicacionProductoXIdML('$p_idML')";
        $query=Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }

    public static function ObtenerListadoProductos($p_id_categoria, $p_id_subcategoria,$p_desde,$p_hasta,$p_marca,$p_minimo,$p_maximo,$p_oferta,$p_prime,$p_nombre)
    {
        $sql = "CALL ObtenerListadoProductos('$p_id_categoria','$p_id_subcategoria',$p_desde,$p_hasta,'$p_marca','$p_minimo','$p_maximo','$p_oferta','$p_prime','$p_nombre')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }
    public static function ContarListadoProductos($p_id_categoria, $p_id_subcategoria,$p_marca,$p_minimo,$p_maximo,$p_oferta,$p_prime,$p_nombre)
    {
        $sql = "CALL ContarListadoProductos('$p_id_categoria','$p_id_subcategoria','$p_marca','$p_minimo','$p_maximo','$p_oferta','$p_prime','$p_nombre')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }

    public static function  ObtenerMaxMinPrecios($p_id_categoria, $p_id_subcategoria,$p_marca,$p_oferta,$p_prime,$p_nombre)
    {
        $sql = "CALL  ObtenerMaxMinPrecios('$p_id_categoria', '$p_id_subcategoria','$p_marca','$p_oferta','$p_prime','$p_nombre')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }

    public static function ObtenerColoresProductoWeb($p_cod_prod)
    {
        $sql = "CALL  ObtenerColoresProductoWeb('$p_cod_prod')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerTallasProductoWeb($p_cod_prod,$p_cod_color)
    {
        $sql = "CALL  ObtenerTallasProductoWeb('$p_cod_prod','$p_cod_color')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerProductosPorPedido($p_id_pedido,$p_id_categoria)
    {
        $sql = "CALL  ObtenerProductosPorPedido('$p_id_pedido','$p_id_categoria')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }
    public static function ObtenerCodigoPedido()
    {
        $sql = "CALL ObtenerCodigoPedido()";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }

    public static function CrearOrden($p_id_user)
    {
        $sql = "CALL  CrearOrden($p_id_user)";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }


    public static function CrearOrdenPedido($p_cod_pedido,$p_id_user,$p_cod_producto,$p_cod_color,$p_cod_tienda,$p_cantidad,$p_estatus,$p_fecha_estatus,$p_cod_talla,$envio)
    {
        $sql = "CALL CrearOrdenPedido($p_cod_pedido,$p_id_user,'$p_cod_producto','$p_cod_color','$p_cod_tienda',$p_cantidad,$p_estatus,'$p_fecha_estatus','$p_cod_talla','$envio')";
        Executor::doitDelorUpdate($sql);
    }


    public static function ObtenerInventarioTiendaTallaColor($p_cod_tienda,$p_cod_producto,$p_cod_color,$p_cod_talla)
    {
        $sql = "CALL ObtenerInventarioTiendaTallaColor('$p_cod_tienda','$p_cod_producto','$p_cod_color','$p_cod_talla')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }

    public static function ObtenerDistanciaCiudades($p_estado,$p_ciudad)
    {
        $sql = "CALL ObtenerDistanciaCiudades($p_estado,$p_ciudad)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerTiendasCiudad($p_estado,$p_ciudad,$p_ML)
    {
        $sql = "CALL ObtenerTiendasCiudad($p_estado,$p_ciudad,'$p_ML')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerInventarioTallaColor($p_cod_producto,$p_cod_color,$p_cod_talla)
    {
        $sql = "CALL ObtenerInventarioTallaColor('$p_cod_producto','$p_cod_color','$p_cod_talla')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }
    public static function ValidaOrdenML($p_ordenML)
    {
        $sql = "CALL  ValidaOrdenML('$p_ordenML')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }

    public static function ObtenerDetalleCompraExpress($p_cod_pedido,$p_id_user)
    {
        $sql = "CALL  ObtenerDetalleCompraExpress($p_cod_pedido,$p_id_user)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new ProductoData());
    }
    public static function  ObtenerDetalleTalla($p_cod_talla)
    {
        $sql = "CALL   ObtenerDetalleTalla('$p_cod_talla')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }
    public static function ObtenerDetalleColor($p_cod_producto,$p_cod_color)
    {
        $sql = "CALL  ObtenerDetalleColor('$p_cod_producto','$p_cod_color')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new ProductoData());
    }



}


?>