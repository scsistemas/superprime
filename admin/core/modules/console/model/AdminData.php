<?php

	// --------------------------------------
	// Servicio de Administradores
	// --------------------------------------

class AdminData
{
    ///============ Obtener Administradores ============\\\
	static function ObtenerAdministradores(){
		$sql = "CALL ObtenerAdministradores()";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new AdminData());
        }

	}
    ///============ Obtener listado de acceso ============\\\
	static function ObtenerMenus(){
		$sql = "CALL ObtenerMenus()";

        $query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new AdminData());
        }

	}

    ///============ Agregar nuevo usuario ============\\\
	static function CrearAdmin($nombre,$apellido,$correo,$telefono,$acceso, $contrasena_temporal, $numid, $tipoid){
		$sql = "CALL InsertarAdministrador(
			'$nombre',
			'$apellido',
			'$correo',
			'$telefono',
			'$acceso',
			'$contrasena_temporal',
			'$numid',
			'$tipoid'
		)";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new AdminData());
        }
	}


    ///============ Update Usuario Admin ============\\\
	static function UpdateAdmin($iduser, $acceso){
		$sql = "CALL ActualizarAdministrador(
			 $iduser,
			'$acceso'
		)";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new AdminData());
        }
	}

    ///============ Delete Usuario Admin ============\\\
	static function EliminarAdministrador($iduser, $acceso){
		$sql = "CALL EliminarAdministrador(
			 $iduser,
			'$acceso'
		)";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::many($query[0], new AdminData());
        }
	}


    ///============ Obtener usuario by ID============\\\
	static function ObtenerAdmin($iduser, $acceso){
		$sql = "CALL ObtenerAdministradorByID(
			 $iduser,
			'$acceso'	

		)";

		$query = Executor::doit($sql);

		if(is_a($query[0],'mysqli_result')==1) {
        	return Model::one($query[0], new AdminData());
        }

	}

}
