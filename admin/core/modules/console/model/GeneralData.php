<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18/5/2016
 * Time: 10:22 PM
 */

class GeneralData {
    public function GeneralData(){
    }

    public static function ObtenerColores(){
        $sql = "CALL ObtenerColores()";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function ObtenerGeneros(){
        $sql = "CALL  ObtenerGeneros()";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function ObtenerMarcas(){
        $sql = "CALL  ObtenerMarcas()";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function ObtenerTallas(){
        $sql = "CALL  ObtenerTallas()";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }
    public static function ObtenerTipoProducto(){
        $sql = "CALL  ObtenerTipoProducto()";
        $query = Executor::doit($sql);
        return Model::many($query[0],new GeneralData());
    }

    public static function ObtenerTotales()
    {
        $sql = "CALL  ObtenerTotales()";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerDestinatarios($p_id_proceso)
    {
        $sql = "CALL  ObtenerDestinatarios($p_id_proceso)";
        $query = Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }
    public static function  ObtenerActividadReciente($p_cantidad)
    {
        $sql = "CALL  ObtenerActividadReciente($p_cantidad)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerImagenesProducto($p_cod_prod,$p_color)
    {
        $sql = "CALL  ObtenerImagenesProducto('$p_cod_prod','$p_color')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function ObtenerTallasColor($p_cod_prod,$p_color)
    {
        $sql = "CALL ObtenerTallasColor('$p_cod_prod','$p_color')";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerPreciosProducto($p_cod_prod,$p_color)
    {
        $sql = "CALL ObtenerPreciosProducto('$p_cod_prod','$p_color')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }

    public static function InsertarEstado($p_cod_estado,$p_codjete,$p_desc)
    {
        $sql = "CALL InsertarEstado($p_cod_estado,'$p_codjete','$p_desc')";
        //echo '#'.$sql.'<br>';
         Executor::doit($sql);
    }
    public static function InsertarCiudad($p_codciudad,$p_cod_estado,$p_codjete,$p_desc)
    {
        $sql = "CALL InsertarCiudad($p_codciudad,$p_cod_estado,'$p_codjete','$p_desc')";
        //echo '**'.$sql.'<br>';
         Executor::doit($sql);
    }
    public static function InsertarMunicipio($p_cod_estado,$p_codciudad,$p_cod_municipio,$p_codjete,$p_desc)
    {
        $sql = "CALL InsertarMunicipio($p_cod_estado,$p_codciudad,$p_cod_municipio,'$p_codjete','$p_desc')";
        //echo '$$$'.$sql.'<br>';
        Executor::doit($sql);
    }
    public static function  InsertarParroquia($p_cod_estado,$p_codciudad,$p_cod_municipio,$p_cod_parroquia,$p_codjete,$p_desc)
    {
        $sql = "CALL InsertarParroquia($p_cod_estado,$p_codciudad,$p_cod_municipio,$p_cod_parroquia,'$p_codjete','$p_desc')";
        //echo '&&&&'.$sql.'<br>';
        Executor::doit($sql);
    }

    public static function ObtenerEstados($p_cod_estado)
    {
        $sql = "CALL  ObtenerEstados($p_cod_estado)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    public static function  ObtenerCiudades($p_cod_estado,$p_cod_ciudad)
    {
        $sql = "CALL   ObtenerCiudades($p_cod_estado,$p_cod_ciudad)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function ObtenerMunicipios($p_cod_estado,$p_cod_ciudad,$p_cod_municipio)
    {
        $sql = "CALL ObtenerMunicipios($p_cod_estado,$p_cod_ciudad,$p_cod_municipio)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }
    
    public static function ObtenerParroquias($p_cod_estado,$p_cod_ciudad,$p_cod_municipio)
    {
        $sql = "CALL  ObtenerParroquias($p_cod_estado,$p_cod_ciudad,$p_cod_municipio)";
        $query = Executor::doit($sql);
        return Model::many($query[0], new GeneralData());
    }

    public static function actualizarClaveValor($p_clave, $p_valor){
        $sql = "CALL actualizarClaveValor('$p_clave','$p_valor')";
        $query = Executor::doit($sql);
    }

    public static function insertarClaveValor($p_clave, $p_valor){
        $sql = "CALL insertarClaveValor('$p_clave','$p_valor')";
        $query = Executor::doit($sql);
    }

    public static function consultarClaveValor($p_clave){
        $sql = "CALL consultarClaveValor('$p_clave')";
        $query = Executor::doit($sql);
        return Model::one($query[0], new GeneralData());
    }

}