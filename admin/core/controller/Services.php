<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 13/10/15
 * Time: 11:05 AM
 */

class Services {/**
 * @function load
 * @brief la funcion load carga una vista correspondiente a un modulo
 **/
    public static function load($service){
        // Module::$module;
        if(!isset($_GET['service'])){
            include "core/modules/".Module::$module."/services/".$service."/widget-default.php";
        }else{


            if(Services::isValid()){
                include "core/modules/".Module::$module."/services/".$_GET['service']."/widget-default.php";
            }else{
                Services::Error("<b>404 NOT FOUND</b> Services <b>".$_GET['service']."</b> folder  !!");
            }



        }
    }

    /**
     * @function isValid
     * @brief valida la existencia de una vista
     **/
    public static function isValid(){
        $valid=false;
        if(isset($_GET["service"])){
            if(file_exists($file = "core/modules/".Module::$module."/services/".$_GET['service']."/widget-default.php")){
                $valid = true;
            }
        }
        return $valid;
    }

    public static function Error($message){
        print $message;
    }

}