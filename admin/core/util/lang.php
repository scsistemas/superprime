<?php

$lang = array(
    /* === ESPAÑOL ===*/
    'ES' => array(
        'lang' => '?lang=ES',
        'idioma' => 'Español',

        /* Secciones */
        //Registro Usuario
        'USUARIO_REGISTRADO' => 'Usuario registrado correctamente',
        'COMPLETAR_REGISTRO' => 'Estimado Usuario(a),<br> Gracias por su interés en nuestra página,<br>
                                 Con la finalidad de completar su registro, le invitamos a pulsar el siguiente',
        'COMPLETAR_REGISTRO_C' => 'y seguir las instrucciones que se indican <br> hasta <b>completar la totalidad de los datos.</b><br><br> 
                                  <b>Importante:</b> Esta es una cuenta no monitoreada. No responda o reenvíe correos a esta cuenta.',
        'USUARIO_ENCONTRADO' => 'Ya se encuentra registrado un usuario con este correo',
        'REGISTRO_USUARIO' => 'Registro de Usuario Super',
        'USUARIO_INVALIDO' => 'Usuario Invalido',
        //Correo Electronico
        'NO_SE_ENVIO' => 'No se pudo enviar el correo electrónico',
        'NO_SE_ENCUENTRA' => 'No existe usuario con ese Correo Electrónico',
        
        
        //Registro Persona
        'PERSONA_ENCONTRADA' => 'Ya se encuentra una persona registrada con esa identificacion',
        'PERSONA_REGISTRADA' => 'Persona registrada correctamente',
        'PERSONA_ACTUALIZADA' => 'Persona actualizada correctamente',
        // RECUPERAR CONTRASEÑA
        'SUBJ_RECUPERAR_CONTRASENA' =>'Restablecimiento de contraseña Super Prime',
        'RECUPERAR_CONTRASENA'=>'Estimado Usuario(a),<br> Recientemente ha solicitado que restablezcamos tu contraseña.<br>Para cambiar tu contraseña de <a href="https://www.superprime.com">www.superprime.com</a> haga clic ',
        'RECUPERAR_CONTRASENA_C' =>'o pegue el siguiente enlace en su navegador:',
        'RECUPERAR_CONTRASENA_D' =>'El enlace caducará a las 48 horas, por favor que asegúrese de utilizarlo inmediatamente.<br><br>
                                    ¡Gracias por usar superprime!<br>
                                    <b>Importante:</b> Esta es una cuenta no monitoreada. No responda o reenvíe correos a esta cuenta.',
        'CAMBIO_CONTRASENA' =>'Cambio de contraseña satisfactorio',
        //TOKEN
        'TOKEN_V' =>'Token Válido',
        'TOKEN_I' =>'Token Invalido',
        //DESEOS
        'DESEO_REGISTRADO' => 'Deseo registrado correctamente',
        'DESEO_ELIMINADO' => 'Deseo eliminado correctamente',
        //DIRECCIONES
        'DIRECION_REGISTRADA' => 'Direccion registrada correctamente',
        'DIRECION_ACTUALIZADA' => 'Direccion actualizada correctamente',
        'DIRECION_ELIMINADA' => 'Direccion eliminada correctamente',
        'DIRECION_NOENCONTRADA' => 'Direccion a eliminar no existe',
        'ALIAS_V' =>'Alias Válido',
        'ALIAS_I' =>'Alias Invalido',
        'ORDEN_V' =>'Orden Válida',
        'ORDEN_I' =>'Orden Invalida',
        //CORREOS
        'CAMBIO_CONTRASENA_MAIL_SUBJECT' => 'Cambio de contraseña satisfactorio',
        'CAMBIO_CONTRASENA_MAIL_BODY' => 'Su cambio contraseña en el sitio web SUPERPRIME ha sido satisfactorio',
        'CAMBIO_DATOS_PERSONALES_MAIL_SUBJECT' => 'Actualizacion de datos personales',
        'CAMBIO_DATOS_PERSONALES_MAIL_BODY' => 'La actualizacion de datos personales en el sitio SUPERPRIME ha sido satisfactorio',
        'CAMBIO_DIRECCION_MAIL_BODY' => 'Actuaizacion de Direccion',
        'CAMBIO_DIRECCION_MAIL_SUBJECT' => 'La actualizacion de direccion en el sitio web SUPERPRIME ha sido satisfactorio',
        'CARRRO_REGISTRADO' =>'Producto agregado al carrito',
        'CARRRO_ELIMINADO' =>'Producto eliminado del carrito',
        'CARRRO_EDITADO' =>'Producto editado en el carrito'
       ),

    /* === INGLES ===*/
    'EN' => array(
        'lang' => '?lang=EN',
        'idioma' => 'Ingles',

        /* Secciones */
        //Registro Usuario
        'USUARIO_REGISTRADO' => 'Usuario registrado correctamente',
        'COMPLETAR_REGISTRO' => 'Estimado Usuario:\nPara completar su registro debe conectarse a:',
        'USUARIO_ENCONTRADO' => 'Ya se encuentra registrado un usuario con este correo',
        //Correo Electronico
        'NO_SE_ENVIO' => 'No se pudo enviar el correo electrónico',
        'NO_SE_ENCUENTRA' => 'No existe usuario con ese Correo Electrónico',
        //Registro Persona
        'PERSONA_ENCONTRADA' => 'Ya se encuentra una persona registrada con esa identificacion',
        'PERSONA_REGISTRADA' => 'Persona registrada correctamente',
        'PERSONA_ACTUALIZADA' => 'Persona actualizada correctamente',
        'PERSONA_NO' => 'Persona a actualizar no existe',
        // RECUPERAR CONTRASEÑA
        'RECUPERAR_CONTRASENA' =>'Estimado Usuario:\nPara recuperar su contraseña debe conectarse a:',
        'CAMBIO_CONTRASENA' =>'Cambio de contraseña satisfactorio',
        //TOKEN
        'TOKEN_V' =>'Token Válido',
        'TOKEN_I' =>'Token Invalido',
        //DESEOS
        'DESEO_REGISTRADO' => 'Deseo registrado correctamente',
        'DESEO_ELIMINADO' => 'Deseo eliminado correctamente',
        //DIRECCIONES
        'DIRECION_REGISTRADA' => 'Direccion registrada correctamente',
        'DIRECION_ACTUALIZADA' => 'Direccion actualizada correctamente',
         'DIRECION_ELIMINADA' => 'Direccion eliminada correctamente',
        'DIRECION_NOACTULIZADA' => 'Direccion a actualizar no existe',
        'ALIAS_V' =>'Alias Válido',
        'ALIAS_I' =>'Alias Invalido',
        'ORDEN_V' =>'Orden Válida',
        'ORDEN_I' =>'Orden Invalida',
        'CARRRO_REGISTRADO' =>'Producto agregado al carrito',
        'CARRRO_ELIMINADO' =>'Producto eliminado del carrito',
        'CARRRO_EDITADO' =>'Producto editado en el carrito'

    ),

    /* === PORTUGUES ===*/
    'PT' => array(
        'lang' => '?lang=PT',
        'idioma' => 'Portugues',

        /* Secciones */
        //Registro Usuario
        'USUARIO_REGISTRADO' => 'Usuario registrado correctamente',
        'COMPLETAR_REGISTRO' => 'Estimado Usuario:\nPara completar su registro debe conectarse a:',
        'USUARIO_ENCONTRADO' => 'Ya se encuentra registrado un usuario con este correo',
        //Correo Electronico
        'NO_SE_ENVIO' => 'No se pudo enviar el correo electrónico',
        'NO_SE_ENCUENTRA' => 'No existe usuario con ese Correo Electrónico',
        //Registro Persona
        'PERSONA_ENCONTRADA' => 'Ya se encuentra una persona registrada con esa identificacion',
        'PERSONA_REGISTRADA' => 'Persona registrada correctamente',
        // RECUPERAR CONTRASEÑA
        'RECUPERAR_CONTRASENA' =>'Estimado Usuario:\nPara recuperar su contraseña debe conectarse a:',
        'CAMBIO_CONTRASENA' =>'Cambio de contraseña satisfactorio',
        //TOKEN
        'TOKEN_V' =>'Token Válido',
        'TOKEN_I' =>'Token Invalido',
        //DESEOS
        'DESEO_REGISTRADO' => 'Deseo registrado correctamente',
        'DESEO_ELIMINADO' => 'Deseo eliminado correctamente',
        //DIRECCIONES
        'DIRECION_REGISTRADA' => 'Direccion registrada correctamente',
        'DIRECION_ACTUALIZADA' => 'Direccion actualizada correctamente',
        'DIRECION_ELIMINADA' => 'Direccion eliminada correctamente',
        'DIRECION_NOENCONTRADA' => 'Direccion a eliminar no existe',
        'DIRECION_NOACTULIZADA' => 'Direccion a actualizar no existe',
        'ALIAS_V' =>'Alias Válido',
        'ALIAS_I' =>'Alias Invalido',
        'ORDEN_V' =>'Orden Válida',
        'ORDEN_I' =>'Orden Invalida'
    ),
);
?>