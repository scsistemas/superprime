<?php

class UtilSitef
{

    public static function callSitef($endpoint, $mUSN, $orderId, $merchant, $merchantKey, $cardnumber, $cvc, $ccType, $amount, $fecha, $ccHolder, $cctipoid, $cedula)
    {


        $wsdl = true;
        $proxyhost = false;
        $proxyport = false;
        $proxyusername = false;
        $proxypassword = false;
        $timeout = 0;
        $response_timeout = 300;

        $client = new nusoap_client($endpoint, $wsdl, $proxyhost, $proxyport, $proxyusername, $proxypassword, $timeout, $response_timeout);

        $err = $client->getError();
        if ($err) {
            return false;
        }

        $mes = substr($fecha, 0, -5);
        $ano = substr($fecha, -2);
        $fecha = $mes . $ano;
        $ccid = str_replace(' ', '', $cctipoid) . str_replace(' ', '', $cedula);
        $ccnum = str_replace(' ', '', $cardnumber);
        $cvc = str_replace(' ', '', $cvc);
        $cctype = number_format(str_replace(' ', '', $ccType));
        $amount = str_replace(',', '', $amount);
        $amount = number_format(str_replace('.', '', $amount), 0, '', '');

        $transactionRequest = array('transactionRequest' => array(
            'amount' => $amount,
            'extraField' => '',
            'merchantId' => $merchant,
            'merchantUSN' => $mUSN,
            'orderId' => $orderId));

        $payment = $client->getProxy();

        $transactionResponse = $payment->beginTransaction($transactionRequest);

        $nit = $transactionResponse['transactionResponse']['nit'];

        //var_dump($transactionRequest);
        //var_dump($transactionResponse);

        $paymentRequest = array('paymentRequest' => array(
            'authorizerId' => $cctype,
            'autoConfirmation' => 'true',
            'cardExpiryDate' => $fecha,
            'cardNumber' => $ccnum,
            'cardSecurityCode' => $cvc,
            'customerId' => $ccid,
            'extraField' => '',
            'installmentType' => '4',
            'installments' => '1',
            'nit' => $nit));

        $result = null;
        try {
            set_time_limit (90);
            $result = $payment->doPayment($paymentRequest);
	        if($result == null){
                for ($i = 1; $i <= 3; $i++) {
                    try {
                        set_time_limit (90);
                        $getStatusRequest = array('merchantKey' => $merchantKey, 'nit' => $nit);
                        $result = $payment->getStatus($getStatusRequest);
                        if($result != null){
                       // echo 'getStatus Success '.$i;
                        break;}else {echo 'getStatus Fail '.$i;}
                    } catch (Exception $e) {
                       // echo 'getStatus Fail '.$i;
                    }
                }
            }else {
           // echo 'doPayment Success';
            }
        } catch (Exception $e) {
	        //echo 'doPayment Fail';

		   for ($i = 1; $i <= 3; $i++) {
			   try {
		            set_time_limit (90);
					$getStatusRequest = array('merchantKey' => $merchantKey, 'nit' => $nit);
					$result = $payment->getStatus($getStatusRequest);
			       // echo 'getStatus Success '.$i;
			   } catch (Exception $e) {
			        //echo 'getStatus Fail '.$i;
			   }
		   }

        }

        //echo '<h2>Status</h2><pre>';
        //print_r($result);
        //echo '</pre>';

        if ($result == null) {
           // echo 'Se ha perdido la comunicacion con la plataforma de pago, por favor, no intente realizar el pago nuevamente y comuniquese con nuestro centro de atenci贸n';
            //throw new Exception($this->msg_error);
            return false;
        }else {

        if ($client->fault) {
           // echo '$client->fault ' . $client->fault;
            return false;
        } else {
            $err = $client->getError();
            if ($err) {
                return false;
            } else {
                $responseCode = $result['paymentResponse']['responseCode'];

                return ($responseCode);
            }
        }
    }}
}

?>