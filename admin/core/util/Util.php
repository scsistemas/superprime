<?php

class Util
{

    private static $Key = "SuperPrime";

    public static function encrypt($input)
    {
        $output = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(Util::$Key), $input, MCRYPT_MODE_CBC, md5(md5(Util::$Key))));
        return $output;
    }

    public static function desde($p_num_pagina, $p_num_registros)
    {

        return ($p_num_pagina * $p_num_registros);
    }

    public static function hasta($p_num_pagina, $p_num_registros)
    {

        return ($p_num_pagina + 1) * $p_num_registros;
    }

    public static function ObtenerEstadosJetes()
    {

        $url = 'http://api-jetes.azurewebsites.net/api/get/estados';
        $resp = file_get_contents($url);
        return json_decode($resp);
    }

    public static function ObtenerCiudadesJetes($cod_estado)
    {

        $url = 'http://api-jetes.azurewebsites.net/api/get/ciudades?estado=' . $cod_estado;
        $resp = file_get_contents($url);
        return json_decode($resp);
    }

    public static function ObtenerMunicipiosJetes($cod_estado, $cod_ciudad)
    {

        $url = 'http://api-jetes.azurewebsites.net/api/get/municipios?ciudad=' . $cod_ciudad . '&estado=' . $cod_estado;
        $resp = file_get_contents($url);
        return json_decode($resp);
    }

    public static function ObtenerParroquiasJetes($cod_estado, $cod_ciudad, $cod_municipio)
    {

        $url = 'http://api-jetes.azurewebsites.net/api/get/parroquias?municipio=' . $cod_municipio . '&ciudad=' . $cod_ciudad . '&estado=' . $cod_estado;
        $resp = file_get_contents($url);
        return json_decode($resp);
    }

    public static function ObtenerToken()
    {
        Return sha1(rand(0, 999) . rand(999, 9999) . rand(1, 300));

    }

    public static function enviar_correo($email, $subject, $body)
    {
        require_once('PHPMailer/class.phpmailer.php');
        require_once('PHPMailer/class.smtp.php');

        //Create a new PHPMailer instance
        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;                               // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = host_prime;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->FromName = "SuperPrime";
        $mail->Username = correo_notificacion;                 // SMTP username
        $mail->Password = contrasena_notificacion;                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                                    // TCP port to connect to

        $mail->setFrom(correo_notificacion, 'SuperPrime');
        $mail->addAddress($email);
        //$mail->addReplyTo('info@example.com', 'Information');
        //$mail->addCC('cc@example.com');
        //$mail->addBCC('bcc@example.com');

        // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
        //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        $mail->isHTML(true);
        // Set email format to HTML
        $mail->CharSet = 'UTF-8';

        $mail->Subject = $subject;
        $mail->Body = $body;
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        return $mail;
    }

    public static function Getfloat($str)
    {
        if (strstr($str, ",")) {
            $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
            $str = str_replace(",", ".", $str); // replace ',' with '.'
        }

        if (preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.'
            return floatval($match[0]);
        } else {
            return floatval($str); // take some last chances with floatval
        }
    }

    public static function arrayAString($no_imagenes)
    {
        try {
            $acolores = array();
            foreach ($no_imagenes as $color) {
                $acolores[] = $color->DescColor;
            }

            implode($acolores, ",");
        } catch (Exception $e) {
            echo $e->getMessage();


        }
    }

    public static function ObtenerDistancia($lat1, $long1, $lat2, $long2)
    {
        $dlong = ($long1 - $long2);
        $dvalue = (sin($lat1 * degtorad) * sin($lat2 * degtorad))
            + (cos($lat1 * degtorad) * cos($lat2 * degtorad)
                * cos($dlong * degtorad));
        $dd = acos($dvalue) * radtodeg;
        $miles = ($dd * 69.16);
        $km = ($dd * 111.302);
        return $km;
    }

    public static function ObtenerTiendaGanadora($direccion, $producto, $p_ML)

    {
        $latitudu = $direccion->Latitud;
        $longitudu = $direccion->Longitud;
        $atienda = null;
        /// Obtengo las ciudadades desde la mas cercana a la mas lejana
        $ciudadest = ProductoData::ObtenerDistanciaCiudades($direccion->DescCiudad);
        if (count($ciudadest) > 0) {
            foreach ($ciudadest as $ciudad) {
                //Obtengo las tiendas de una ciudad
                $tiendas = ProductoData::ObtenerTiendasCiudad($ciudad->CiudadDestino, $p_ML);
                $atienda = self::ObtenerTienda($tiendas, $producto, $latitudu, $longitudu);
                if (!empty($atienda)) {
                    return $atienda;
                }


            }
        }
        return $atienda;
    }

    public static function ObtenerTiendaGanadoraML($latitudu, $longitudu,$estadou ,$ciudadu, $producto, $p_ML, $cod_pedido, $cliente,$envio)

    {
        $date = new DateTime();
        $hoy = $date->format('d-m-Y H:i:s');
        $htienda = false;
        $cod_tienda=null;
        /// Obtengo las ciudadades desde la mas cercana a la mas lejana
        $ciudadest = ProductoData::ObtenerDistanciaCiudades($estadou,$ciudadu);
        if (count($ciudadest) > 0) {
            foreach ($ciudadest as $ciudad) {
                //Obtengo las tiendas de una ciudad
                $tiendas = ProductoData::ObtenerTiendasCiudad($ciudad->CodEstadoDestino, $ciudad->CodCiudadDestino, $p_ML);
                $atiendas = array();
                ///ORDENAR EL ARREGLO DE LA MAS CERCANA A LA MAS LEJANA
                foreach ($tiendas as $tienda) {
                    //Obtengo la distancia entre la ciudad del usuario y la tienda
                    $distanciat = self::ObtenerDistancia($latitudu, $longitudu, $tienda->Latitud, $tienda->Longitud);
                    $tienda->distancia = $distanciat;
                    $atiendas[] = $tienda;
                }
                self::array_sort_by($atiendas, 'distancia', SORT_ASC);
                foreach ($atiendas as $tiendaord) {
                    $cantidad = ProductoData::ObtenerInventarioTiendaTallaColor($tiendaord->CodTienda, $producto->CodProducto, $producto->CodColor, $producto->CodTalla)->Cantidad - $producto->Cantidad;

                    if ($cantidad >= 0) {
                        $ws=$tiendaord->WS;
                        if (self::VerificaConexion($ws)) {
                            if (self::CrearPedidoPT($producto, $cod_pedido, $cliente,$ws,$envio)) {
                                // ACTUALIZAR INVENTARIO DE LA TIENDA
                                ProductoData::ActualizaInventario($tiendaord->CodTienda, $producto->CodProducto, $cantidad, $producto->CodColor, $producto->CodTalla);
                                $cod_tienda=$tiendaord->CodTienda;
                                $htienda = true;


                            }
                        }


                    }
                    if ($htienda)
                        break;


                }
                if ($htienda)
                    break;
            }
            //GENERAR ORDEN DE PEDIDO ACA CON O SIN TIENDA
            ProductoData::CrearOrdenPedido($cod_pedido,0,$producto->CodProducto, $producto->CodColor,$cod_tienda, $producto->Cantidad, 3, $hoy, $producto->CodTalla,$envio);
            if ($htienda)
                return true;
        }
        return false;

    }public static function ObtenerTiendaGanadoraCompra($latitudu, $longitudu,$estadou ,$ciudadu, $producto, $p_ML, $cod_pedido, $cliente,$envio)

    {
        $date = new DateTime();
        $hoy = $date->format('d-m-Y H:i:s');
        $htienda = false;
        $cod_tienda=null;
        /// Obtengo las ciudadades desde la mas cercana a la mas lejana
        $ciudadest = ProductoData::ObtenerDistanciaCiudades($estadou,$ciudadu);
        if (count($ciudadest) > 0) {
            foreach ($ciudadest as $ciudad) {
                //Obtengo las tiendas de una ciudad
                $tiendas = ProductoData::ObtenerTiendasCiudad($ciudad->CodEstadoDestino, $ciudad->CodCiudadDestino, $p_ML);
                $atiendas = array();
                ///ORDENAR EL ARREGLO DE LA MAS CERCANA A LA MAS LEJANA
                foreach ($tiendas as $tienda) {
                    //Obtengo la distancia entre la ciudad del usuario y la tienda
                    $distanciat = self::ObtenerDistancia($latitudu, $longitudu, $tienda->Latitud, $tienda->Longitud);
                    $tienda->distancia = $distanciat;
                    $atiendas[] = $tienda;
                }
                self::array_sort_by($atiendas, 'distancia', SORT_ASC);
                foreach ($atiendas as $tiendaord) {
                    $cantidad = ProductoData::ObtenerInventarioTiendaTallaColor($tiendaord->CodTienda, $producto->CodProducto, $producto->CodColor, $producto->CodTalla)->Cantidad - $producto->Cantidad;

                    if ($cantidad >= 0) {
                        $ws=$tiendaord->WS;
                      //  if (self::VerificaConexion($ws)) {
                        //    if (self::CrearPedidoPT($producto, $cod_pedido, $cliente,$ws,$envio)) {
                                // ACTUALIZAR INVENTARIO DE LA TIENDA
                                ProductoData::ActualizaInventario($tiendaord->CodTienda, $producto->CodProducto, $cantidad, $producto->CodColor, $producto->CodTalla);
                                $cod_tienda=$tiendaord->CodTienda;
                                $htienda = true;


                           // }
                       // }


                    }
                    if ($htienda)
                        break;


                }
                if ($htienda)
                    break;
            }
            //GENERAR ORDEN DE PEDIDO ACA CON O SIN TIENDA
            ProductoData::CrearOrdenPedido($cod_pedido,0,$producto->CodProducto, $producto->CodColor,$cod_tienda, $producto->Cantidad, 3, $hoy, $producto->CodTalla,$envio);
            if ($htienda)
                return true;
        }
        return false;

    }




    public static function ObtenerDireccionGoogle($direccion)
    {
        $url = GOOGLE_URL . urlencode($direccion) . '&key=' . GOOGLE_APIKEY;
        $resp = file_get_contents($url);
        return json_decode($resp);
    }


    function array_sort_by(&$arrIni, $col, $order = SORT_ASC)
    {
        $arrAux = array();
        foreach ($arrIni as $key=> $row)
        {
            $arrAux[$key] = is_object($row) ? $arrAux[$key] = $row->$col : $row[$col];
            $arrAux[$key] = strtolower($arrAux[$key]);
        }
        array_multisort($arrAux, $order, $arrIni);
    }


    public static function VerificaConexion($ws)
    {
        $client = new SoapClient($ws, array(  'soap_version' => SOAP_1_1,
                                        'trace' => true,
                                        )); 

        try {
           $params = array();    
           $Response = $client->__soapCall( 'VerificarConectividad', $params );

           return $Response->VerificarConectividadResult;

        } catch (SoapFault $s) {
            return false;
        }

    }


    public static function CrearPedidoPT($producto, $cod_pedido, $cliente,$ws,$envio)
    {
        //echo "Creando Pedido PT";
        
        $client = new SoapClient($ws, array(  'soap_version' => SOAP_1_1,
                                        'trace' => true,
                                        )); 

        // Datos Basicos de la Cotizacion
        $articulo1 = new stdClass();
        $desc_talla = ProductoData::ObtenerDescTalla($producto->CodTalla);
        $articulo1->Codigo = $producto->CodProducto . $producto->CodColor . $desc_talla . '0';
        //Instruccion con el codigo correcto dela talla $articulo1->Codigo = $producto->CodProducto . $producto->CodColor .     $producto->CodTalla;
        $articulo1->Cantidad = $producto->Cantidad;
        $articulo1->PrecioUnitario = $producto->Precios->PrecioWeb;
        $articulo1->TasaImpuesto = $producto->Precios->IvaWeb;

        $articulos = array($articulo1);

        $cli = new stdClass();
        $cli->Nombre = $cliente->Nombre;
        $cli->Rif = $cliente->Numid;
        $cli->DireccionEntrega = $envio=='S'?$cliente->Direccion:$cliente->DireccionF;
        $cli->DireccionFiscal =  $cliente->DireccionF;
        $cli->Referencia = "";


        $pedido = new stdClass();
        $pedido->NumeroPedido = $cod_pedido;
        $pedido->NumeroTransaccion =$cod_pedido;
        $pedido->Articulos = $articulos;
        $pedido->Cliente = $cli;
        $pedido->Origen = "MercadoLibre"; // Tienda o MercadoLibre
        $pedido->TipoEnvio =$envio=='S'?"EnvioPorEncomienda":"RetiroEnTienda"; // RetiroEnTienda o EnvioPorEncomienda
        $pedido->TipoPago = "0";


        $nuevopedido = new stdClass();
        $nuevopedido->pedido = $pedido;

        try {
           $params = array($nuevopedido);    
           $Response = $client->__soapCall( 'FacturarPedido', $params );
           //echo 'Nuevo pedido:';
           //print_r($nuevopedido);
        
           //echo 'Respuesta:';
           //print_r($Response);

           if(empty($Response->FacturarPedidoResult)){

              try {
                $params2 = array($nuevopedido);    
                $Response2 = $client->__soapCall( 'AsignarCliente', $params2 );
                //echo 'Asignar Ciente:';
                //print_r($nuevopedido);
        
                //echo 'Respuesta:';
                //print_r($Response2);

                if(empty($Response2->AsignarClienteResult)){
       		    return true;
        
                }else{
	            return false;
                }

              } catch (SoapFault $s) {
                 return false;
              }

            }else{
                if($Response->FacturarPedidoResult->IError=='117'||$Response->FacturarPedidoResult->IError=='118'||$Response->FacturarPedidoResult->IError=='120'){
                    // Debo evaluar $pedidoResponse->FacturarPedidoResult->Articulos para ver cuantos me vendió la tienda
                    return false;
                }else{   
	            return false;//se deben capturar excepciones inesperadas
                }
            }



        } catch (SoapFault $s) {
            return false;
        }
    }
    public static function CrearEnvio($producto)
    {

      try {
        $data['courier']  ='5632794f90075e9f5d70902c';//SOLAMENTE ZOOM POR AHORA;
        $data['estado']  =$producto->Direccion->CodJEstado;
        $data['ciudad']  =$producto->Direccion->CodJCiudad;
        $data['municipio']  =$producto->Direccion->CodJMunicipio;
        $data['parroquia']  =$producto->Direccion->CodJParroquia;
        $data['contacto']  =$producto->Direccion->NombrePersona;
        $data['cirif']  =$producto->Direccion->NumId;
        $data['telefono']  =$producto->Direccion->TelCel;
        $data['direccion']  =$producto->Direccion->CalleUno.' '.$producto->Direccion->CalleDos.' '.$producto->Direccion->TipoVivienda.' '.$producto->Direccion->Nombre.' '.$producto->Direccion->Piso .' '.$producto->Direccion->Apto.' '.$producto->Direccion->DescCiudad.' '.$producto->Direccion->DesEstado ;
        $data['inmueble']  =$producto->Direccion->TipoVivienda;
        $data['descripcionPaquete']  =$producto->Direccion->DescSap;
        $data['referencia']  =$producto->Pedido;
        $data['numeroPiezas']  =$producto->Cantidad;
        $data['peso']  =$producto->Peso;
        $data['tipoEnvio']  ='M';
        $data['valor']  = $producto->Precios->TotalWeb;

        $ch = curl_init();
        $url ='http://api-jetes.azurewebsites.net/api/generar/guia';
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $result = json_decode(curl_exec($ch),true);

        /*curl_close($ch);
        echo "<pre>";
        print_r($result);
        echo "</pre>";*/


        return $result['guia'];

        } catch (SoapFault $s) {
            return null;
        }
    }


}