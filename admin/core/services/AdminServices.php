<?php
	// --------------------------------------
	// Servicio de Administradores
	// --------------------------------------


class AdminServices 
{

	// --------------------------------------
	// Scape Parametros
	// --------------------------------------
	function scape($var){
		if(is_array($var)){
			foreach($var as $name => $val){
				$var[$name] = @mysql_real_escape_string(strtolower($val)); 
			}
		}else{
			$var = @mysql_real_escape_string(strtolower($var));
		}
		return $var;
	}	

	// --------------------------------------
	// Listado de administradores
	// --------------------------------------
    public function ObtenerAdministradores(){
		
		
        try {
            $administradores = AdminData::ObtenerAdministradores();
            $posts = $administradores;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


	
	// --------------------------------------
	// Listado de nivel de acceso
	// --------------------------------------
    public function ObtenerMenus(){
        try {
            $accesos = AdminData::ObtenerMenus();
            $posts = $accesos;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


	// --------------------------------------
	// Agregar nuevo usuario admin
	// --------------------------------------
     public function CrearAdmin($param){
		 try{
			$nombre= ucfirst(strtolower($param['nombre']));
			$apellido= ucfirst(strtolower($param['apellido']));
			$correo = $param['correo'];
			$telefono = $param['telefono'];
			$acceso = $param['acceso'];
			$contrasena_temporal = $param['contrasena'];
			$numid = $param['cedula'];
			$tipoid = $param['tipo_cedula'];
			$result = AdminData::CrearAdmin($nombre,$apellido,$correo,$telefono,$acceso, $contrasena_temporal, $numid, $tipoid);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
				if($obj[0]->{'ID'} == 0){
						$post_msg = "Usuario ya existe";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj[0]->{'ID'}  > 0){
								$post_msg = "El administrador fue guardado satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}


	// --------------------------------------
	// Actualiza usuario admin
	// --------------------------------------
     public function UpdateAdmin($param){
		 try{
			$iduser = $param['iduser'];
			$acceso = $param['acceso'];
			
			$result = AdminData::UpdateAdmin($iduser, $acceso);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
				if($obj[0]->{'ID'} == 0){
						$post_msg = "El administrador ya posee este acceso";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj[0]->{'ID'}  > 0){
								$post_msg = "El administrador fue actualizado satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

	
	// --------------------------------------
	// Obtener usuario admin
	// --------------------------------------
     public function ObtenerAdmin($param){
		 try{
			$iduser= $param['iduser'];
			$acceso = $param['acceso_desc'];
            $administrador = AdminData::ObtenerAdmin($iduser, $acceso);
            $posts = $administrador;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}
	
	// --------------------------------------
	// Eliminar usuario admin
	// --------------------------------------
     public function EliminarAdministrador($param){
		 try{
			$iduser= $param['iduser'];
			$acceso = $param['acceso'];
            $delete = AdminData::EliminarAdministrador($iduser, $acceso);
            $posts = $delete;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

}
?>