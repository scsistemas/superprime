<?php
	// --------------------------------------
	// Servicio de Categoria
	// --------------------------------------


class CategoriaServices 
{

	// --------------------------------------
	// Scape Parametros
	// --------------------------------------
	function scape($var){
		if(is_array($var)){
			foreach($var as $name => $val){
				$var[$name] = @mysql_real_escape_string(strtolower($val)); 
			}
		}else{
			$var = @mysql_real_escape_string(strtolower($var));
		}
		return $var;
	}	

	// --------------------------------------
	// Crear Categoria
	// --------------------------------------
    public function create($param){
		 try{
			$nombre= ucfirst(strtolower($param['nombre']));
			$principal = $param['principal'];
			$destacado = $param['destacado'];
			$descuento = $param['descuento'];

			if (!$param['descuento']){
				$descuento = 0;
				$flat_descuento = 0;
			}else{
				$flat_descuento = 1;
			}

			$destacado_desde = $param['destacado_desde'];
			$destacado_hasta = $param['destacado_hasta'];

			
			$descuento_desde = $param['descuento_desde'];
			$descuento_hasta = $param['descuento_hasta'];

			if (!$param['destacado_desde']){
				$destacado_desde = "1111-11-11";
				$destacado_hasta =  "1111-11-11";
			}

			if (!$param['descuento_desde']){
				$descuento_desde = "1111-11-11";
				$descuento_hasta =  "1111-11-11";
			}
			$result = CategoriaData::create($nombre,$principal,$destacado,$flat_descuento);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
				if($obj->{'ID'} == 0){
						$post_msg = "Categoria ya existe";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj->{'ID'}  > 0){
								$id = CategoriaData::ObtenerCategoriaID($nombre);
								$estatus = json_encode($id);
								$obj_id = json_decode($estatus);
								$categorias_id = $obj_id->{'id'};
								if ($descuento > 0 ){
									$detalle_descuento = CategoriaData::InsertarDescuento($categorias_id, $descuento, $descuento_desde, $descuento_hasta);
								}
								if ($destacado == 1 ){
									$detalle_destacado = CategoriaData::InsertarDestacado($categorias_id, $destacado_desde, $destacado_hasta);
								}
								$post_msg = "La categoria guardada satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}


	// --------------------------------------
	// Crear Subcategoria
	// --------------------------------------
	public function subcategoriacreate($param){

		 try{
			$nombre= ucfirst(strtolower($param['nombre']));
			$principal = $param['principal'];
			$destacado = $param['destacado'];
			$descuento = $param['descuento'];
			$filtro = $param['filtro'];
			$parent = $param['parent'];
			if (!$param['descuento']){
				$descuento = 0;
				$flat_descuento = 0;
			}else{
				$flat_descuento = 1;
			}

			$destacado_desde = $param['destacado_desde'];
			$destacado_hasta = $param['destacado_hasta'];

			
			$descuento_desde = $param['descuento_desde'];
			$descuento_hasta = $param['descuento_hasta'];

			if (!$param['destacado_desde']){
				$destacado_desde = "1111-11-11";
				$destacado_hasta =  "1111-11-11";
			}

			if (!$param['descuento_desde']){
				$descuento_desde = "1111-11-11";
				$descuento_hasta =  "1111-11-11";
			}
			$result = CategoriaData::subcategoriacreate($nombre,$principal,$destacado,$flat_descuento);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
				if($obj->{'ID'} == 0){
						$post_msg = "Categoria ya existe";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj->{'ID'}  > 0){
								$id = CategoriaData::ObtenerSubCategoriaID();
								$estatus = json_encode($id);
								$obj_id = json_decode($estatus);
								$subcategoria_id = $obj_id->{'id'};
								if ($descuento > 0 ){
									$detalle_descuento = CategoriaData::InsertarSubCategoriaDescuento($subcategoria_id, $descuento, $descuento_desde, $descuento_hasta);
								}
								if ($destacado == 1 ){
									$detalle_destacado = CategoriaData::InsertarSubCategoriaDestacada($subcategoria_id, $destacado_desde, $destacado_hasta);
								}
								if (isset($filtro)){
									foreach ($filtro as $key => $value) {
										$filt = $filtro[$key];
										$filtro_SubCategoria = CategoriaData::InsertarFiltroSubCategoria($subcategoria_id, $filt);
									}
								}
								if (isset($parent)){
									foreach ($parent as $p => $v) {
									    $categoria_id = $parent[$p];
										$categorias_subcategorias = CategoriaData::InsertarCategorias_SubCategorias($subcategoria_id, $categoria_id);
									}
								}
								$post_msg = "La categoria guardada satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}


}
?>