<?php
	// --------------------------------------
	// Servicio de Filtro
	// --------------------------------------
class FiltroServices 
{
	// --------------------------------------
	// Listar Filtros
	// --------------------------------------
	public function ObtenerFiltros()
    {
        try {
            $filtros = FiltrosData::ObtenerFiltros();
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($filtros, JSON_UNESCAPED_UNICODE);
	}


	// --------------------------------------
	// Eliminar Filtro
	// --------------------------------------
	public function EliminarFiltro($id)
    {
        try {
            $id = $id['id'];
            $salida = FiltrosData::EliminarFiltro($id);
            $posts = array('success' => '1', 'msg' => "Filtro eliminado exitosamente");
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
	}


	// --------------------------------------
	// Insertar Filtro
	// --------------------------------------
    public function InsertarFiltro($param){
		 try{
			$nombre= ucfirst(strtolower($param['nombre_filtro']));
			$tipo_elemento = $param['tipo_elemento'];
			$opcion_1 = $param['opcion_1'];
			$opcion_2 = $param['opcion_2'];
			$opcion_3 = $param['opcion_3'];
			$result = FiltrosData::InsertarFiltro($nombre,$tipo_elemento,$opcion_1,$opcion_2,$opcion_3);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
				if($obj->{'ID'} == 0){
						$post_msg = "Filtro ya existe";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj->{'ID'}  > 0){
								$post_msg = "El filtro fue guardado satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}
}