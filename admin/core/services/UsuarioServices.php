<?php
	// --------------------------------------
	// Servicio de Usuario
	// --------------------------------------


class UsuarioServices 
{

	// --------------------------------------
	// Scape Parametros
	// --------------------------------------
	function scape($var){
		if(is_array($var)){
			foreach($var as $name => $val){
				$var[$name] = @mysql_real_escape_string(strtolower($val)); 
			}
		}else{
			$var = @mysql_real_escape_string(strtolower($var));
		}
		return $var;
	}	

	// --------------------------------------
	// Listado de usuarios
	// --------------------------------------
    public function ObtenerUsuarios(){
		
		
        try {
            $usuarios = UsuarioData::ObtenerUsuarios();
            $posts = $usuarios;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }	

	// --------------------------------------
	// Listado de Estados
	// --------------------------------------
    public function ObtenerEstados(){
		
		
        try {
            $estados = UsuarioData::ObtenerEstados();
            $posts = $estados;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


	// --------------------------------------
	// Actualiza usuario 
	// --------------------------------------
     public function ActualizarUsuario($param){
		 try{
			$idUser = $param['idUser'];
			$nombre = $param['nombre'];
			$apellido = $param['apellido'];
			$usuario = $param['usuario'];
			$estado = $param['estado'];
			
			$result = UsuarioData::ActualizarUsuario($idUser, $nombre, $apellido, $usuario, $estado);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
				if($obj->{'ID'} == 0){
						$post_msg = "Este email ya se encuentra registrado para otro usuario";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj->{'ID'}  > 0){
								$post_msg = "El usuario fue actualizado satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

	
	// --------------------------------------
	// Obtener usuario 
	// --------------------------------------
     public function ObtenerUsuarioByEmail($param){
		 try{
			$iduser= $param['usuario'];
            $usuario = UsuarioData::ObtenerUsuarioByEmail($iduser);
            $posts = $usuario;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}
	
	// --------------------------------------
	// Eliminar usuario 
	// --------------------------------------
     public function EliminarUsuario($param){
		 try{
			$usuario= $param['usuario'];
            $delete = UsuarioData::EliminarUsuario($usuario);
            $posts = $delete;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

	// --------------------------------------
	// Bloquear usuario 
	// --------------------------------------
     public function BloquearUsuario($param){
		 try{
			$usuario= $param['usuario'];
            $delete = UsuarioData::BloquearUsuario($usuario);
            $posts = $delete;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

	// --------------------------------------
	// Desbloquear usuario 
	// --------------------------------------
     public function DesbloquearUsuario($param){
		 try{
			$usuario= $param['usuario'];
            $delete = UsuarioData::DesbloquearUsuario($usuario);
            $posts = $delete;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

}
?>