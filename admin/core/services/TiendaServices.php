<?php
	// --------------------------------------
	// Servicio de Tienda
	// --------------------------------------


class TiendaServices 
{

	// --------------------------------------
	// Scape Parametros
	// --------------------------------------
	function scape($var){
		if(is_array($var)){
			foreach($var as $name => $val){
				$var[$name] = @mysql_real_escape_string(strtolower($val)); 
			}
		}else{
			$var = @mysql_real_escape_string(strtolower($var));
		}
		return $var;
	}	

	// --------------------------------------
	// Listado de Tiendas
	// --------------------------------------
    public function ObtenerRazonesSociales(){
		
		
        try {
            $Tiendas = TiendaData::ObtenerRazonesSociales();
            $posts = $Tiendas;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }	

	
	// --------------------------------------
	// Obtener Tienda By Estado
	// --------------------------------------
     public function ObtenerTiendasByEstado($param){
		 try{
			$estado_id= $param['estado_id'];
            $Tienda = TiendaData::ObtenerTiendasByEstado($estado_id);
            $posts = $Tienda;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}
	// --------------------------------------
	// Obtener Estados by Razon Social
	// --------------------------------------
     public function ObtenerCiudadesByRazonSocial($param){
		 try{
			$razonsocial_id= $param['razonsocial_id'];
            $Tienda = TiendaData::ObtenerCiudadesByRazonSocial($razonsocial_id);
            $posts = $Tienda;
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}


	// --------------------------------------
	// Actualiza Tienda 
	// --------------------------------------
     public function ActualizarTienda($param){
		 try{
			 $codTienda = $param['codTienda'];
			 $nombre =  $param['nombre'];
			 $estado = 	  $param['estado'];
			 $ciudad  =   $param['ciudad'];
             $municipio =    $param['municipio'];
             $parroquia =  $param['parroquia'];
             $postal =    $param['postal'];
             $website =    $param['website'];
             $direccion =    $param['direccion'];
             $email =   $param['email'];
             $encargado =  $param['encargado'];
             $telefono_1 =   $param['telefono_1'];
             $telefono_2 =  $param['telefono_2'];
			
			$result = TiendaData:: ActualizarTienda($codTienda, $nombre, $estado, $ciudad, $municipio, $parroquia, $postal, $website, $direccion, $email, $encargado, $telefono_1, $telefono_2);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
			    if($obj->{'ID'}  > 0){
							$post_msg = "La tienda fue actualizada satisfactoriamente";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}
	// --------------------------------------
	// Insertar Tienda 
	// --------------------------------------
     public function InsertarTienda($param){
		 try{
			 $codTienda = $param['codTienda'];
			 $nombre =  $param['nombre'];
			 $estado = 	  $param['estado'];
			 $ciudad  =   $param['ciudad'];
             $municipio =    $param['municipio'];
             $parroquia =  $param['parroquia'];
             $postal =    $param['postal'];
             $website =    $param['website'];
             $direccion =    $param['direccion'];
             $email =   $param['email'];
             $encargado =  $param['encargado'];
             $telefono_1 =   $param['telefono_1'];
             $telefono_2 =  $param['telefono_2'];
			
			$result = TiendaData:: InsertarTienda($codTienda, $nombre, $estado, $ciudad, $municipio, $parroquia, $postal, $website, $direccion, $email, $encargado, $telefono_1, $telefono_2);
			$estatus = json_encode($result);
			$obj = json_decode($estatus);
			    if($obj->{'ID'} == 0){
						$post_msg = "Este codigo de tienda ya esta registrado";
						$posts = array('success' => '0', 'msg' => utf8_encode($post_msg));
					}elseif($obj->{'ID'}  > 0){
								$post_msg = "La tienda fue insertada satisfactoriamente";
								$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
					}else{
							$post_msg = "Error al guardar los datos por favor verifique";
							$posts = array('success' => '1', 'msg' => utf8_encode($post_msg));
				}
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }

        echo json_encode($posts, JSON_UNESCAPED_UNICODE);

	}

}
?>