 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">

	    		<div class="col-lg-12" id="bankPaiment">

	    			<h1>Realizar Pago | Orden número: <?php echo $_GET['cod_pedido'];?></h1>

					<div class="container-fluid">

						<h6><span class="icon-credit-card"></span> Pago con Tarjeta de Crédito</h6>

						<!-- Detalle de productos -->
	    				<div class="panel panel-default">
						  <div class="panel-body">
						  	<span class="total">
								Total Bs:  <span class="monto"  id="preciototalL"   data-value="<?php echo $resumen['Total']; ?>"></span>
							</span>
						    <button class="btn btn-info btn-sm pull-right" type="button" data-remodal-target="resume">
							  <span class="icon-cart"></span> Ver detalles
							</button>
						  </div>
						</div>
						<!-- /Detalle de productos -->

	    				<div class="col-lg-6 col-md-offset-3">
			    			<fieldset>
					     	  <form data-toggle="validator" id="fpago" role="form">
								  <input type="hidden" id="id_user" name="id_user" value="<?php echo $_GET['id_user'];?>">
								  <input type="hidden" id="cod_pedido" name="cod_pedido" value="<?php echo  $_GET['cod_pedido'];?>">
								  <input type="hidden" id="total" name="total" value="<?php echo   $resumen['Total'];?>">
								  <div class="row">
									  <div class="col-lg-6">
										  <div class="form-group">
											  <label for="cirif" class="cols-sm-2 control-label">C.I / RIF</label>
											  <div class="row">
												  <div class="col-lg-4">
													  <select class="form-control" name="tipoid" id="tipoid" required>
														  <option value=""></option>
														  <option value="V">V</option>
														  <option value="E">E</option>
														  <option value="J">J</option>
													  </select>
												  </div>
												  <div class="col-lg-8">
													  <input id="numid"  name="numid" type="text" pattern="[0-9]{8,9}" class="form-control" required>
												  </div>
											  </div>

										  </div>
									  </div>
								  </div>
					          <!-- Name -->
					          <div class="form-group">
					            <label class="control-label">Nombre en la tarjeta</label>
					            <div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
					              <input type="text" id="nomtarjeta" name="nomtarjeta" placeholder="" class="form-control" required>
					            </div>
					          </div>
					     
					          <!-- Card Number -->
					          <div class="form-group">
					            <label class="control-label">Número de la tarjeta</label>
					            <div class="input-group">
					              <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
					              <input type="text" id="numtarjeta" name="numtarjeta"  pattern="[0-9]{15,19}" minlength="15" maxlength="19" placeholder="" class="form-control numeric" required>
					            </div>
					          </div>

					     		
					     	  <div class="row">
					     	  	<div class="col-lg-6">
									<!-- Expiry-->
									<div class="form-group">
										<label class="control-label">Fecha de expiración</label>
										<input type="text" class="form-control" value="" id="fecha" name="fecha" required>
									</div>	
					     	  	</div>
					     	  	<div class="col-lg-6">
					     	  		<!-- CVV -->
									<div class="form-group">
										<label class="control-label" >Cod de seguridad</label>
										<div class="input-group">
										  <span class="input-group-addon"><span class="icon-credit-card"></span></span>
										  <input type="text"  placeholder=""  pattern="[0-9]{3,5}" minlength="3" maxlength="5"  id="codigo" name="codigo" class="form-control numeric" required>
										</div>
									</div>
					     	  	</div>
					     	  </div>	
					          
					          
					          <!-- Tipo de tarjeta -->
					          <div class="form-group">
					            <label class="control-label" >Tipo de Tarjeta</label>
					            <select class="form-control" id="tipot" name="tipot" required>
									<option></option>
									<option value="2"><i class="fa fa-credit-card"></i> Master Card</option>
									<option value="1">Visa</option>
								</select>
					          </div>
					    
					     
					          <!-- Submit -->
					          <div class="control-group">
					            <div class="controls">
					              <button type="submit" class="btn btn-success">Pocesar pago</button>
					              <button class="btn btn-warning">Cancelar</button>
					            </div>
					          </div>

		   		              <p class="text-danger">Por su seguridad, SuperPrime no almacenará los datos de su tarjeta de crédito</p>


					          </form>
					     
					        </fieldset>
		    			</div>

					</div>


	    		 	<!-- Remodal -->
	    		 	<div class="remodal remodal-sm" data-remodal-id="resume">
			          <button data-remodal-action="close" class="remodal-close"></button>
			          <h1>Resumen</h1>
			          <div class="resumen">
				            <span class="help-block">A continuación se muestra el detalle del pago a realizar</span>

						  <table class="table">
							  <thead>
							  <tr>
								  <th>Producto</th>
								  <th>Color</th>
								  <th class="text-center">Talla</th>
								  <th class="text-center">Cantidad</th>
								  <th class="text-center">Costo</th>
							  </tr>
							  </thead>
							  <tbody>
							  <?php foreach ($resumen['Productos'] as $producto) {?>
							  <tr>
								  <td>
									  <?php
									  $nombre_imagen="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][0]['color']."/".$producto['ImagenesSelec'][0]['nombre'];
									  if (!file_exists($nombre_imagen))
										  $nombre_imagen="../images/noImageThumb.jpg";?>
									  <img src="<?php echo $nombre_imagen;?>"  width="100">
								  </td>
								  <td  class="options">
									  <ul class="colorOptions">
										  <li style="opacity: 1;border: 2px solid #a1a1a1;  background:<?=$producto['ColorPrinc'].';'?> <?php echo $producto['ColorSec']!=''?'filter: alpha(opacity=75); opacity: 1; border-bottom: 10px '.$producto['ColorSec'].' solid;':''; ?>  " title="<?=$producto['DescWeb']?>"></li>

									  </ul>
								  </td>
								  <td  class="options" align="center">
									  <?php echo $producto['DescTalla']; ?>
								  </td>
								  <td  class="text-center"><?php echo $producto['Cantidad'];?></td>
								  <td  class="text-center">Bs <span class="precio" id="preciounitario-<?php echo $producto['CodProducto']; ?>"  data-CodProducto="<?php echo $producto['CodProducto']; ?>" data-value="<?php echo $producto['PrecioUnitario']; ?>"></span></td>
							  </tr>
							  <tr>
								  <td colspan="3" align="center">
									  <h7><?php echo $producto['Nombre'];?></h7>
								  </td>
							  </tr>
							  <?php } ?>
							  </tbody>
						  </table>
						  <table class="resume table-striped table">
							  <tbody>
							  <tr>
								  <td>Precio total Bsf</td>
								  <td>Bs. <span class="monto"  id="preciototal"   data-value="<?php echo $resumen['Precio']; ?>"></span>
								  </td>
							  </tr>
							  <tr class="iva">
								  <td>IVA (12%)</td>
								  <td>Bs. <span  class="monto" id="iva"   data-value="<?php echo $resumen['Iva']; ?>"></span></td>
							  </tr>
							  <tr>
								  <td>SubTotal</td>
								  <td>Bs. <span  class="monto"  id="subtotal"   data-value="<?php echo $resumen['SubTotal']; ?>"></span>
								  </td>
							  </tr>
							  <tr>
								  <td>Costo de envío Bsf</td>
								  <td>Bs.  <span  class="monto" id="envio"   data-value="<?php echo $resumen['CostoEnvio']; ?>"></span></td>
							  </tr>
							  <tr class="total">
								  <td>Total a pagar</td>
								  <td>Bs.  <span  class="monto" id="totaltotal"   data-value="<?php echo $resumen['Total']; ?>"></span>
								  </td>
							  </tr>
							  </tbody>
						  </table>
			    		</div>
			          <br>
			          <button data-remodal-action="confirm" class="btn btn-success">Cerrar</button>
			        </div>
			        <!-- /Remodal -->


	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>
 <script src="../js/views/backend-PagoTDC.js"></script>
