 <?php include '../includes/header.php';?>

    <div class="backend-user">
	    <div class="container ">
		    	<div class="row">

    	    		<h1>Carrito de Compras - Pago con Tarjeta de Crédito</h1>

		    		<div class="col-lg-3">
		    			<p class="alert alert-info">Cambiar por un mensaje proporcionando ayuda sobre el proceso de pago.</p> 
		    		</div>
		    		
		    		<div class="col-lg-4">
		    			<fieldset>
				          <div id="legend">
				            <legend class="">Pago con Tarjeta de Crédito</legend>
				          </div>
				     		
				     	  <form>
				          <!-- Name -->
				          <div class="form-group">
				            <label class="control-label">Nombre en la tarjeta</label>
				            <div class="input-group">
							  <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
				              <input type="text" id="username" name="username" placeholder="" class="form-control">
				            </div>
				          </div>
				     
				          <!-- Card Number -->
				          <div class="form-group">
				            <label class="control-label">Número de la tarjeta</label>
				            <div class="input-group">
				              <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
				              <input type="text" id="email" name="email" placeholder="" class="form-control numeric">
				            </div>
				          </div>

				     		
				     	  <div class="row">
				     	  	<div class="col-lg-6">
								<!-- Expiry-->
								<div class="form-group">
									<label class="control-label">Fecha de expiración</label>
									<input type="text" class="form-control" value="" id="datepicker">
								</div>	
				     	  	</div>
				     	  	<div class="col-lg-6">
				     	  		<!-- CVV -->
								<div class="form-group">
									<label class="control-label" >Código de seguridad</label>
									<div class="input-group">
									  <span class="input-group-addon"><span class="icon-credit-card"></span></span>
									  <input type="text"  placeholder="" class="form-control numeric">
									</div>
								</div>
				     	  	</div>
				     	  </div>	
				          
				          
				          <!-- Tipo de tarjeta -->
				          <div class="form-group">
				            <label class="control-label" >Tipo de Tarjeta</label>
				            <select class="form-control" name="expiry_year">
								<option></option>
								<option value="13"><i class="fa fa-credit-card"></i> Master Card</option>
								<option value="14">Visa</option>
							</select>
				          </div>
				    
				     
				          <!-- Submit -->
				          <div class="control-group">
				            <div class="controls">
				              <button class="btn btn-success">Pocesar pago</button>
				              <button class="btn btn-warning">Cancelar</button>
				            </div>
				          </div>

	   		              <p class="text-danger">Por su seguridad, SuperPrime no almacenará los datos de su tarjeta de crédito</p>


				          </form>
				     
				        </fieldset>
		    		</div>

		    		<div class="col-lg-4 col-md-offset-1 resumen">
			            <legend class="">Resumen</legend>
			            <span class="help-block">A continuación se muestra en detalle del pago a realizar</span>

			            <div class="content">
				            <div class="item"><strong>Zapatos Nike 23343</strong><span class="pull-right">Bs 965.32</span></div>
				            <div class="item"><strong>Zapatos Kriza night</strong><span class="pull-right">Bs 1.265.32</span></div>
				            <div class="item"><strong>Cartera fem 089</strong><span class="pull-right">Bs 1.665.32</span></div>
				            <div class="item"><strong>Juguete para niño</strong><span class="pull-right">Bs 265.32</span></div>	
			            </div>
			            

			            <h2>Sub-Total<span class="pull-right">Bs 4161,28</span></h2>
						<div></div>

						<h2 class="">I.V.A<span class="pull-right">Bs 499,35</span></h2>
						<div></div>

						<h2 class="">Total<span class="pull-right total">Bs 4660.63</span></h2>
						<div></div>
		    		</div>
		    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>