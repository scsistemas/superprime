<?php include '../includes/header.php';?>
<?php //include '../includes/globalMenu.php';?>
<div class="container">
    <h1><?php echo $nombrecategoria;?></h1>

    <div class="minijumbotron">
        <div class="row-fluid">
            <form>
                <input type="hidden" id="oferta" name="oferta"/>
                <input type="hidden" id="prime" name="prime"/>
                <input type="hidden" id="categoria" value="<?php echo $categoria; ?>" />
                <input type="hidden" id="nombrecateg" value="<?php echo $nombrecategoria;?>" />
                <input type="hidden" id="listaminimo" value="<?php echo $listaProductos['Minimo']; ?>" />
                <input type="hidden" id="listamaximo" value="<?php echo $listaProductos['Maximo']; ?>" />
                <input type="hidden" id="minimo" value="<?php echo $minimo; ?>" />
                <input type="hidden" id="maximo" value="<?php echo $maximo; ?>" />
                <input type="hidden" id="desde" value="<?php echo $desde; ?>" />
                <input type="hidden" id="hasta" value="<?php echo $hasta; ?>" />
                <input type="hidden" id="total" value="<?php echo $listaProductos['Total']/$hasta; ?>"/>
                <input type="hidden" id="id_user" value="<?php echo $_SESSION['userid']; ?>"/>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="categoria">Categoría</label>
                        <select id="s-subcategoria" class="form-control">
                            <option value=""></option>
                            <?php
                            foreach ($Subcategorias['SubCategorias'] as $subcategoria){
                                ?>
                                <option value="<?php echo $subcategoria['Id'];?>" <?php echo $subcategoria['Id']==$subCategoria?'selected':'';?>><?php echo $subcategoria['Nombre'];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="s-precios">Precio</label>
                        <input type="text" id="s-precios" name="s-precios"/>
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="s-marca">Marcas</label>
                        <select id="s-marca" name="s-marca" class="form-control">
                            <option value=""></option>
                            <?php
                            foreach ($marcas as $marca){
                                ?>
                                <option value="<?php echo $marca['CodMarca'];?>" <?php echo $marca['CodMarca']==$smarca?'selected':'';?>><?php echo $marca['DescMarca'];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="padding-top:15px">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" id="chk-oferta"  <?php echo $oferta=='S'?'checked':'';?> name="chk-oferta"> Ofertas web
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="flat" id="chk-prime"  <?php echo $prime=='S'?'checked':'';?> name="chk-prime"> Prime
                        </label>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="nombre">Buscar por:</label>
                        <input type="text" id="nombre" name="nombre" class="form-control" value="<?php echo $nombre;?>" placeholder="Buscar Por">

                    </div>

                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="margin-top: 23px">
                    <button id="buscar" type="button" class="btn btn-success">Buscar</button>
                </div>
                <div class="clearfix"></div>

            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="form-inline">
                <div class="form-group">
                    <label>
                        Mostrar por página
                    </label>
                    <select id="s-cantidad" class="form-control">
                        <option value="10" <?php echo $hasta=='10'?'selected':''; ?>>10</option>
                        <option value="20" <?php echo $hasta=='20'?'selected':''; ?>>20</option>
                        <option value="30" <?php echo $hasta=='30'?'selected':''; ?>>30</option>
                    </select>
                </div>
            </div>

        </div>
    </div>


    <div id="productsList">

        <div class="row" id="listado">

            <div class="mg-space-init">
                <div class="mg-rows">

                    <?php foreach ($listaProductos['productos'] as $producto) {
                        $nombre_imagen="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][0]['color']."/".$producto['ImagenesSelec'][0]['nombre'];
                        if (!file_exists($nombre_imagen)){
                            $nombre_imagen="../images/noImageThumb.jpg";
                        }
                        ?>
                        <div class="thumbnail mg-trigger  <?php echo $producto['Prime']=='S'?'prime':''; ?> ">

                            <a href="" class="trigger lprime" data-largesrc="<?php echo $nombre_imagen;?>" data-title="Articulo uno" data-description="Swiss"></a>
                            <a href="" class="trigger" data-largesrc="../../backend/images/fotos/<?php echo $producto['CodProducto']; ?>/<?php echo $producto['ImagenesSelec'][0]['color']; ?>/<?php echo $producto['ImagenesSelec'][0]['nombre']; ?>" title="<?php echo $producto['Nombre']; ?>" data-title="Articulo uno" data-description="Swiss">
                                <img src="<?php echo $nombre_imagen;?>"  alt="..." width="190">
                                <h6><?php echo substr($producto['Nombre'],0,25).'...'; ?></h6>
                                <div class="caption">
                                    <p>Bs. <?php echo $producto['Precio']; ?></p>
                                    <a href="../pages/product-detail.php?codPro=<?php echo $producto['CodProducto']; ?>"  class="btn btn-default btn-xs verDetalle"><span class="icon-ver"></span> Ver detalle</a>
                                </div>
                                </a>

                        </div>
                    <?php }?>
                </div>


                <!-- contenedores de detalles -->

                <div class="mg-targets row">
                    <?php foreach ($listaProductos['productos'] as $producto) { ?>
                        <div class="item">
                            <div class="row">
                                <div class="col-lg-6 left" id="div-carousel-<?php echo $producto['CodProducto']; ?>">
                                    <div id="carousel-example-generic-<?php echo $producto['CodProducto']; ?>" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <?php
                                            for($i = 0; $i < count($producto['ImagenesSelec']); ++$i) {
                                                $clase=$i=='0'? 'active':'';?>
                                                <li data-target="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>"  data-slide-to="<?=$i?>" class="<?php echo $clase; ?>"></li>
                                            <?php }?>
                                             <li data-target="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>"  data-slide-to="<?php echo count($producto['ImagenesSelec'])+1;?>"></li>
                                        </ol>
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <?php
                                            for($j = 0; $j < count($producto['ImagenesSelec']); ++$j) {
                                                $nombre_imagenc="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][$j]['color']."/".$producto['ImagenesSelec'][$j]['nombre'];
                                                if (!file_exists($nombre_imagenc))
                                                    $nombre_imagenc="../images/noImageThumb.jpg";?>
                                                <div class="item <?php echo $j=='0'?'active':'';?>">
                                                    <img src="<?php echo $nombre_imagenc;?>" class="img-responsive">
                                                </div>
                                            <?php }?>
                                            <div class="item">
                                                <div align="center">
                                                    <video controls class="video-responsive">
                                                        <source src="../../backend/images/prueba.mp4"  type="video/mp4">
                                                    </video>
                                                </div>
                                            </div>

                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6 right">
                                    <h1><?php echo $producto['Nombre']; ?></h1>
                                    <p><?php echo $producto['Modelo']; ?></p>
                                    <h2>Bs. <span id="id-precio-<?php echo $producto['CodProducto']; ?>"><?php echo $producto['Precio']; ?></span></h2>
                                    <div class="line"></div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <label for=s-talla-<?php echo $producto['CodProducto']; ?>">Talla</label>
                                                <select data-CodProducto="<?php echo $producto['CodProducto']; ?>"   name="s-talla-<?php echo $producto['CodProducto']; ?>" id="s-talla-<?php echo $producto['CodProducto']; ?>" class="form-control select">
                                                    <?php foreach ($producto['Talla'] as $talla ) { ?>
                                                        <option  value="<?=$talla['CodTalla']?>"><?=$talla['DescTalla']?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <input type="hidden" id="id-cantidad-<?php echo $producto['CodProducto']; ?>" name="id-cantidad-<?php echo $producto['CodProducto']; ?>" value="">
                                                <label for="cantidad">Cantidad</label>
                                                <input type="number" class="form-control" id="cantidad-<?php echo $producto['CodProducto'];?>" placeholder="-">
                                                <input type="hidden" disabled class="form-control" id="disponible-<?php echo $producto['CodProducto'];?>" value="<?php echo $producto['Cantidad'];?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <input type="hidden" id="colorselec-<?php echo $producto['CodProducto'];?>" name="colorselec-<?php echo $producto['CodProducto'];?> " value="<?php echo $producto['ColorDisp'][0]['CodColor']; ?>">
                                                <label for="cantidad">Color</label>
                                                <ul class="colorOptions">
                                                    <?php foreach ($producto['ColorDisp'] as $coloresdisp ) { ?>
                                                        <li style=" background:<?=$coloresdisp['ColorPrinc'].';'?> <?php echo $coloresdisp['ColorSec']!=''?'filter: alpha(opacity=75); opacity: 1; border-bottom: 10px '.$coloresdisp['ColorSec'].' solid;':''; ?>  " title="<?=$coloresdisp['DescWeb']?>" data-CodProducto="<?php echo $producto['CodProducto']; ?>" data-CodColor="<?=$coloresdisp['CodColor']?>" class="<?=$coloresdisp['DescColor']?>"></li>
                                                    <?php }?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="button"  data-CodProducto="<?php echo $producto['CodProducto']; ?>" class="btn btn-default buyNow"><span class="icon-cart"></span> Comprar Ahora</button>
                                        <button type="button"  data-CodProducto="<?php echo $producto['CodProducto']; ?>" class="btn btn-default addCart"><span class="icon-cart"></span> Añadir al Carrito</button>
                                    </div>

                                    <div class="line"></div>
                                    <div class="lastBox">
                                        <div class="pretty plain smooth toggle">
                                            <input data-CodProducto="<?php echo $producto['CodProducto']; ?>" <?php echo $producto['Favorito']>0?'checked':''; ?>  type="checkbox"  class="favorito">
                                            <label><i class="fa fa-heart-o"></i></label>
                                            <label><i class="fa fa-heart danger"></i></label>
                                            Añadir a mi lista de deseos
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>

        </div>
        <div class="pagination col-lg-12 text-center">

        </div>
    </div>
</div>

<div class="aliados">
    <div class="container">
        <h1>Aliados</h1>
        <img src="../images/rs21.jpg"><img src="../images/kriza.jpg"><img src="../images/volpe.jpg"><img src="../images/vita.jpg"><img src="../images/prime.jpg">
    </div>
</div>

<?php include '../includes/footer.php';?>
<script src="../js/views/product-list.js"></script>