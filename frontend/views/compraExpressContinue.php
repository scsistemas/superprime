<?php include '../includes/header.php';?>

<div class="backend-user">
    <div class="container">
        <form  data-toggle="validator" role="form">
         <input type="hidden" id="cod_pedido" name="cod_pedido" value="<?php echo $_GET['cod_pedido'];?>">
         <input type="hidden" id="id_user" name="id_user" value="<?php echo $_GET['id_user'];?>">
        <div class="row contentArea" id="cart">


            <div class="col-lg-3">
                <p class="alert alert-info">Cambiar por un mensaje proporcionando ayuda sobre el proceso de pago.</p>
            </div>
            <div class="col-lg-9">

                <h1>Compra - Forma de Pago</h1>

                <div class="container-fluid">
                    <div class="row">



                            <div class="col-lg-6 left">
                                <h4>Resumen de la Compra</h4>

                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Producto</th>
                                        <th>Color</th>
                                        <th class="text-center">Talla</th>
                                        <th class="text-center">Cantidad</th>
                                        <th class="text-center">Costo</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($resumen['Productos'] as $producto) {?>
                                    <tr>
                                        <td>
                                            <?php
                                            $nombre_imagen="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][0]['color']."/".$producto['ImagenesSelec'][0]['nombre'];
                                            if (!file_exists($nombre_imagen))
                                                $nombre_imagen="../images/noImageThumb.jpg";?>
                                            <img src="<?php echo $nombre_imagen;?>"  width="100">
                                        </td>
                                        <td  class="options">
                                            <ul class="colorOptions">
                                                <li style="opacity: 1;border: 2px solid #a1a1a1;  background:<?=$producto['ColorPrinc'].';'?> <?php echo $producto['ColorSec']!=''?'filter: alpha(opacity=75); opacity: 1; border-bottom: 10px '.$producto['ColorSec'].' solid;':''; ?>  " title="<?=$producto['DescWeb']?>"></li>

                                            </ul>
                                        </td>
                                        <td  class="options" align="center">
                                            <?php echo $producto['DescTalla']; ?>
                                        </td>
                                        <td   class="options"><?php echo $producto['Cantidad'];?></td>
                                        <td   class="options">Bs <span class="precio" id="preciounitario-<?php echo $producto['CodProducto']; ?>"  data-CodProducto="<?php echo $producto['CodProducto']; ?>" data-value="<?php echo $producto['PrecioUnitario']; ?>"></span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="center">
                                            <h7><?php echo $producto['Nombre'];?></h7>
                                        </td>
                                    </tr>
                                    <?php }?>
                                    </tbody>
                                </table>
                                <table class="resume table-striped table">
                                    <tbody>
                                    <tr>
                                        <td>Precio total Bsf</td>
                                        <td>Bs. <span class="monto"  id="preciototal"   data-value="<?php echo $resumen['Precio']; ?>"></span>
                                        </td>
                                    </tr>
                                    <tr class="iva">
                                        <td>IVA (12%)</td>
                                        <td>Bs. <span  class="monto" id="iva"   data-value="<?php echo $resumen['Iva']; ?>"></span></td>
                                    </tr>
                                    <tr>
                                        <td>SubTotal</td>
                                        <td>Bs. <span  class="monto"  id="subtotal"   data-value="<?php echo $resumen['SubTotal']; ?>"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Costo de envío Bsf</td>
                                        <td>Bs.  <span  class="monto" id="envio"   data-value="<?php echo $resumen['CostoEnvio']; ?>"></span></td>
                                    </tr>
                                    <tr class="total">
                                        <td>Total a pagar</td>
                                        <td>Bs.  <span  class="monto" id="totaltotal"   data-value="<?php echo $resumen['Total']; ?>"></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6 right form-group">
                                <h4>Seleccionar la forma de pago</h4>

                                <div class="radio">
                                    <label>
                                        <input type="radio" name="paidMethod" class="flat form-control" id="optionsRadios1" value="tdc" checked required>
                                        <span class="control-label">Tarjeta de Crédito</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="paidMethod" class="flat  form-control" id="optionsRadios2" value="bank" required>
                                        <span class="control-label">Deposito y/o transferencia</span>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="paidMethod" class="flat  form-control" id="optionsRadios3" value="deb" required>
                                        <span class="control-label">Tarjeta de débito</span>
                                    </label>
                                </div>

                    </div>
                </div>

                <div class="row retirement">
                    <div class="col-lg-6 ">
                        <div class="radio form-group">
                            <label>
                                <input type="radio" name="retirement" class="flat  form-control"  value="direction" required>
                                <span class="control-label">Enviar a una dirección registrada</span>
                            </label>
                        </div>
                        <div id="direction" class="message" style='display:none'>
                            <div class="radio form-group">
                            <?php
                            foreach ($resumen['Direcciones'] as $direccion) {;?>
                                <span class="control-label"><input type="radio" name="direccion" id="dir-<?php echo $direccion['Id'];?>" class="direccion flat  form-control"  value="<?php echo $direccion['Id'];?>"></span>
                                <?php

                                echo  '<b>'.ucfirst(strtolower($direccion['Alias'])).':</b> '.$direccion['CalleUno'].' '.$direccion['CalleDos'].'  '.$direccion['DesCiudad'].'  '.$direccion['DesEstado'].'  '.$direccion['DesMunicipio'].'.<br>';


                            }?>



                        </div>
                        </div>

                    </div>
                    <div class="col-lg-6  form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="retirement" class="flat  form-control"  value="personal" required>
                                <span class="control-label"> Ahorrate el costo de envío y retiralo en una tienda</span>
                            </label>
                        </div>

                        <div id="personal"  class="message" style='display:none'>
                            <div class="msg1">
                                Tu pedido está en <strong>Prime Shoes Sambil Caracas</strong><br>
                                Av Libertador, C.C Sambil Caracas Nivel Feria Local Fc-18 Chacao<br>
                                Telf: 0212 3659565 | 0212 9056584
                            </div>
                            <div class="msg2">
                                <i class="fa fa-exclamation-triangle pull-left" aria-hidden="true"></i> Los productos están en diferentes tiendas ubicados en varias ciudades
                            </div>

                        </div>

                    </div>
                </div>

                <div class="text-center">
                    <br>
                    <button type="submit" href="" class="btn btn-success">Aceptar</button>
                </div>
            </div>


        </div>

    </div>
     </form>
</div>
</div>

<?php include '../includes/footer.php';?>
<script src="../js/views/compraExpressContinue.js"></script>




