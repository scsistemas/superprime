<?php include '../includes/header.php';?>

  <div class="register">
  


    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Registro de Usuario</h3>

          <form id="register" method="post" action="../pages/register.php" role="form" data-toggle="validator">

            <div class="form-group">
              <label for="email" class="cols-sm-2 control-label">E-mail</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                  <input type="email" pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$" class="form-control" name="email" id="email"  placeholder="introduce tu email" required/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Contraseña</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-minlength="8" class="form-control" name="password" id="inputPassword"  placeholder="Elije una contraseña" required/>
                  
                </div>
                <!--<span class="help-block">Debe tener una extensión de  ^[A-Za-z]\w{7,14}$"
al menos ocho caracteres, letras y números, al menos una letra en mayúscula</span>-->
              </div>


            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Confirmar Contraseña</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" class="form-control" name="password" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Las contraseñas no coindicen"  placeholder="Repite la contraseña" required/>
                </div>
                <div class="help-block with-errors"></div>
              </div>
            </div>

            <div class="form-group ">
              <!--<button type="submit" class="btn btn-primary btn-lg btn-block login-button" data-remodal-target="reporte" >Registrar</button>-->
              <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Registrar</button>
            </div>

            </form>

            
             <div id="pswd_info">
                <h4>La contraseña debe cumplir los siguientes requisitos:</h4>
                <ul>
                  <li id="letter" class="invalid">Mínimo <strong>una letra</strong></li>
                  <li id="capital" class="invalid">Mínimo <strong>una mayúscula</strong></li>
                  <li id="number" class="invalid">Mínimo <strong>un número</strong></li>
                  <li id="length" class="invalid">Mínimo <strong>8 caracteres</strong></li>
                </ul>
              </div>
        </div>

        <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p id='msj'>Para resguardar su información, le enviamos un correo electrónico con un enlace para continuar con el proceso de registro.</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>

    </div>
    </div>
   <?php include '../includes/footer.php';?>

<script type="text/javascript"> 
    $( document ).ready(function() {

       
        var val = <?php echo $user['success']?>;
        //var msj = <?php echo $user['msg']?>;

        console.log(val);
        $('#msj').html('');

        if (val == 1){
          $('#msj').html('Se he enviado un correo de confimación...');
          $('[data-remodal-id=reporte]').remodal().open();
        }

        if (val == 0){
          $('#msj').html('Ya existe un usuario registrado con este correo');
          $('[data-remodal-id=reporte]').remodal().open();
        }
        
    });
</script>
