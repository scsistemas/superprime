<?php include '../includes/header.php';?>
<?php /*include '../includes/globalMenu.php';*/?>

    <div class="container" id="productsList">
		<input type="hidden" id="id_user" value="<?php echo $_SESSION['userid']; ?>"/>
    	<div class="mg-targets product-detail">
    		<div class="item">
	          <div class="row">
	              <div class="col-sm-6 col-md-6 col-lg-6 left" id="div-carousel-<?php echo $producto['CodProducto']; ?>">

	              	<div id="carousel-example-generic-<?php echo $producto['CodProducto']; ?>" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <?php
                            for($i = 0; $i < count($producto['ImagenesSelec']); ++$i) {
                                $clase=$i=='0'? 'active':'';?>
                                <li data-target="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>"  data-slide-to="<?=$i?>" class="<?php echo $clase; ?>"></li>
                            <?php }?>
                               <li data-target="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>"  data-slide-to="<?php echo count($producto['ImagenesSelec'])+1;?>"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                            for($j = 0; $j < count($producto['ImagenesSelec']); ++$j) {
                                $nombre_imagenc="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][$j]['color']."/".$producto['ImagenesSelec'][$j]['nombre'];
                                if (!file_exists($nombre_imagenc))
                                    $nombre_imagenc="../images/noImageThumb.jpg";?>
                                <div class="item <?php echo $j=='0'?'active':'';?>">
                                    <img src="<?php echo $nombre_imagenc;?>" class="img-responsive">
                                </div>
                            <?php }?>

                            <div class="item">
                                <div align="center">
                                    <video controls autoplay loop class="video-responsive">
                                        <source src="../../backend/images/prueba.mp4"  type="video/mp4">
                                    </video>
                                </div>
                            </div>

                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic-<?php echo $producto['CodProducto']; ?>" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
					</div>
	              </div>
	              <div class="col-sm-6 col-md-6 col-lg-6 right">
	              	<h1><?php  echo $producto['Nombre']; ?></h1>
	              	<p><?php echo $producto['Modelo']; ?></p>
                      <h2>Bs. <span id="id-precio-<?php echo $producto['CodProducto']; ?>"><?php echo $producto['Precio']; ?></span></h2>

	              	<div class="line"></div>

	              	<div class="row">
		              		<div class="col-lg-2">
		              			<div class="form-group">
                                    <label for=s-talla-<?php echo $producto['CodProducto']; ?>">Talla</label>
                                    <select data-CodProducto="<?php echo $producto['CodProducto']; ?>"   name="s-talla-<?php echo $producto['CodProducto']; ?>" id="s-talla-<?php echo $producto['CodProducto']; ?>" class="form-control select">
                                        <?php foreach ($producto['Talla'] as $talla ) { ?>
                                            <option  value="<?=$talla['CodTalla']?>"><?=$talla['DescTalla']?></option>
                                        <?php }?>
                                    </select>
							</div>
		              		</div>
		              		<div class="col-lg-2">
                                <div class="form-group">
                                    <input type="hidden" id="id-cantidad-<?php echo $producto['CodProducto']; ?>" name="id-cantidad-<?php echo $producto['CodProducto']; ?>" value="">
                                    <label for="cantidad">Cantidad</label>
                                    <input type="number" class="form-control" id="cantidad-<?php echo $producto['CodProducto'];?>" placeholder="-">
                                    <input type="hidden" disabled class="form-control" id="disponible-<?php echo $producto['CodProducto'];?>" value="<?php echo $producto['Cantidad'];?>">
                                </div>
		              		</div>
		              		<div class="col-lg-8">
		              			<div class="form-group">
                                    <input type="hidden" id="colorselec-<?php echo $producto['CodProducto'];?>" name="colorselec-<?php echo $producto['CodProducto'];?> " value="<?php echo $producto['ColorDisp'][0]['CodColor']; ?>">
                                    <label for="cantidad">Color</label>
                                    <ul class="colorOptions">
                                        <?php foreach ($producto['ColorDisp'] as $coloresdisp ) { ?>
                                            <li style=" background:<?=$coloresdisp['ColorPrinc'].';'?> <?php echo $coloresdisp['ColorSec']!=''?'filter: alpha(opacity=75); opacity: 1; border-bottom: 10px '.$coloresdisp['ColorSec'].' solid;':''; ?>  " title="<?=$coloresdisp['DescWeb']?>" data-CodProducto="<?php echo $producto['CodProducto']; ?>" data-CodColor="<?=$coloresdisp['CodColor']?>" class="<?=$coloresdisp['DescColor']?>"></li>
                                        <?php }?>
                                    </ul>
							</div>
		              		</div>
		              	</div>

		              	<div class="text-center">
                            <?php if ($producto['Prime']=='S'){?>
                            <span class='label' id="iprime">Prime</span>
                            <?php }?>
				        	<button type="button" data-CodProducto="<?php echo $producto['CodProducto']; ?>" class="btn btn-default buyNow"><span class="icon-cart"></span> Comprar Ahora</button>
							<button type="button"  data-CodProducto="<?php echo $producto['CodProducto']; ?>" class="btn btn-default addCart"><span class="icon-cart"></span> Añadir al Carrito</button>
		              	</div>

		              	<br><br>
                      <div class="lastBox">
                          <div class="pretty plain smooth toggle">
                              <input data-CodProducto="<?php echo $producto['CodProducto']; ?>" <?php echo $producto['Favorito']>0?'checked':''; ?>  type="checkbox"  class="favorito">
                              <label><i class="fa fa-heart-o"></i></label>
                              <label><i class="fa fa-heart danger"></i></label>
                               Añadir a mi lista de deseos
                          </div>
                      </div>
		              </div>
	          </div>
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<h3>Detalles del producto</h3>

						<div class="col-sm-12 col-md-12 col-lg-12">
							<dl class="dl-horizontal">
							  <dt>Brief</dt>
							  <dd><?php echo $producto['Brief']; ?></dd>
							</dl>
						</div>
                        <h3>Caracteristicas del producto</h3>
						<div class="col-sm-6 col-md-6 col-lg-6">
							<dl class="dl-horizontal">
                              <dt>Dimensiones</dt>
                              <dd><?php echo $producto['Ancho']; ?> ancho x <?php echo $producto['Alto']; ?> alto</dd>
							  <dt>Peso</dt>
							  <dd><?php echo $producto['Peso']; ?></dd>
							</dl>
						</div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
							<dl class="dl-horizontal">
                                <dt>Profundidad</dt>
                                <dd><?php echo $producto['Profundidad']; ?></dd>
                                <?php foreach ($producto['Caracteristicas'] as $caracteristica ) { ?>
                                     <?php if ($caracteristica['MostrarWeb']=='S'){ ?>
                                    <dt><?php echo $caracteristica['DescCarac']; ?></dt>
                                <?php } }?>

							</dl>
						</div>
					</div>
				</div>
	      </div>
	      <!-- /item -->

	      <!-- Banner -->
	      <a href=""><img src="../images/banner.jpg" style="width:100%" class="img-responsive"></a>
	      <!-- /Banner -->

	      <!-- Relacionados -->
	      <div class="relacionados">
	      	<h3>Relacionados</h3>
	      	<ul id="light-slider">
                <?php
                 foreach ($top['productos'] as $productop) {
                    $nombre_imagentop="../../backend/images/fotos/".$productop['CodProducto']."/".$productop['ImagenesSelec'][0]['color']."/".$productop['ImagenesSelec'][0]['nombre'];
                    if (!file_exists($nombre_imagentop))
                        $nombre_imagentop="../images/noImageThumb.jpg";?>
                    <li>  <a href="../pages/product-detail.php?codPro=<?php echo $productop['CodProducto']; ?>"><img src="<?php echo $nombre_imagentop; ?>" class="img-responsive"><?php echo $productop['Precio']; ?></a></li>

                <?php }?>
			</ul>
	      </div>
	       <!-- /Relacionados -->

    	</div>

    </div>
    <!-- container -->

	<div class="aliados">
		<div class="container">
			<h1>Aliados</h1>
			<img src="../images/rs21.jpg"><img src="../images/kriza.jpg"><img src="../images/volpe.jpg"><img src="../images/vita.jpg"><img src="../images/prime.jpg">
		</div>
	</div>

<?php include '../includes/footer.php';?>
<script src="../js/views/product-detail.js"></script>

