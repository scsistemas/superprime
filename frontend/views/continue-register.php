<?php include '../includes/header.php';?>

<div class="register">



    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Registro de Usuario</h3>

          <form id="register" action="../pages/end-register.php" method="post" role="form" data-toggle="validator">

            <div class="form-group">
            <label for="email" class="cols-sm-2 control-label">Documento de Identidad</label>
            <div class="row">
              <div class="col-lg-3">
                <select class="form-control" name="tipoCedula" id="tipoCedula" required>
                  <option></option>
                  <option value="V">V</option>
                  <option value="E">E</option>
                  <option value="N">N</option>
                  <option value="J">J</option>
                  <option value="G">G</option>
                </select>
              </div>
              <div class="col-lg-9">
                <input type="text" class="form-control col-lg-3" pattern="[0-9]{1,10}" maxlength="10" name="cedula" id="cedula"  placeholder="documento de identidad" data-match-error="Introduzca su número de identificación" required/>
              </div>
            </div>
             <div class="help-block with-errors"></div>
          </div>

          <div class="form-group" id="identidad" style="visibility:hidden; height:0px">
              <div class="form-group">
                <label for="nombreEmpresa" class="cols-sm-2 control-label">Nombre de la Empresa</label>
                <input type="text" class="form-control" name="nombreEmpresa" id="nombreEmpresa"  placeholder="Nombre de la Empresa" required/>
                <div class="help-block with-errors"></div>
              </div>
          </div>

          <div class="form-group">
              <div id="labelnombre">
                <label for="nombre" class="cols-sm-2 control-label">Nombre</label>
              </div>
              <input type="text" class="form-control" pattern="[A-Za-z ]*" name="nombre" id="nombre"  placeholder="Nombre" data-match-error="Introduzca nombre" required/>
              <div class="help-block with-errors"></div>
          </div>


          <div class="form-group">
              <div id="labelapellido">
                <label for="nombre" class="cols-sm-2 control-label">Apellido</label>
              </div>
              <input type="text" class="form-control" pattern="[A-Za-z ]*" name="apellido" id="apellido"  placeholder="Apellido" data-match-error="Introduzca su apellido" required/>
              <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
              <div id="fechaN" name="fechaN">
                <label for="nombre" class="cols-sm-2 control-label">Fecha de Nacimiento <span>(Opcional)</span></label>
              </div>
              <div class="input-append date"  data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                <input class="form-control" id="dpYears" name="dpYears" size="16" type="text" placeholder="12-02-2012" data-match-error="Introduzca su fecha de nacimiento" readonly>
              </div>
              <div class="help-block with-errors"></div>
          </div>

          <div class="form-group">
              <label for="nombre" class="cols-sm-2 control-label">Sexo</label>
              <div class="row">
                <div class="col-lg-3">
                  <label>
                    <input type="radio" name="optionsRadios" id="hombre" value="M" required >
                    Hombre
                  </label>
                </div>
                <div class="col-lg-3">
                  <label>
                    <input type="radio" name="optionsRadios" id="mujer" value="F" >
                    Mujer
                  </label>
                </div>  
              </div>
              <div class="help-block with-errors"></div>
          </div>

           <input type="text" id="idusuario" hidden="true" name="idusuario" value="<?php echo $_GET['idusuario']?>"/>
           <input type="text" id="token" hidden="true" name="token" value="<?php echo $_GET['token']?>">
            <div class="form-group ">
              <button type="submit" id="siguiente" class="btn btn-primary btn-lg btn-block login-button">Siguiente</button>
            </div>

            </form>
        </div>

        <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p id='msj'>Para resguardar su información, le enviamos un correo electrónico con un enlace para continuar con el proceso de registro.</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>



    </div>
</div>
<?php include '../includes/footer.php';?>

<script type="text/javascript"> 
      $( document ).ready(function() {

          if ( $('#nombreEmpresa').val=='')
            $('#siguiente').prop('disable','disable');
        
          var val = "<?php echo $result['success']?>";  
        
         
          $('#msj').html('');

          /*if (val == 1){
            $('#msj').html('Token Valido...');
            $('[data-remodal-id=reporte]').remodal().open();
          }

          if (val == 0){
            $('#msj').html('Token Invalido por favor intente nuevamente');
            $('[data-remodal-id=reporte]').remodal().open();
            window.location.replace("register.php");
          }*/

         
          
      });
</script>