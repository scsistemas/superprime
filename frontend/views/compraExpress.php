<?php include '../includes/header.php';?>
<div class="backend-user">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2" id="bankPaiment">

                <h1>Datos del producto</h1>

                <div class="container-fluid">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Producto</th>
                            <th class="text-center">Cantidad</th>
                            <th class="text-center">Costo</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <?php
                                $nombre_imagen="../../backend/images/fotos/".$producto['CodProducto']."/".$producto['ImagenesSelec'][0]['color']."/".$producto['ImagenesSelec'][0]['nombre'];
                                if (!file_exists($nombre_imagen))
                                    $nombre_imagen="../images/noImageThumb.jpg";?>
                                <img src="<?php echo $nombre_imagen;?>"  width="100">
                                <h7><?php echo $producto['Nombre'];?></h7>
                            </td>
                            <td  class="text-center"><?php echo $cantidad;?></td>
                            <td  class="text-center">Bs <?php echo $producto['Precio'];?></td>
                        </tr>
                        </tbody>
                    </table>

                    <h1>Datos para Facturación</h1>

                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        Por favor indiquenos los datos fiscales para la facturación y envío
                    </div>

                    <!-- Formulario para primera direccion -->

                    <form id="register" data-toggle="validator" method="post" action="../pages/process-CompraExpress.php" role="form">
                        <input type="hidden" id="cod_producto" name="cod_producto" value="<?php echo $codProd;?>">
                        <input type="hidden" id="cod_color" name="cod_color" value="<?php echo $codColor;?>">
                        <input type="hidden" id="cantidad" name="cantidad" value="<?php echo $cantidad;?>">
                        <input type="hidden" id="cod_talla" name="cod_talla" value="<?php echo $codTalla;?>">
                        <input type="hidden" id="p_envio" name="p_envio" value="S">
                        <input type="hidden" id="p_id_dir_f" name="p_id_dir_f" value="">
                        <input type="hidden" id="p_id_dir_e" name="p_id_dir_e" value="">
                        <input type="hidden" id="p_id_user" name="p_id_user" value="">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="nombre" class="cols-sm-2 control-label">Correo electrónico</label>
                                    <input type="email" class="form-control" name="p_correo" id="p_correo"  placeholder="Email" required/>
                                </div>
                            </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Nombre</label>
                                        <input name="nombre" id="nombre" type="text" class="form-control " required>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="cirif" class="cols-sm-2 control-label">C.I / RIF</label>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <select class="form-control" name="tipoid" id="tipoid" required>
                                                    <option value=""></option>
                                                    <option value="V">V</option>
                                                    <option value="E">E</option>
                                                    <option value="J">J</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-8">
                                                <input id="numid"  name="numid" type="text" pattern="[0-9]{8,9}" class="form-control" required>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Estado</label>
                                        <select class="form-control" name="estadof" id="estadof" required>
                                            <option value="">-Seleccione-</option>
                                            <?php foreach ($estados as $estado) { ?>
                                                <option
                                                    value="<?php echo $estado["CodEstado"] ?>"><?php echo $estado["DesEstado"] ?></option>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Ciudad</label>
                                        <select class="form-control" name="ciudadf" id="ciudadf" disabled="true" required>
                                            <option value="">-Seleccione-</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Municipio</label>
                                        <select class="form-control" name="municipiof" id="municipiof" disabled="true" required>
                                            <option value="">-Seleccione-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Parroquia</label>
                                        <select class="form-control" name="parroquiaf" id="parroquiaf" disabled="true" required>
                                            <option value="">-Seleccione-</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Código Postal</label>
                                        <input type="text" class="form-control" pattern="[0-9]{4}" name="codigoPostalf"
                                               id="codigoPostalf" placeholder="Código Postal" required/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="nombre" class="cols-sm-2 control-label">Calle 1</label>
                                <input type="text" class="form-control" name="calle1f"
                                       id="calle1f" placeholder="Calle" required/>
                            </div>

                            <div class="form-group">
                                <label for="nombre" class="cols-sm-2 control-label">Calle 2 <span>(Opcional)</span></label>
                                <input type="text" class="form-control" name="calle2f" id="calle2f" placeholder="Calle"/>
                            </div>

                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label for="nombre" class="cols-sm-2 control-label">Tipo de Vivienda</label>
                                    <select class="form-control" name="tipoViviendaf" id="tipoViviendaf" required>
                                        <option value=""></option>
                                        <option value="C">Casa</option>
                                        <option value="E">Edificio</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-6" >
                                    <label for="nombre" class="cols-sm-2 control-label">Nombre</label>
                                    <input type="text" class="form-control" name="nombrecf" id="nombrecf"
                                           placeholder="Nombre Casa"/>
                                </div>


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="nombre" class="cols-sm-2 control-label">Piso/Apartamento</label>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="pisof" id="piso"
                                                       placeholder="Piso"/>
                                            </div>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" name="apartamentof" id="apartamento"
                                                       placeholder="Apto"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="tcel" class="cols-sm-2 control-label">Telf Principal</label>
                                        <input type="text" id="tcelf" name="tcelf" class="form-control bfh-phone"
                                               minlength="14" data-format="(dddd)ddd-dddd"/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="thab"" class="cols-sm-2 control-label">Telf Secundario</label>
                                        <input type="text" id="thabf" name="thabf" class="form-control bfh-phone"
                                               minlength="14" data-format="(dddd)ddd-dddd"/>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="tofi" class="cols-sm-2 control-label">Telf Otro</label>
                                        <input type="text" id="tofif" name="tofif" class="form-control bfh-phone"
                                               minlength="14" data-format="(dddd)ddd-dddd"/>
                                    </div>
                                    <div class="col-lg-12" id="telefonosf">
                                    </div>
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>


                            <!-- Formulario para primera direccion -->
                            <br>

                            <!-- Multiple Radios -->
                            <div class="form-group" id="div_radios">
                                <div class="col-lg-4 col-md-offset-2">
                                    <div class="radio">
                                        <label for="radios-0">
                                            <input type="radio" class="flat" name="envio" id="radios-0" value="direccion1" checked>
                                            Enviar a la dirección fiscal
                                        </label>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="radio">
                                        <label for="radios-1">
                                            <input type="radio" class="flat" name="envio" id="culopeluo" value="direccion2">
                                            Enviar a otra dirección
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <!-- Multiple Radios -->


                            <br>
                            <br><br>


                            <div id="direccion2" style="display:none">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="nombre" class="cols-sm-2 control-label">Estado</label>
                                            <select class="form-control" name="estado" id="estado">
                                                <option value="">-Seleccione-</option>
                                                <?php foreach ($estados as $estado) { ?>
                                                    <option
                                                        value="<?php echo $estado["CodEstado"] ?>"><?php echo $estado["DesEstado"] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="nombre" class="cols-sm-2 control-label">Ciudad</label>
                                            <select class="form-control" name="ciudad" id="ciudad" disabled="disabled">
                                                <option value="">-Seleccione-</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="nombre" class="cols-sm-2 control-label">Municipio</label>
                                            <select class="form-control" name="municipio" id="municipio" disabled="true">
                                                <option value="">-Seleccione-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="nombre" class="cols-sm-2 control-label">Parroquia</label>
                                            <select class="form-control" name="parroquia" id="parroquia" disabled="true">
                                                <option value="">-Seleccione-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="nombre" class="cols-sm-2 control-label">Código Postal</label>
                                            <input type="text" class="form-control" name="codigoPostal" pattern="[0-9]{4}"
                                                   id="codigoPostal" placeholder="Código Postal"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="nombre" class="cols-sm-2 control-label">Calle 1</label>
                                    <input type="text" class="form-control"  name="calle1"
                                           id="calle1" placeholder="Calle"/>
                                </div>

                                <div class="form-group">
                                    <label for="nombre" class="cols-sm-2 control-label">Calle 2 <span>(Opcional)</span></label>
                                    <input type="text" class="form-control" name="calle2" id="calle2" placeholder="Calle"
                                    />
                                </div>

                                <div class="row">
                                    <div class="form-group col-lg-6">
                                        <label for="nombre" class="cols-sm-2 control-label">Tipo de Vivienda</label>
                                        <select class="form-control" name="tipoVivienda" id="tipoVivienda">
                                            <option value=""></option>
                                            <option value="C">Casa</option>
                                            <option value="E">Edificio</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="nombre" class="cols-sm-2 control-label">Nombre</label>
                                        <input type="text" class="form-control" name="nombrec" id="nombrec"
                                               placeholder="Nombre Casa"/>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="nombre" class="cols-sm-2 control-label">Piso/Apartamento</label>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" name="piso" id="piso"
                                                           placeholder="Piso"/>
                                                </div>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" name="apartamento" id="apartamento"
                                                           placeholder="Apto"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="tcel" class="cols-sm-2 control-label">Telf Principal</label>
                                            <input type="text" id="tcel" name="tcel" class="form-control bfh-phone"
                                                   minlength="14" data-format="(dddd)ddd-dddd"/>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="thab" class="cols-sm-2 control-label">Telf Secundario</label>
                                            <input type="text" id="thab" name="thab" class="form-control bfh-phone"
                                                   minlength="14" data-format="(dddd)ddd-dddd"/>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="tofi" class="cols-sm-2 control-label">Telf Otro</label>
                                            <input type="text" id="tofi" name="tofi" class="form-control bfh-phone"
                                                   minlength="14" data-format="(dddd)ddd-dddd"/>
                                        </div>
                                        <div class="col-lg-12" id="telefonos">
                                        </div>
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-primary login-button">
                                    Guardar
                                </button>
                            </div>

                    </form>

                    <!-- Formulario para segunda direccion -->
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<?php include '../includes/footer.php'; ?>
<script src="../js/views/compraExpress.js"></script>
