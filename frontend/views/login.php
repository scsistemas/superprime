<?php include '../includes/header.php';?>

  <div class="register">



    <div class="container">
        
        <div class="jumbotron">
          
          <h3>Iniciar</h3>

          <form id="register" action="../pages/login.php" method="post" role="form" data-toggle="validator">
              <input type="hidden" id="codProd" name="codProd" value="<?php echo $codProd; ?>">
              <input type="hidden" id="codColor" name="codColor" value="<?php echo $codColor ?>">
              <input type="hidden" id="codTalla" name="codTalla" value="<?php echo $codTalla;?>">
              <input type="hidden" id="cantidad" name="cantidad" value="<?php echo $cantidad; ?>">
              <input type="hidden" id="redirect" name="redirect" value="<?php echo $redirect; ?>">

            <div class="form-group">
              <label for="email" class="cols-sm-2 control-label">E-mail</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                  <input type="email" class="form-control" name="email" id="email"  placeholder="Email" required/>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="cols-sm-2 control-label">Contraseña</label>
              <div class="cols-sm-10">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                  <input type="password" data-minlength="8" class="form-control" name="password" id="password"  placeholder="Contraseña" required/> 
                </div>
              </div>
            </div>

            <div class="checkbox">
              <label>
                <input type="checkbox" id="rememberUser"> Recordarme
              </label>
            </div>

            <div class="form-group ">

              <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Ingresar</button>
            </div>

            <div class="form-group ">
              <a href="../pages/forgot-password.php" class="">Olvidé mi contraseña</a>
            </div>

            <div class="or-box">
              <span class="or">Ó</span>
            </div>

            <div class="form-group ">
              <button type="button" class="btn btn-primary btn-lg btn-block login-button"><i class="fa fa-facebook" aria-hidden="true"></i>   Accede con Facebook</button>
            </div>

            <div class="form-group ">
              <button type="button" class="btn btn-danger btn-lg btn-block login-button"><i class="fa fa-google" aria-hidden="true"></i>    Accede con  Google</button>
            </div>



            </form>
        </div>

        <div class="remodal" data-remodal-id="reporte">
          <button data-remodal-action="close" class="remodal-close"></button>
          <h1>Información</h1>
          <p id="msj">Para resguardar su información, le enviamos un correo electrónico con un enlace para continuar con el proceso de registro.</p>
          <br>
          <button data-remodal-action="confirm" class="btn btn-success">OK</button>
        </div>

    </div>
    </div>
   <?php include '../includes/footer.php';?>

   <script type="text/javascript"> 
    $( document ).ready(function() {

       
        var val = <?php echo $varMsj ?>;
        //var reg = <?php echo $_GET['register']?>;
        
        //console.log(val);
        //console.log(reg);
        $('#msj').html('');

        if (val == 1){
          $('#msj').html('Usuario y clave correcto');
          $('[data-remodal-id=reporte]').remodal().open();
            
        }

        if (val == 0){
          $('#msj').html('Usuario o Clave invalido... Intente nuevamente');
          $('[data-remodal-id=reporte]').remodal().open();
        }

        /*if (reg == 1){
          $('#msj').html('Usuario registrado correctamente');
          $('[data-remodal-id=reporte]').remodal().open();
        }*/
        
    });
</script>