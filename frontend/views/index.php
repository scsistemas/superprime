<?php include '../includes/header.php';?>
<?php include '../includes/globalMenu.php';?>

<div class="slider">
        <ul class="rslides">
        <?php
            foreach ($categoriasSlider['Categorias']as $categorias){
            ?>
            <li>
                <img src="../images/<?php echo $categorias['Imagen'];?>" alt="">
                <div class="container detailsCategory">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <h1><?php echo $categorias['Nombre'];?></h1>
                            <p><?php echo $categorias['Descripcion'];?></p>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                            <ul>
                                <?php
                                foreach ($categorias['subCategorias'] as $subCategorias){
                                ?>
                                <li><a href="product-list.php?categoria=<?php echo $categorias['Id']; ?>&subcategoria=<?php echo $subCategorias['Id'];?>&nombrecateg=<?php echo $categorias['Nombre'];?>"><?php echo $subCategorias['Nombre']; ?></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                            <a href="product-list.php?categoria=<?php echo $categorias['Id'];?>&nombrecateg=<?php echo $categorias['Nombre'];?>" class="more pull-right">Ver Todos</a>
                        </div>
                    </div>
                </div>
            </li>
        <?php
        }
        ?>
		</ul>

    </div>

    <!-- Aliados -->
	<div class="aliados">
		<div class="container">
			<h1>Aliados</h1>
			<img src="../images/rs21.jpg"><img src="../images/kriza.jpg"><img src="../images/volpe.jpg"><img src="../images/vita.jpg"><img src="../images/prime.jpg">
		</div>
	</div>
 

<?php include '../includes/footer.php';?>

<script>
  $(document).ready(function() {

  	// Inicializar el slider
  	$(".rslides").responsiveSlides({
  		auto: false,
        pager: false,
        nav: true,
        timeout: 10000,
        speed: 500,
        manualControls: '.sliderNav'
 	});

  	// Hacer scroll a la pag al hacer click en la navegacion
  	$('.sliderNav li a').click(function(){
		$('.globalMenu').ScrollTo({
		    duration: 500,
		    easing: 'linear'
		});
  	}); 

  	$('.rslides_nav').click(function(){
		$('.globalMenu').ScrollTo({
		    duration: 500,
		    easing: 'linear'
		});
  	}); 

 	

  });

</script>