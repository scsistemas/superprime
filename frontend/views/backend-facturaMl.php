 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
	    				  	<h4>Carlos Rojas</h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>


	    		<div class="col-lg-9" id="bankPaiment">

	    			<h1>Datos para Facturación</h1>

	    		 	<div class="container-fluid">

	    		 		<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Por favor indiquenos los datos fiscales para la facturación y envío</div>
	    		 		


   		 				<!-- Formulario para primera direccion -->

	    		 		<form  data-toggle="validator" role="form" >
							 
							 <div class="row">
							 	<div class="col-lg-6">
									<div class="form-group">
									  <label for="nombre" class="cols-sm-2 control-label">Nombre</label>
									  <input  name="nombre" type="text" class="form-control " required>
									</div>
							 	</div>

							 	<div class="col-lg-6">
					             <div class="form-group">
									<label for="cirif" class="cols-sm-2 control-label">C.I / RIF</label>
									<div class="row">
										<div class="col-lg-4">
											<select class="form-control" name="cirif" id="cirif" required>
												<option value=""></option>
												<option value="1">V</option>
												<option value="2">E</option>
												<option value="2">J</option>
											</select>
										</div>
										<div class="col-lg-8">
											<input  name="textinput" type="text" class="form-control" required>
										</div>
									</div>
									
					              </div> 
					            </div>
							 </div>

					          <div class="row">
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Estado</label>
					                  <select class="form-control" name="estado" id="estado" required>
										  <option value=""></option>
					          			  <option value="V">Distrito Federal</option>
					                      <option value="E">Miranda</option>
					                      <option value="N">Merida</option>
					                    </select>
					              </div>
					            </div>
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Ciudad</label>
					                  <select class="form-control" name="ciudad" id="ciudad" required>
										  <option value=""></option>
					                      <option value="V">Guarenas</option>
					                      <option value="E">Guatire</option>
					                      <option value="N">Los Teques</option>
					                    </select>
					              </div>
					            </div>
					          </div>

					          <div class="row">
					            <div class="col-lg-4">
					             <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Municipio</label>
					                  <select class="form-control" name="municipio" id="municipio" required>
					                  	  <option value=""></option>
					                      <option value="V">Plaza</option>
					                      <option value="E">Zamora</option>
					                      <option value="N">Guaicaipuro</option>
					                    </select>
					              </div> 
					            </div>
					            <div class="col-lg-4">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Parroquia</label>
					                  <select class="form-control" name="parroquia" id="parroquia" required>
					                  	  <option value=""></option>
					                      <option value="V">Guarenas</option>
					                      <option value="E">Guatire</option>
					                      <option value="N">Los Teques</option>
					                    </select>
					              </div>
					            </div>
					            <div class="col-lg-4">
					            	<div class="form-group">
							              <label for="nombre" class="cols-sm-2 control-label">Código Postal</label>
							              <input type="text" class="form-control" name="codigoPostal" value="1220" id="codigoPostal"  placeholder="Código Postal" required/>
							          </div>
					            </div>
					          </div>

					          <div class="form-group">
					              <label for="nombre" class="cols-sm-2 control-label">Calle 1</label>
					              <input type="text" class="form-control" value="Urb Los Roques, Av principal" name="calle1" id="calle1"  placeholder="Calle" required/>
					          </div>

					          <div class="form-group">
					              <label for="nombre" class="cols-sm-2 control-label">Calle 2 <span>(Opcional)</span></label>
					              <input type="text" class="form-control" name="calle2" id="calle2"  placeholder="Calle" required/>
					          </div>

					          <div class="row">
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Tipo de Vivienda</label>
					                  <select class="form-control" name="tipoVivienda" id="tipoVivienda" required>
					                  	  <option value=""></option>
					                      <option value="V">Casa</option>
					                      <option value="E">Edificio</option>
					                    </select>
					              </div>
					            </div>
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Piso/Apartamento</label>
					                  <div class="row">
					                    <div class="col-lg-6">
					                      <input type="text" class="form-control" name="piso" id="piso" value="07" placeholder="Piso" />
					                    </div>
					                    <div class="col-lg-6">
					                      <input type="text" class="form-control" name="apartamento" id="apartamento" value="08"  placeholder="Apto" />
					                    </div>
					                  </div>
					              </div>
					            </div>
					            <div class="col-lg-4">
					              
					            </div>
					          </div>

					          <div class="row">
					            <div class="col-lg-4">
					                <label for="nombre" class="cols-sm-2 control-label">Telf Celular</label>
					                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" value="04266058079">
					            </div>
					            <div class="col-lg-4">
					                <label for="nombre" class="cols-sm-2 control-label">Telf Habitación</label>
					                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" value="02123698578">
					            </div>
					            <div class="col-lg-4">
					                <label for="nombre" class="cols-sm-2 control-label">Telf Oficina</label>
					                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" value="02129504687">
					            </div>
					            <div class="col-lg-12">
					              <span class="help-block">Debe especificar al menos un número telefónico</span>
					            </div>
					          </div>
				

					        </form>

					        <!-- Formulario para primera direccion -->
					           

					        
					        <br>
					        <br>

							<!-- Multiple Radios -->
							<div class="form-group">
							  <div class="col-lg-4 col-md-offset-2">
							  <div class="radio">
							    <label for="radios-0">
							      <input type="radio" class="flat" name="envio" id="radios-0" value="direccion1"  >
							      Enviar a la dirección fiscal
							    </label>
								</div>
							 
							  </div>
							  <div class="col-lg-6">
							  	 <div class="radio">
							    <label for="radios-1">
							      <input type="radio" class="flat" name="envio" id="radios-1" value="direccion2" >
							      Enviar a otra dirección
							    </label>
								</div>
							  </div>
							</div>
							<!-- Multiple Radios -->


							<br>
							<br><br>


							<!-- Formulario para segunda direccion -->

							<form  data-toggle="validator" role="form" id="direccion2" style="display:none" >


					          <div class="row">
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Estado</label>
					                  <select class="form-control" name="estado" id="estado" required>
										  <option value=""></option>
					          			  <option value="V">Distrito Federal</option>
					                      <option value="E">Miranda</option>
					                      <option value="N">Merida</option>
					                    </select>
					              </div>
					            </div>
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Ciudad</label>
					                  <select class="form-control" name="ciudad" id="ciudad" required>
										  <option value=""></option>
					                      <option value="V">Guarenas</option>
					                      <option value="E">Guatire</option>
					                      <option value="N">Los Teques</option>
					                    </select>
					              </div>
					            </div>
					          </div>

					          <div class="row">
					            <div class="col-lg-4">
					             <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Municipio</label>
					                  <select class="form-control" name="municipio" id="municipio" required>
					                  	  <option value=""></option>
					                      <option value="V">Plaza</option>
					                      <option value="E">Zamora</option>
					                      <option value="N">Guaicaipuro</option>
					                    </select>
					              </div> 
					            </div>
					            <div class="col-lg-4">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Parroquia</label>
					                  <select class="form-control" name="parroquia" id="parroquia" required>
					                  	  <option value=""></option>
					                      <option value="V">Guarenas</option>
					                      <option value="E">Guatire</option>
					                      <option value="N">Los Teques</option>
					                    </select>
					              </div>
					            </div>
					            <div class="col-lg-4">
					            	<div class="form-group">
							              <label for="nombre" class="cols-sm-2 control-label">Código Postal</label>
							              <input type="text" class="form-control" name="codigoPostal" value="1220" id="codigoPostal"  placeholder="Código Postal" required/>
							          </div>
					            </div>
					          </div>

					          <div class="form-group">
					              <label for="nombre" class="cols-sm-2 control-label">Calle 1</label>
					              <input type="text" class="form-control" value="Urb Los Roques, Av principal" name="calle1" id="calle1"  placeholder="Calle" required/>
					          </div>

					          <div class="form-group">
					              <label for="nombre" class="cols-sm-2 control-label">Calle 2 <span>(Opcional)</span></label>
					              <input type="text" class="form-control" name="calle2" id="calle2"  placeholder="Calle" required/>
					          </div>

					          <div class="row">
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Tipo de Vivienda</label>
					                  <select class="form-control" name="tipoVivienda" id="tipoVivienda" required>
					                  	  <option value=""></option>
					                      <option value="V">Casa</option>
					                      <option value="E">Edificio</option>
					                    </select>
					              </div>
					            </div>
					            <div class="col-lg-6">
					              <div class="form-group">
					                  <label for="nombre" class="cols-sm-2 control-label">Piso/Apartamento</label>
					                  <div class="row">
					                    <div class="col-lg-6">
					                      <input type="text" class="form-control" name="piso" id="piso" value="07" placeholder="Piso" />
					                    </div>
					                    <div class="col-lg-6">
					                      <input type="text" class="form-control" name="apartamento" id="apartamento" value="08"  placeholder="Apto" />
					                    </div>
					                  </div>
					              </div>
					            </div>
					            <div class="col-lg-4">
					              
					            </div>
					          </div>

					          <div class="row">
					            <div class="col-lg-4">
					                <label for="nombre" class="cols-sm-2 control-label">Telf Celular</label>
					                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" value="04266058079">
					            </div>
					            <div class="col-lg-4">
					                <label for="nombre" class="cols-sm-2 control-label">Telf Habitación</label>
					                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" value="02123698578">
					            </div>
					            <div class="col-lg-4">
					                <label for="nombre" class="cols-sm-2 control-label">Telf Oficina</label>
					                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" value="02129504687">
					            </div>
					            <div class="col-lg-12">
					              <span class="help-block">Debe especificar al menos un número telefónico</span>
					            </div>
					          </div>
					           
					            

					        </form>
								
					        <!-- Formulario para segunda direccion -->

								<div class="form-group text-center">
					              <button type="submit" class="btn btn-primary login-button" data-remodal-target="reporte">Guardar</button>
					            </div>
					    </div>

	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>

<script type="text/javascript">
	$(document).ready(function() {	


		// Mostrar direcciones de envio con radiobutton
		$("input[name='envio']").on('ifChecked', function(event) {
		    $('#direccion2').css('display', ($(this).val() === 'direccion2') ? 'block':'none');
		});


	});
</script>