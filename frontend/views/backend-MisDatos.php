 <?php include '../includes/header.php';?>



    <div class="backend-user">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-lg-3">
	    			<div class="user-head row">
	    				  <div class="col-lg-4 avatar">
	    				  	 <img  width="64" hieght="60" src="../images/avatar.png">
	    				  </div>
	    				  <div class="col-lg-8 info">
							  <h4><span id="nombre_u"></span></h4>
                              <a href="#">Editar perfil</a>
	    				  </div>
                      </div>
	    			 <?php include '../includes/userMenu.php';?>
	    		</div>
	    		<div class="col-lg-9" id="misDatos">

	    			<h1>Mis Datos</h1>


					  <!-- Nav tabs -->
					  <ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#acceso" aria-controls="acceso" role="tab" data-toggle="tab">Información de Acceso</a></li>
					    <li role="presentation"><a href="#personal" aria-controls="personal" role="tab" data-toggle="tab">Información Personal</a></li>
					    <li role="presentation"><a href="#envio" aria-controls="envio" role="tab" data-toggle="tab">Mis Direcciones</a></li>
					  </ul>

					  <!-- Tab panes -->
					  <div class="tab-content">

					  	<!-- Informacion de acceso -->
					    <div role="tabpanel" class="tab-pane active" id="acceso">

					    	<h4>Cambio de Clave de Acceso</h4>

					    	<form action="javascript:changePassword()" class="form-horizontal"  data-toggle="validator" role="form">

								<!-- Password input-->
								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Contraseña Actual</label>
								  <div class="col-md-4">
                                      <input id="currentPassword" name="currentPassword" type="password" placeholder="" class="form-control input-md" required>
								  </div>
								</div>

								<!-- Password input-->
								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Nueva Contraseña</label>
								  <div class="col-md-4">
                                      <input id="newPassword" name="newPassword" type="password" placeholder="" class="form-control input-md" data-minlength="8" data-minlength-error="La contraseña debe ser superior a 8 caracteres" required>
								  </div>
								</div>

								<!-- Password input-->
								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Repita Contraseña</label>
								  <div class="col-md-4">
								    <input id="passwordinput" name="passwordinput" type="password" data-minlength="8" data-minlength-error="La contraseña debe ser superior a 8 caracteres"  data-match="#newPassword" data-match-error="La contraseña no coincide" placeholder="" class="form-control input-md" required >
								  	<div class="help-block with-errors"></div>
								  </div>
								</div>
                                <div class="remodal" data-remodal-id="password-message">
                                    <button data-remodal-action="close" class="remodal-close"></button>
                                    <h1>Información</h1>
                                    <p id="msj-password"></p>
                                    <br>
                                    <button data-remodal-action="confirm" class="btn btn-success">OK</button>
                                </div>
                                <div class="remodal" data-remodal-id="password-confirm">
                                    <div id="confirmation-box">
                                        <p>¿Desea realizar el cambio de contraseña?</p>
                                        <a  data-remodal-action="confirm" class="remodal-confirm confirm-password"><span>Si</span></a>
                                        <a  data-remodal-action="cancel" class="remodal-cancel">No</a>
                                    </div>
                                </div>
								<!-- Button -->
								<div class="form-group">
								  <label class="col-md-3 control-label" for="singlebutton"></label>
								  <div class="col-md-4">
                                      <button type="submit" name="singlebutton" class="btn btn-primary">Guardar</button>
								  </div>
								</div>

							</form>

					    </div>

					    <!-- Informacion personal -->
					    <div role="tabpanel" class="tab-pane" id="personal">
							
							<h4>Actualizar datos personales</h4>
                            <input type="hidden" name="user_id_type" id="user_id_type">
                            <input type="hidden" name="user_mail" id="user_mail">
					    	<form action="javascript:savePerson()" class="form-horizontal"  data-toggle="validator" role="form">

					    		
								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Nombres</label>
								  <div class="col-md-4">
								    <input id="name" name="name" type="text" placeholder="" value="Carlos Alberto" class="form-control input-md">
								  </div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Apellidos</label>
								  <div class="col-md-4">
								    <input id="lastname" name="lastname" type="text" placeholder="" class="form-control input-md">
								  </div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label" for="passwordinput">Documento de Identidad</label>
									<div class="col-lg-4">
									    <div class="row">
									      <div class="col-lg-3">
									        <select class="form-control" name="tipoCedula" id="tipoCedula" style="width:60px" onclick="hideDiv()">
									          <option value="V">V</option>
									          <option value="E">E</option>
									          <option value="N">N</option>
									          <option value="J">J</option>
									          <option value="G">G</option>
									        </select>
									      </div>
									      <div class="col-lg-9">
									        <input type="text" class="form-control col-lg-3" name="cedula" id="cedula" required/>
									      </div>
									    </div>	
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-3 control-label" for="passwordinput">Fecha de Nacimiento</label>
									<div class="col-md-4">
										<div class="input-append date"  data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
											<input class="form-control" id="dpYears" size="16" type="text" value="12-02-2012" readonly>
										</div>
									</div>
								</div>

								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Sexo</label>
								  <div class="col-md-4" style="padding-top: 7px;">
								    <div class="row">
						                <div class="col-lg-5">
						                  <label>
						                    <input type="radio" name="optionsRadios" id="hombre" value="option1" >
						                    Hombre
						                  </label>
						                </div>
						                <div class="col-lg-4">
						                  <label>
						                    <input type="radio" name="optionsRadios" id="mujer" value="option1" >
						                    Mujer
						                  </label>
						                </div>  
						              </div>
								  </div>
								</div>

                                <div class="form-group" id="contacto_div">
                                    <label class="col-md-3 control-label" for="passwordinput">Persona de Contacto</label>
                                    <div class="col-md-4">
                                        <input id="persona_contacto" name="persona_contacto" type="text" placeholder="" class="form-control input-md">
                                    </div>
                                </div>

								<!-- Password input-->
								<div class="form-group">
								  <label class="col-md-3 control-label" for="passwordinput">Foto de Perfil</label>
								  <div class="col-md-4">
								  	<div class="fileinput fileinput-new" data-provides="fileinput">
                                        <input type="hidden" name="path_hidden" id="path_hidden">
                                        <div id="person-img" class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Selecionar imagen</span><span class="fileinput-exists">Cambiar</span><input type="file" name="..."></span>
                                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="remodal" data-remodal-id="personal-information-message">
                                    <button data-remodal-action="close" class="remodal-close"></button>
                                    <h1>Información</h1>
                                    <p id="msj-personal-information"></p>
                                    <br>
                                    <button data-remodal-action="confirm" class="btn btn-success">OK</button>
                                </div>
                                <div class="remodal" data-remodal-id="personal-confirm">
                                    <div id="confirmation-box">
                                        <p>¿Desea realizar los cambios sobre su informacion personal?</p>
                                        <a  data-remodal-action="confirm" class="remodal-confirm confirm-personal"><span>Si</span></a>
                                        <a  data-remodal-action="cancel" class="remodal-cancel">No</a>
                                    </div>
                                </div>
								<!-- Button -->
								<div class="form-group">
								  <label class="col-md-3 control-label" for="singlebutton"></label>
								  <div class="col-md-4">
								    <button type="submit" name="singlebutton" class="btn btn-primary">Guardar</button>
								  </div>
								</div>

							</form>
					    	
					    </div>


					    <!-- Direcciones de envio -->
					    <div role="tabpanel" class="tab-pane" id="envio">
					    	
					    	<h4>Modificar direcciónes de envío</h4>

					    	<table id="table_direcciones" class="table table-hover table-bordered" cellspacing="0" width="100%">
					    		<thead> 
					    			<tr> 
						    			<th style="width: 100px;">Predeterminada</th> 
						    			<th>Alias</th> 
						    			<th>Ciudad</th> 
						    			<th>Opciones</th> 
					    			</tr> 
					    		</thead> 
					    		<tbody>
					    		</tbody>
					    	</table>

					    	<div class="text-center"> 
					    		<a class="btn btn-success" onclick="clearDataDirecciones()" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Añadir Dirección</a>
					    	</div>


					    	<!-- Modal para edición o carga de dirección -->

					    	<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title">Crear/Editar nueva dirección de envío</h4>
							      </div>
							      <div class="modal-body">
							        <form action="javascript:saveFacturacion()" id="register" role="form" data-toggle="validator">

							          <div class="row">
							            <div class="col-lg-6">
							              <div class="form-group">
                                              <input type="hidden" name="address_id" id="address_id">
                                              <input type="hidden" name="estado_user" id="estado_user">
							                  <label for="nombre" class="cols-sm-2 control-label">Estado</label>
							                  <select class="form-control" name="estado" id="estado" required>
                                                  <option CodEstado='0'>Seleccione...</option>
							                    </select>
							              </div>
							            </div>
							            <div class="col-lg-6">
							              <div class="form-group">
                                              <input type="hidden" name="ciudad_user" id="ciudad_user">
							                  <label for="nombre" class="cols-sm-2 control-label">Ciudad</label>
							                  <select class="form-control" name="ciudad" id="ciudad" required>
                                                  <option CodEstado='0' CodCiudad='0'>Seleccione...</option>
							                    </select>
							              </div>
							            </div>
							          </div>

							          <div class="row">
							            <div class="col-lg-6">
							             <div class="form-group">
                                             <input type="hidden" name="municipio_user" id="municipio_user">
							                  <label for="nombre" class="cols-sm-2 control-label">Municipio</label>
							                  <select class="form-control" name="municipio" id="municipio" required>
                                                  <option CodEstado='0' CodCiudad='0' CodMunicipio='0'>Seleccione...</option>
							                    </select>
							              </div> 
							            </div>
							            <div class="col-lg-6">
							              <div class="form-group">
                                              <input type="hidden" name="parroquia_user" id="parroquia_user">
							                  <label for="nombre" class="cols-sm-2 control-label">Parroquia</label>
							                  <select class="form-control" name="parroquia" id="parroquia" required>
                                                  <option CodEstado='0' CodCiudad='0' CodMunicipio='0' CodParroquia='0'>Seleccione...</option>
							                    </select>
							              </div>
							            </div>
							          </div>

							          <div class="row">
							          	<div class="col-lg-6">
											<div class="form-group">
											  <label for="nombre" class="cols-sm-2 control-label">Código Postal</label>
											  <input type="text" class="form-control" name="codigoPostal" id="codigoPostal"  placeholder="Código Postal" required/>
											</div>
							          	</div>
							          	<div class="col-lg-6">
							          		<div class="form-group">
											  <label for="nombre" class="cols-sm-2 control-label">Alias</label>
											  <input onblur="validarAlias()" type="text" class="form-control" name="alias" id="alias"  placeholder="" required/>
											</div>
							          	</div>
							          </div>
							          

							          <div class="form-group">
							              <label for="nombre" class="cols-sm-2 control-label">Calle 1</label>
							              <input type="text" class="form-control" name="calle1" id="calle1"  placeholder="Calle" required/>
							          </div>

							          <div class="form-group">
							              <label for="nombre" class="cols-sm-2 control-label">Calle 2 <span>(Opcional)</span></label>
							              <input type="text" class="form-control" name="calle2" id="calle2"  placeholder="Calle" required/>
							          </div>

							          <div class="row">
							            <div class="col-lg-6">
							              <div class="form-group">
							                  <label for="nombre" class="cols-sm-2 control-label">Tipo de Vivienda</label>
							                  <select class="form-control" name="tipoVivienda" id="tipoVivienda" required>
							                      <option></option>
							                      <option value="CASA">Casa</option>
							                      <option value="EDIFICIO">Edificio</option>
							                    </select>
							              </div>
							            </div>
							            <div class="col-lg-6">
							              <div class="form-group">
							                  <label for="nombre" class="cols-sm-2 control-label">Piso/Apartamento</label>
							                  <div class="row">
							                    <div class="col-lg-6">
							                      <input type="text" class="form-control" name="piso" id="piso"  placeholder="Piso" />
							                    </div>
							                    <div class="col-lg-6">
							                      <input type="text" class="form-control" name="apartamento" id="apartamento"  placeholder="Apto" />
							                    </div>
							                  </div>
							              </div>
							            </div>
							            <div class="col-lg-4">
							              
							            </div>
							          </div>

							          <div class="row">
							            <div class="col-lg-4">
							                <label for="nombre" class="cols-sm-2 control-label">Telf Celular</label>
							                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" name="celular" id="celular">
							            </div>
							            <div class="col-lg-4">
							                <label for="nombre" class="cols-sm-2 control-label">Telf Habitación</label>
							                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" name="telf_habitacion" id="telf_habitacion">
							            </div>
							            <div class="col-lg-4">
							                <label for="nombre" class="cols-sm-2 control-label">Telf Oficina</label>
							                <input type="text" class="form-control bfh-phone" data-format=" (dddd) ddd-dddd" name="telf_oficina" id="telf_oficina">
							            </div>
                                          <input type="hidden" name="new_direccion" id="new_direccion" value=''>
							            <div class="col-lg-12">
							              <span class="help-block">Debe especificar al menos un número telefónico</span>
							            </div>
                                          <div class="remodal" data-remodal-id="address-information-message">
                                              <button data-remodal-action="close" class="remodal-close"></button>
                                              <h1>Información</h1>
                                              <p id="msj-address-information"></p>
                                              <br>
                                              <button data-remodal-action="confirm" class="btn btn-success">OK</button>
                                          </div>
                                          <div class="remodal" data-remodal-id="address-confirm-delete">
                                              <div id="confirmation-box">
                                                  <p>¿Desea eliminar esta direccion?</p>
                                                  <input type="hidden" name="address_id_delete" id="address_id_delete">
                                                  <a  data-remodal-action="confirm" class="remodal-confirm confirm-address-delete"><span>Si</span></a>
                                                  <a  data-remodal-action="cancel" class="remodal-cancel">No</a>
                                              </div>
                                          </div>
                                          <div class="remodal" data-remodal-id="address-confirm-update">
                                              <div id="confirmation-box">
                                                  <p>¿Desea realizar los cambios sobre esta direccion?</p>
                                                  <input type="hidden" name="address_id_delete" id="address_id_delete">
                                                  <a  data-remodal-action="confirm" class="remodal-confirm confirm-address-update"><span>Si</span></a>
                                                  <a  data-remodal-action="cancel" class="remodal-cancel">No</a>
                                              </div>
                                          </div>
							          </div>
							           
							            <div class="form-group text-center">
							              <button type="submit" class="btn btn-primary login-button">Guardar</button>
							            </div>

							            </form>
							      </div>
	
							    </div><!-- /.modal-content -->
							  </div><!-- /.modal-dialog -->
							</div><!-- /.modal -->

					    </div>
					  </div>


	    		 	

	    		</div>
	    	</div>
	    </div>
    </div>

 <?php include '../includes/footer.php';?>
 <script>
     var userId = <?php echo  $_SESSION['userid']?>;
 </script>
 <script type="text/javascript" src="../js/app/misDatos.js"></script>
