
/* ==========================================================
 * Scripts
 * ========================================================== */

jQuery(document).ready(function($){
    


// iCheck
$(document).ready(function() {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-red',
                radioClass: 'iradio_flat-red'
            });
        });
    }
});

// Inicializar tootltip
$('[data-toggle="tooltip"]').tooltip();

// Seleccionar todos los checkbox del carrito

// Validador en el formulario de registro
$('#register').validator()

//Datepicker en el formulario continue-register
$('#dpYears').datepicker()

$('#tipoCedula').change(function() {
    opt = $(this).val();
    if (opt=="J" || opt=="G") {
        //$('.identidad').html('<div class="form-group"><label for="nombreEmpresa" class="cols-sm-2 control-label">Nombre de la Empresa</label><input type="text" class="form-control" name="nombreEmpresa" id="nombreEmpresa"  placeholder="Nombre de la Empresa" required/><div class="help-block with-errors"></div></div>');
        $('#identidad').attr('style','visibility:true; height:80px');
        $('#nombreEmpresa').prop('required');
        $('#fechaN').html('<label for="nombre" class="cols-sm-2 control-label">Fecha de Nacimiento del Representante Legal <span>(Opcional)</span></label>');
        $('#labelnombre').html('<label for="nombre" class="cols-sm-2 control-label">Nombre del Representante Legal</label>')
        $('#labelapellido').html('<label for="nombre" class="cols-sm-2 control-label">Apellido del Representante Legal</label>')
    }
    else
    {
        //$('.identidad').html('');
         $('#nombreEmpresa').val('vacio');
        $('#identidad').attr('style','visibility:hidden; height:0px');
        $('#fechaN').html('<label for="nombre" class="cols-sm-2 control-label">Fecha de Nacimiento <span>(Opcional)</span></label>');
        $('#labelnombre').html('<label for="nombre" class="cols-sm-2 control-label">Nombre</label>')
        $('#labelapellido').html('<label for="nombre" class="cols-sm-2 control-label">Apellido</label>')
    }

});

// numeric
$("input.numeric").numeric()

// phone input

$("#datepicker").datepicker( {
    format: "mm-yyyy",
    viewMode: "years", 
    minViewMode: "months"
});
if($('#id_user').val()!=''){
devuelveCantidadCarrito($('#id_user').val());
    $(".cartButton").click(function () {
        window.location.href="cart.php";

    });
}


});