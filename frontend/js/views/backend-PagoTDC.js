$(document).ready(function () {


    $('[data-toggle="tooltip"]').tooltip();
    $('#fecha').datepicker({
        format: "mm/yyyy",
        startDate: new Date(),
        startView: 2,
        minViewMode: 1,
        autoclose: true
    });

    $('form').validator().on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            pagarPedido();
            e.preventDefault();
        }
    });

    $(".precio").each(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var precio = $(this).attr('data-value');
        $('#preciounitario-' + codProducto).append(precio.numberToMoneda());
    });
    $(".monto").each(function () {
        var precio = $(this).attr('data-value');
        $('#' +$(this).attr("id")).append(precio.numberToMoneda());
        console.log($(this).attr("id"));
    });

    pagarPedido = function () {
        var parametros = {
            'cod_pedido': $('#cod_pedido').val(),
            'id_user': $('#id_user').val(),
            'monto': $('#total').val(),
            'numta': $('#numtarjeta').val(),
            'cod': $('#codigo').val(),
            'tipo': $('#tipot').val(),
            'fec': $('#fecha').val(),
            'nomta': $('#nomtarjeta').val(),
            'tipoid': $('#tipoid').val(),
            'numid': $('#numid').val()
        };
        $.ajax({
                type: "POST",
                url: "../../admin/index.php?service=productoservices&metodo=PagarPedido",
                data: JSON.stringify(parametros),
                contentType: "application/json",
                success: function (respuesta) {
                    var ajaxResponse = $.parseJSON(respuesta);
                    var mensaje = ajaxResponse.Mensaje;
                    if (ajaxResponse != null) {
                        if (ajaxResponse.success == 1) {
                            mostrarMensajeError('Exito', mensaje, 'INFO');
                        } else {
                            mostrarMensajeError('Error', mensaje, 'ERROR');
                        }
                    } else {
                        mostrarMensajeError('Error', 'Por favor intente mas tarde', 'ERROR');
                    }

                }, beforeSend: function () {
                    $.blockUI({
                        message: '<h4>Por favor Espere...</h4>'
                    });

                },
                complete: function () {
                    $.unblockUI();
                }
            }
        )

    };


});

