$('table').DataTable({
    "language": {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "bFilter": false,
    "createdRow": function( row, data, dataIndex ) {
        $(row).addClass('row item');
    },
    "aoColumns": [
        {"sClass": "row item" }
    ]
});

$(document).ready(function() {
    var metodo;
    $('#desde, #hasta').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    obtenerDeseos();
});


//Obtener Lista de Deseos
function obtenerDeseos(){
    var templatefila = Handlebars.getTemplate('../views/templates/filaMiLista');
    var codUsuario = $('#id_user').val();
    $.ajax({
        type: 'GET',
        url: '../../admin/index.php?service=userservices&metodo=ObtenerListaDeseos&p_id_usuario='+codUsuario,
        dataType: 'json',
        success: function (response) {
            var item_to_append;
            $('#table_wishes').DataTable().clear();
            var t = $('#table_wishes').DataTable();
            $.each(response.mi_lista_de_deseo, function(index, value) {
                item_number= "item_"+index;
                item_to_append=templatefila(value);
                t.row.add([item_to_append]).draw(false);
            });
            crearEventos();
        },beforeSend: function () {
            $.blockUI({
                message: '<h4>Por favor Espere...</h4>'
            });

        },
        complete: function () {
            $.unblockUI();
            $("#table_wishes").show('slow');
        },
        error: function(xhr, status, error) {
            var acc = []
            $.each(xhr, function(index, value) {
                acc.push(index + ': ' + value);
            });
            console.log("Error obteniendo la lista de deseos"+JSON.stringify(acc));
        }
    });
}



function crearEventos(){

    $('ul li').click(function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codColor = $(this).attr('data-CodColor');
        var codTalla=$("#s-talla-"+codProducto).val();
        devuelvePrecio(codProducto,codColor);
        devuelveTallas(codProducto,codColor);
        $('#colorselec-'+codProducto).val(codColor);
        devuelveInventario(codProducto,codColor,codTalla);
    });

    $('.select').off("change").on("change", function() {
        var codProducto = $(this).attr('data-CodProducto');
        var codColor= $('#colorselec-'+codProducto).val();
        var codTalla= $(this).val();
        devuelveInventario(codProducto,codColor,codTalla);

    });

    $(".cancel").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        $('#cantidad-' + codProducto).val($('#cantidadAnterior-'+codProducto).val());
        $("#botones-compra-" + codProducto).hide( "slow", function() {
            $("#botones-princ-" + codProducto).show('slow');
        });
    });
    $(".delFav").click(function () {
        var codProducto = $(this).attr('data-CodProducto');
        var id_user = $('#id_user').val();
        $.ajax({
            type: 'POST',
            url: '../../admin/index.php?service=userservices&metodo=EliminarDeseo',
            data: JSON.stringify({p_id_user: id_user, p_cod_producto: codProducto}),
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (response) {
                if (response.success == '1') {
                    mostrarMensajeError('Éxito', response.msg, 'INFO');
                    obtenerDeseos();
                } else {
                    mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');
                }
            }, beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            },
            error: function (xhr, status, error) {
                var acc = []
                $.each(xhr, function (index, value) {
                    acc.push(index + ': ' + value);
                });
                console.log("Error eliminando deseo" + JSON.stringify(acc));
            }
        });

    });

    $(".buyNow").click(function () {
        metodo='Comprar';
        var codProducto = $(this).attr('data-CodProducto');
        $("#botones-princ-" + codProducto).hide("slow", function () {
            $("#botones-compra-" + codProducto).show('slow');
        });
      });

    $(".addCart").click(function () {
        metodo='Carrito';
        var codProducto = $(this).attr('data-CodProducto');
        $("#botones-princ-" + codProducto).hide("slow", function () {
            $("#botones-compra-" + codProducto).show('slow');
        });
      });



        $(".acept").click(function () {
            var codProducto = $(this).attr('data-CodProducto');
            var codTalla = $("#s-talla-" + codProducto).val();
            var codColor = $('#colorselec-' + codProducto).val();
            var cantidad = $('#cantidad-' + codProducto).val();
            var disponible = $('#disponible-' + codProducto).val();
            var cantidadAnterior = $('#cantidadAnterior-'+codProducto).val();
            var codUsuario = $('#id_user').val();
           /* if(cantidad<=0){
                $('#cantidad-' + codProducto).val(cantidadAnterior);
                mostrarMensajeError('Error', 'La cantidad no puede ser menor o igual a 0', 'ERROR');
                return;
            }
            if(cantidad>disponible){
                mostrarMensajeError('Error', 'La cantidad solicitada no está disponible', 'ERROR');
                return;
            }*/

           if(metodo=='Carrito'){
               insertarEnCarro(codProducto, codColor,codTalla,cantidad,codUsuario,"deseos");
               obtenerDeseos();
               return;
           }
           if(metodo=='Comprar'){
               var dialog = new BootstrapDialog({
                   message: function (dialogRef) {
                       var $message = $('<div>Producto:'+codProducto+' Talla:'+codTalla+' Color:'+codColor +' Cantidad:'+cantidad +' Metodo :'+ metodo +' </div>');
                       //var $message = $('<div></div>');
                       var $message2 = $('<div class="col-lg-12 text-center"> <h4>¿Está seguro de comprar este producto?</h4></div>');
                       var $si = $('<button class="btn btn-success btn-lg btn-block">Si</button>');
                       var $no = $('<button class="btn btn-danger btn-lg btn-block">No</button>');
                       $si.on('click', {dialogRef: dialogRef}, function (event) {
                           window.location.href = "process-CompraExpress.php?codProd=" + codProducto + "&codColor=" + codColor + "&codTalla=" + codTalla + "&cantidad=" + cantidad+"&metodo=deseos";
                           dialogRef.close();
                       });
                       $no.on('click', {dialogRef: dialogRef}, function (event) {
                           dialogRef.close();
                       });

                       $message.append($message2);
                       $message.append($si);
                       $message.append($no);

                       return $message;
                   },
                   closable: true
               });
               dialog.realize();
               dialog.getModalHeader().hide();
               dialog.getModalFooter().hide();
               dialog.open();
           }
        });


}

devuelvePrecio=function(codProducto,codColor){
    $.ajax({
        type: "GET",
        url: "../../admin/index.php?service=productoservices&metodo=ObtenerPrecioProducto&p_cod_producto="+codProducto+"&p_cod_color="+codColor,
        success: function (respuesta) {
            var ajaxResponse = $.parseJSON(respuesta);
            $('#id-precio-'+codProducto).empty();
            $('#id-precio-total-'+codProducto).empty();
            $('#id-precio-'+codProducto).append(ajaxResponse.precio.numberToMoneda());
            $('#id-precio-total-'+codProducto).append(ajaxResponse.precio.numberToMoneda());
        },beforeSend: function () {
            $.blockUI({
                message: '<h4>Por favor Espere...</h4>'
            });

        },
        complete: function () {
            $.unblockUI();
        }
    })

} ;

devuelveInventario=function(codProducto,codColor,codTalla){
    $.ajax({
        type: "GET",
        url: "../../admin/index.php?service=productoservices&metodo=ObtenerInventarioTallaColor&p_cod_producto="+codProducto+"&p_cod_color="+codColor+"&p_cod_talla="+codTalla,
        success: function (respuesta) {
            var ajaxResponse = $.parseJSON(respuesta);
            $('#disponible-'+codProducto).val(ajaxResponse.total);
        },
        beforeSend: function () {
            $.blockUI({
                message: '<h4>Por favor Espere...</h4>'
            });

        },
        complete: function () {
            $.unblockUI();
        }
    })

} ;

marcaFavorito=function(codProducto,codColor,codUsuario,servicio) {
    var parametros={
        'p_cod_producto':codProducto,
        'p_id_user':codUsuario
    };
    $.ajax({
            type: "POST",
            url: "../../admin/index.php?service=userservices&metodo="+servicio,
            data:JSON.stringify(parametros),
            contentType : "application/json",
            success: function (respuesta) {
                var ajaxResponse = $.parseJSON(respuesta);
                if(ajaxResponse.success=='1'){
                    mostrarMensajeError('Éxito', ajaxResponse.msg, 'INFO');
                    devuelveCantidadCarrito(codUsuario);
                }else{
                    mostrarMensajeError('Error', 'Ha ocurrido un error', 'ERROR');
                }

            },beforeSend: function () {
                $.blockUI({
                    message: '<h4>Por favor Espere...</h4>'
                });

            },
            complete: function () {
                $.unblockUI();
            }
        }
    )

};

devuelveTallas=function(codProducto,codColor){
    $.ajax({
        type: "GET",
        url: "../../admin/index.php?service=productoservices&metodo=ObtenerTallaColor&p_cod_producto="+codProducto+"&p_cod_color="+codColor,
        success: function (respuesta) {
            var ajaxResponse = $.parseJSON(respuesta);
            $.each(ajaxResponse,function(v,k){
                $("#s-talla"+codProducto).empty();
                $('<option>').val(k.CodMarca).text(k.DescMarca).appendTo('#s-marca');
            });
        },beforeSend: function () {
            $.blockUI({
                message: '<h4>Por favor Espere...</h4>'
            });

        },
        complete: function () {
            $.unblockUI();
        }
    })

} ;