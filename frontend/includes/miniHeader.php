<header class="miniHeader">
  <div class="container">
    <a href="index.php"><img src="images/logo.png" class="brand"></a>
    <ul class="nav nav-pills pull-right">
      <li><a href="../login.php"><span class="icon-user"></span> Iniciar</a></li>
      <li><a href="../register.php"><span class="icon-user"></span> Registrarse</a></li>
      <li><a href="../help.php"><span class="icon-help"></span> Ayuda</a></li>
    </ul>
  </div>
</header>