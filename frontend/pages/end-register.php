<?php 
	include '../config/definitions.php';

	$ch = curl_init();
	$url = $urlWS.'service=generalservices&metodo=ObtenerEstados';
	//echo $url;
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$resultData = curl_exec($ch);
	$estados = json_decode($resultData, true);
	

	if (isset($_POST)){

		session_start();
		$_SESSION['persona'] = array();

		$_SESSION['persona']['p_id_user'] = $_POST['idusuario'];
		$_SESSION['persona']['p_token'] = $_POST['token'];
		$_SESSION['persona']['p_tipoid'] = $_POST['tipoCedula'];
		$_SESSION['persona']['p_numid'] = $_POST['cedula'];
		if ($_POST['tipoCedula'] == 'J' || $_POST['tipoCedula']=='G')
		{
			$_SESSION['persona']['p_persona_c'] =  $_POST['nombre'].' '.$_POST['apellido'];
			$_SESSION['persona']['p_nombre_persona'] =$_POST['nombreEmpresa'];
			$_SESSION['persona']['p_apellido'] = '';
		}else{
			$_SESSION['persona']['p_nombre_persona'] = $_POST['nombre'];
			$_SESSION['persona']['p_apellido'] = $_POST['apellido'];
			$_SESSION['persona']['p_persona_c'] = "";
		}	
		
		$_SESSION['persona']['p_sexo'] = $_POST['optionsRadios'];
		$_SESSION['persona']['p_fec_nac'] = $_POST['dpYears'];

		/*echo "<pre>";
			print_r($_SESSION['persona']);
		echo "</pre>";*/


	}

	curl_close($ch);
	include '../views/end-register.php';
	
?>

