<?php 
	
include '../config/definitions.php';
$codProd = $_GET['codProd'];
$codColor = $_GET['codColor'];
$codTalla = $_GET['codTalla'];
$cantidad = $_GET['cantidad'];
$redirect = $_GET['redirect'];
	$varMsj = 2;

	if ((isset($_POST['email'])) && isset($_POST['password'])){
		$data = array();
		$data["p_usuario"] = $_POST['email'];
		$data["p_password"] = $_POST['password'];
		$codProd = $_POST['codProd'];
		$codColor = $_POST['codColor'];
		$codTalla = $_POST['codTalla'];
		$cantidad = $_POST['cantidad'];
		$redirect = $_POST['redirect'];
	
		$ch = curl_init();
		$url = $urlWS.'service=userservices&metodo=Login';
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

		$resultData = curl_exec($ch);
		$user = json_decode($resultData, true);

		$varMsj = $user['success'];
		if($varMsj==1){
			if(!isset($_SESSION))
			      session_start();

		$_SESSION['userid'] = $user['IdUser'];
           if (!empty($redirect))
			$redir = "Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/".$redirect.'.php?codProd='.$codProd.'&codColor='.$codColor.'&codTalla='.$codTalla.'&cantidad='.$cantidad;
			else
			$redir = "Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/index.php";
		    header($redir);
		}
	}

	include '../views/login.php';
?>
