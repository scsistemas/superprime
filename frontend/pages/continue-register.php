<?php 
	
	include '../config/definitions.php';

	if (isset($_GET['token']) && isset($_GET['idusuario']))
	{	
		$ch = curl_init();
		$url = $urlWS.'service=userservices&metodo=ValidarToken&p_codigo='.$_GET['token'];

		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$result = json_decode(curl_exec($ch), true);
		/*echo "<pre>";
		  	print_r($result);
		echo "</pre>";*/
		curl_close($ch);
		
		
		//echo($_GET['token']);
		if ($result['success'] == 1){
			session_start();
			$_SESSION['persona'] = array();
			$_SESSION['persona']['p_id_user'] = $_GET['idusuario'];
			$_SESSION['persona']['p_token'] = $_GET['token'];

			/*echo "<pre>";
		  	print_r($_SESSION['persona']);
			echo "</pre>";*/


		}else if ($result['success'] == 0){
					$redir = "Location: https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . "/register.php";
					header($redir);
				}
			

		
	}

	include '../views/continue-register.php';

?>