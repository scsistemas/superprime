<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11/01/16
 * Time: 11:37 AM
 */
require_once('../dao/IntegrationSelectDAO.php');
require_once('../dao/IntegrationInsertDAO.php');

class IntegrationServices {

    public function sincronizar($user)
    {
        try {
            $this->sincronizarMetaData();
            $this->sincronizarProductos($user);

            $posts = array('success' => '1');
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function sincronizarMetaData()
    {
        try {
            $sapDB = IntegrationSelectDAO::ObtenerDBSAP();
            foreach ($sapDB as $db) {
                $this->sincronizarMetaDataByBD($db);
            }

            $posts = array('success' => '1');
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }

    public function sincronizarMetaDataByBD($sapdb)
    {
        IntegrationInsertDAO::InsertarColores(IntegrationSelectDAO::ObtenerColores($sapdb));
        IntegrationInsertDAO::InsertarGeneros(IntegrationSelectDAO::ObtenerGeneros($sapdb));
        IntegrationInsertDAO::InsertarMarcas(IntegrationSelectDAO::ObtenerMarcas($sapdb));
        IntegrationInsertDAO::InsertarTallas(IntegrationSelectDAO::ObtenerTallas($sapdb));
        IntegrationInsertDAO::InsertarTipoProducto(IntegrationSelectDAO::ObtenerTipoProducto($sapdb));
    }

    public function sincronizarProductos($user, $fechaInicio, $fechaFin)
    {
        try {
            $idSinc = IntegrationInsertDAO::generarSincronizacion($user);
            $sapDB = IntegrationSelectDAO::ObtenerDBSAP();
            foreach ($sapDB as $db) {
                $this->sincronizarProductosByBD($db, $idSinc, $fechaInicio, $fechaFin);
            }
            IntegrationInsertDAO::actualizarSincronizacion($idSinc);
            $posts = array('success' => '1');
        } catch (Exception $e) {
            $error = $e->getMessage();
            $posts = array('success' => '0', 'error' => $error);
            echo json_encode($posts, JSON_UNESCAPED_UNICODE);
            exit;
        }
        echo json_encode($posts, JSON_UNESCAPED_UNICODE);
    }


    public function sincronizarProductosByBD($sapdb, $idSinc, $fechaInicio, $fechaFin)
    {
        $productos = IntegrationSelectDAO::ObtenerProductos($sapdb, $fechaInicio, $fechaFin);
        foreach ($productos as $codProd) {
            IntegrationInsertDAO::InsertarProducto($idSinc, IntegrationSelectDAO::ObtenerProducto($sapdb, $codProd));
            $colores = IntegrationSelectDAO::ObtenerColoresProducto($sapdb, $codProd);
            foreach ($colores as $color) {
                IntegrationInsertDAO::InsertarColor($codProd, $color);
                $codColor = $color['codigo'];
                IntegrationInsertDAO::InsertarPrecios($codProd, $codColor,IntegrationSelectDAO::ObtenerPreciosProducto($sapdb, $codProd, $codColor));

                $tallas = IntegrationSelectDAO::ObtenerTallasProducto($sapdb, $codProd, $codColor);
                foreach ($tallas as $talla) {
                    IntegrationInsertDAO::InsertarTalla($codProd, $codColor, $talla);
                }
            }
        }

    }
}