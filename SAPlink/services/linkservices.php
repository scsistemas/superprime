<?php
include_once 'IntegrationServices.php';
ini_set('max_execution_time', 90000);
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $services = new IntegrationServices();
    switch ($_GET['process']) {
        case 'sincronizar' :
            $services->sincronizar($_GET['user']);
            break;
        case 'sincronizarProductos' :
            $services->sincronizarProductos($_GET['user'], $_GET['fechaInicio'], $_GET['fechaFin']);
            break;
        case 'sincronizarMetaData' :
            $services->sincronizarMetaData();
            break;

    }
}

?>