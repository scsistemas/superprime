<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18/5/2016
 * Time: 10:22 PM
 */
require_once('../util/Database.php');

class IntegrationInsertDAO {
    public function IntegrationInsertDAO(){
    }

    public static function InsertarColores($data){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        foreach ($data as $row) {
           $result = mysqli_query($con, "SELECT * FROM colores WHERE CodColor = '$row[0]' LIMIT 1");
           if(!$result){
           }else{
               if (mysqli_num_rows($result)==0){
                   mysqli_query($con,"INSERT INTO `colores`(`CodColor`, `DescColor`) VALUES ('".$row[0]."','".$row[1]."');");
               }else{
                   mysqli_query($con,"UPDATE `colores` SET `DescColor` = '".$row[1]."' WHERE CodColor = '".$row[0]."' ");
               }
           }
        }
    }

    public static function InsertarGeneros($data){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        foreach ($data as $row) {
            $result = mysqli_query($con, "SELECT * FROM generos WHERE CodGenero = '$row[0]' LIMIT 1");
            if(!$result){
            }else{
                if (mysqli_num_rows($result)==0){
                    mysqli_query($con,"INSERT INTO `generos`(`CodGenero`, `DescGenero`) VALUES ('".$row[0]."','".$row[1]."');");
                }else{
                    mysqli_query($con,"UPDATE `generos` SET `DescGenero` = '".$row[1]."' WHERE CodGenero = '".$row[0]."' ");
                }
            }
        }
    }

    public static function InsertarMarcas($data){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        foreach ($data as $row) {
            $result = mysqli_query($con, "SELECT * FROM marcas WHERE CodMarca = '$row[0]' LIMIT 1");
            if(!$result){
            }else{
                if (mysqli_num_rows($result)==0){
                    mysqli_query($con,"INSERT INTO `marcas`(`CodMarca`, `DescMarca`) VALUES ('".$row[0]."','".$row[1]."');");
                }else{
                    mysqli_query($con,"UPDATE `marcas` SET `DescMarca` = '".$row[1]."' WHERE CodMarca = '".$row[0]."' ");
                }
            }
        }
    }

    public static function InsertarTallas($data){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        foreach ($data as $row) {
            $result = mysqli_query($con, "SELECT * FROM tallas WHERE CodTalla = '$row[0]' LIMIT 1");
            if(!$result){
            }else{
                if (mysqli_num_rows($result)==0){
                    mysqli_query($con,"INSERT INTO `tallas`(`CodTalla`, `DescTalla`) VALUES ('".$row[0]."','".$row[1]."');");
                }else{
                    mysqli_query($con,"UPDATE `tallas` SET `DescTalla` = '".$row[1]."' WHERE CodTalla = '".$row[0]."' ");
                }
            }
        }
    }

    public static function InsertarTipoProducto($data){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        foreach ($data as $row) {
            $result = mysqli_query($con, "SELECT * FROM tipoproducto WHERE CodTipoProd = '$row[0]' LIMIT 1");
            if(!$result){
            }else{
                if (mysqli_num_rows($result)==0){
                    mysqli_query($con,"INSERT INTO `tipoproducto`(`CodTipoProd`, `DescTipoProd`) VALUES ('".$row[0]."','".$row[1]."');");
                }else{
                    mysqli_query($con,"UPDATE `tipoproducto` SET `DescTipoProd` = '".$row[1]."' WHERE CodTipoProd = '".$row[0]."' ");
                }
            }
        }
    }

    public static function InsertarProducto($idSinc, $producto){
        if (!empty($producto)){
            $base = new Database('MySQL','');
            $con = $base->connect('MySQL');
            $result = mysqli_query($con, "SELECT * FROM productos WHERE CodProducto = '".$producto['codigo']."' LIMIT 1");
            if (!$result) {
            } else {
                if (mysqli_num_rows($result) == 0) {
                    mysqli_query($con, "INSERT INTO `productos`(`CodProducto`, `CodMarca`, `DescSap`, `CodTipoProd`, `CodGenero`, `idSinc`) VALUES ('".$producto['codigo']."','".$producto['codmarca']."','".$producto['descsap']."','".$producto['codtipoprod']."','".$producto['codgenero']."','".$idSinc."');");
                } else {
                    mysqli_query($con, "UPDATE `productos` SET `CodMarca` = '" . $producto['codmarca'] . "', `DescSap` = '" . $producto['descsap'] . "', `CodTipoProd` = '" . $producto['codtipoprod'] . "', `CodGenero` = '" . $producto['codgenero'] . "' WHERE CodProducto = '" . $producto['codigo'] . "' ");
                }
            }
        }
    }

    public static function InsertarColor($codProd, $color){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        $result = mysqli_query($con, "SELECT * FROM coloresproducto WHERE CodProducto = '".$codProd."' AND CodColor = '".$color['codigo']."' LIMIT 1");
        if (!$result) {
        } else {
            if (mysqli_num_rows($result) == 0) {
                mysqli_query($con, "INSERT INTO `coloresproducto`(`CodProducto`, `CodColor`, `DescSap`) VALUES ('".$codProd."','".$color['codigo']."','".$color['descripcion']."');");
            } else {
                mysqli_query($con, "UPDATE `coloresproducto` SET `DescSap` = '" .$color['descripcion'] . "' WHERE CodProducto = '".$codProd."' AND CodColor = '".$color['codigo']."' ");
            }
        }
    }

    public static function InsertarPrecios($codProd, $codColor, $precios){
        if (!empty($precios)){
            $base = new Database('MySQL','');
            $con = $base->connect('MySQL');
            $result = mysqli_query($con, "SELECT * FROM preciosproducto WHERE CodProducto = '".$codProd."' AND CodColor = '".$codColor."' LIMIT 1");
            if (!$result) {
            } else {
                if (mysqli_num_rows($result) == 0) {
                    mysqli_query($con, "INSERT INTO `preciosproducto`(`CodProducto`, `CodColor`, `PrecioSap`, `IvaSap`, `TotalSap`) VALUES ('".$codProd."','".$codColor."','".$precios['base']."','".$precios['impuesto']."','".$precios['total']."');");
                } else {
                    mysqli_query($con, "UPDATE `preciosproducto` SET `PrecioSap` = '" .$precios['base'] . "',`IvaSap` = '" .$precios['impuesto'] . "',`TotalSap` = '" .$precios['total'] . "' WHERE CodProducto = '".$codProd."' AND CodColor = '".$codColor."' ");
                }
            }
        }
    }


    public static function InsertarTalla($codProd, $codColor, $talla){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        $result = mysqli_query($con, "SELECT * FROM colorestallas WHERE CodProducto = '".$codProd."' AND CodColor = '".$codColor."' AND CodTalla = '".$talla."' LIMIT 1");
        if (!$result) {
        } else {
            if (mysqli_num_rows($result) == 0) {
                mysqli_query($con, "INSERT INTO `colorestallas`(`CodProducto`, `CodColor`, `CodTalla`) VALUES ('".$codProd."','".$codColor."','".$talla."');");
            }
        }
    }

    public static function generarSincronizacion($user){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        mysqli_query($con, "INSERT INTO `sincronizaciones`(`usuario`) VALUES ('$user');");
        $idSinc = $con->insert_id;
        $result = mysqli_query($con, "SELECT count(CodProducto) cant FROM productos WHERE idSinc = '".$idSinc."'");
        if (!$result) {
        } else {
            if ($row = $result->fetch_array()) {
                mysqli_query($con, "UPDATE `sincronizaciones` SET `cantProdSinc` = '" .$row['cant'] . "' WHERE idSinc = '".$idSinc."' ");
            }
        }

        return $idSinc;
    }

    public static function actualizarSincronizacion($idSinc){
        $base = new Database('MySQL','');
        $con = $base->connect('MySQL');
        $result = mysqli_query($con, "SELECT count(CodProducto) cant FROM productos WHERE idSinc = '".$idSinc."'");
        if (!$result) {
        } else {
            if ($row = $result->fetch_array()) {
                mysqli_query($con, "UPDATE `sincronizaciones` SET `cantProdSinc` = '" .$row['cant'] . "' WHERE idSinc = '".$idSinc."' ");
            }
        }
    }
}