<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18/5/2016
 * Time: 10:22 PM
 */
require_once('../util/Database.php');
require_once('IntegrationSelectDAO.php');

class IntegrationSelectDAO {
    public function IntegrationSelectDAO(){
    }

    public static function ObtenerColores($sapdb){
        $base = new Database('SQLServer',$sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct c.code codigo, c.name descripcion";
        $sql .= ' from dbo."@ctn_color" c;';
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = array ($row['codigo'],$row['descripcion']);
        }

        return $output;
    }
    public static function ObtenerGeneros($sapdb){
        $base = new Database('SQLServer',$sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct g.code codigo, g.name descripcion";
        $sql .= ' from dbo."@ctn_genero" g;';
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = array ($row['codigo'],$row['descripcion']);
        }

        return $output;
    }
    public static function ObtenerMarcas($sapdb){
        $base = new Database('SQLServer',$sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct m.code codigo, m.name descripcion";
        $sql .= ' from dbo."@ctn_marca" m;';
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = array ($row['codigo'],$row['descripcion']);
        }

        return $output;
    }
    public static function ObtenerTallas($sapdb){
        $base = new Database('SQLServer',$sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct t.code codigo, t.u_talla descripcion";
        $sql .= ' from dbo."@als_talla" t;';
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = array ($row['codigo'],$row['descripcion']);
        }

        return $output;
    }
    public static function ObtenerTipoProducto($sapdb){
        $base = new Database('SQLServer',$sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct e.code codigo, CONCAT(e.u_ctn_grupo,' - ',e.name) descripcion";
        $sql .= ' from dbo."@ctn_estilo" e order by descripcion;';
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = array ($row['codigo'],$row['descripcion']);
        }

        return $output;
    }

    public static function ObtenerProductos($sapdb, $fechaInicio, $fechaFin){
        $base = new Database('SQLServer', $sapdb);
        $con = $base->connect('SQLServer');
        $sql = "/*SET ROWCOUNT 10;*/select distinct substring(i.ItemCode,1,11) codigo";
        $sql .= " from oitm i where i.u_ctn_tipo='P'";
        if (!empty($fechaInicio) && !empty(fechaFin) ){
            $sql .= " and (CreateDate between '".$fechaInicio."' and '".$fechaFin."' or UpdateDate between '".$fechaInicio."' and '".$fechaFin."')";
        }
        $sql .= ";";

        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = $row['codigo'];
        }

        return $output;
    }

    public static function ObtenerProducto($sapdb, $codProd){
        $base = new Database('SQLServer', $sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct substring(i.ItemCode,1,11) codigo, i.u_ctn_marca codmarca, i.u_ctn_genero codgenero, i.u_ctn_estilo codtipoprod, CONCAT(e.u_ctn_grupo,' ',e.name,' ',m.name) descsap";
        $sql.=' from oitm i, dbo."@ctn_marca" m, dbo."@ctn_estilo" e, dbo."@ctn_genero" g';
        $sql.=" where i.u_ctn_tipo='P'
                and i.u_ctn_marca = m.code
                and i.u_ctn_estilo = e.code
                and i.u_ctn_genero = g.code
                and substring(i.ItemCode,1,11) = '".$codProd."';";
        $producto = array();
        $stmt = sqlsrv_query($con, $sql);

        if($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $producto['codigo'] = $row['codigo'];
            $producto['codmarca'] = $row['codmarca'];
            $producto['codgenero'] = $row['codgenero'];
            $producto['codtipoprod'] = $row['codtipoprod'];
            $producto['descsap'] = $row['descsap'];
        }

        return $producto;
    }

    public static function ObtenerColoresProducto($sapdb, $codProd){
        $base = new Database('SQLServer', $sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select distinct c.code codigo, c.name descripcion";
        $sql .= ' from oitm i, dbo."@ctn_color" c';
        $sql .= " where i.u_ctn_color = c.code
                  and substring(i.ItemCode,1,11) = '".$codProd."';";
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $color['codigo'] = $row['codigo'];
            $color['descripcion'] = $row['descripcion'];
            $output[] = $color;
        }

        return $output;
    }

    public static function ObtenerPreciosProducto($sapdb, $codProd, $codColor){
        $base = new Database('SQLServer', $sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select i.price precio, t.rate porcimp, (i.price*t.rate)/100 impuesto, (i.price+((i.price*t.rate)/100)) total
                from itm1 i, oitm oi, ostc t
                where oi.ItemCode = i.ItemCode
                and i.pricelist = 3
                and oi.TaxCodeAR = t.Code
                and i.ItemCode ='".$codProd.$codColor."';";
        $precio = array();
        $stmt = sqlsrv_query($con, $sql);

        if($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $precio['base'] = $row['precio'];
            $precio['porcimp'] = $row['porcimp'];
            $precio['impuesto'] = $row['impuesto'];
            $precio['total'] = $row['total'];
        }

        return $precio;
    }

    public static function ObtenerTallasProducto($sapdb, $codProd, $codColor){
        $base = new Database('SQLServer', $sapdb);
        $con = $base->connect('SQLServer');
        $sql = "select t.code codigo";
        $sql .= ' from oitm i,  dbo."@als_talla" t';
        $sql .= " where i.u_ctn_talla = t.code
                  and substring(i.ItemCode,1,15) ='".$codProd.$codColor."';";
        $output = array();
        $stmt = sqlsrv_query($con, $sql);

        while($row= sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
            $output[] = $row['codigo'];
        }

        return $output;
    }

    public static function ObtenerDBSAP()
    {
        $base = new Database('MySQL', '');
        $con = $base->connect('MySQL');
        $result = mysqli_query($con, "SELECT nombre FROM sapdb WHERE activa = 1");
        $rows = array();
        while ($row = $result->fetch_array()) {
            $bd[] = $row['nombre'];
        }
        return $bd;
    }
}